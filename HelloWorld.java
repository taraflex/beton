import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Base64;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;

class HelloWorld {

    
    public static void closeQuietly(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
        }
    }

     public static void sendBody(javax.net.ssl.HttpsURLConnection r7) throws java.io.IOException {
        
            r7.setDoOutput(true) ;
            r7.setFixedLengthStreamingMode(43)     ;
            r7.setRequestProperty("Content-Type", "application/json")  ;
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream (r7.getOutputStream());
            r3.write("{\"sKey\":\"nS96#F@o4y|Pxa7LOgr5eg2JP|#IH8%x\"}".getBytes())    ;
           closeQuietly(r3)     ;
    }

     public static String readToken(HttpsURLConnection cn) throws IOException {
        InputStream in = new BufferedInputStream(cn.getInputStream());
        try {
          return HelloWorld.toStringQuietly(in);
        }  finally {
            HelloWorld.closeQuietly(in);
        }
    }

    public static String toString(InputStream is) throws IOException {
        StringBuilder result = new StringBuilder();
        Reader reader = new InputStreamReader(is, Charset.defaultCharset());
        char[] buffer = new char[65536];
        while (true) {
            try {
                int bytes = reader.read(buffer);
                if (bytes != -1) {
                    result.append(buffer, 0, bytes);
                } else {
                    closeQuietly(reader);
                    return result.toString();
                }
            } catch (Throwable th) {
                closeQuietly(reader);
                throw th;
            }
        }
    }

    public static String toStringQuietly(InputStream is) {
        try {
            return toString(is);
        } catch (IOException e) {
            return "";
        }
    }

    public static void main(String[] args) throws IOException {
    try {
    SSLContext context = SSLContext.getInstance("TLS");
    HttpsURLConnection.setDefaultHostnameVerifier(
        (hostname, session) -> true);
    context.init(
        null,
        new X509TrustManager[] {
          new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {}

            public void checkServerTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {}

            public X509Certificate[] getAcceptedIssuers() {
              return new X509Certificate[0];
            }
          }
        },
        new SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
  } catch (Exception e) {
    e.printStackTrace();
  }
 /*System.setProperty("http.proxyHost", "127.0.0.1");
    System.setProperty("https.proxyHost", "127.0.0.1");
    System.setProperty("http.proxyPort", "8888");
    System.setProperty("https.proxyPort", "8888");*/
          HttpsURLConnection cn = (HttpsURLConnection) new URL("https://bet-hub.com/mobile_get_token/").openConnection();
        //  conn.setHostnameVerifier(RelaxedSSLContext.allHostsValid); 
        cn.setRequestMethod("PUT");
        cn.addRequestProperty("Accept", "application/json");
        cn.addRequestProperty("Authorization", "Basic " + new String(Base64.getEncoder().encode(  "footballx2017:Baku2017".getBytes() )) );
        sendBody(cn);
      //  System.out.println(readToken(cn)); 
    }
}