!function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: !1,
            exports: {}
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
        module.l = !0, module.exports;
    }
    __webpack_require__.m = modules, __webpack_require__.c = installedModules, __webpack_require__.d = function(exports, name, getter) {
        __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
            enumerable: !0,
            get: getter
        });
    }, __webpack_require__.r = function(exports) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(exports, "__esModule", {
            value: !0
        });
    }, __webpack_require__.t = function(value, mode) {
        if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
        if (4 & mode && "object" == typeof value && value && value.__esModule) return value;
        var ns = Object.create(null);
        if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
            enumerable: !0,
            value
        }), 2 & mode && "string" != typeof value) for (var key in value) __webpack_require__.d(ns, key, function(key) {
            return value[key];
        }.bind(null, key));
        return ns;
    }, __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module.default;
        } : function getModuleExports() {
            return module;
        };
        return __webpack_require__.d(getter, "a", getter), getter;
    }, __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 132);
}([ function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return __decorate;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return __param;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return __metadata;
    }));
    function __decorate(decorators, target, key, desc) {
        var d, c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc;
        if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function(target, key) {
            decorator(target, key, paramIndex);
        };
    }
    function __metadata(metadataKey, metadataValue) {
        if ("object" == typeof Reflect && "function" == typeof Reflect.metadata) return Reflect.metadata(metadataKey, metadataValue);
    }
    Object.create;
    Object.create;
}, function(module, exports) {
    module.exports = require("typescript-ioc");
}, function(module, exports) {
    module.exports = require("typeorm");
}, function(module, exports) {
    module.exports = require("@taraflex/string-tools");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return Access;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return crud_v;
    }));
    __webpack_require__(116);
    var external_typeorm_ = __webpack_require__(2), types = __webpack_require__(11), string_tools_ = __webpack_require__(3), external_fastest_validator_ = __webpack_require__(78), external_fastest_validator_default = __webpack_require__.n(external_fastest_validator_), array_ = __webpack_require__(79), array_default = __webpack_require__.n(array_), string_ = __webpack_require__(48), string_default = __webpack_require__.n(string_);
    const validator_helpers_v = new external_fastest_validator_default.a({
        messages: {
            validRegex: "The '{field}' {actual}"
        }
    });
    validator_helpers_v.add("regex", (function({messages}) {
        return {
            source: `\n            try {\n                new RegExp(value);\n            } catch(err) {\n                ${this.makeError({
                type: "validRegex",
                actual: "err.message",
                messages
            })}\n            }\n            return value;\n        `
        };
    })), validator_helpers_v.add("javascript", (function({messages}) {
        return {
            source: `\n            try {\n                new Function(value);\n            } catch(err) {\n                ${this.makeError({
                type: "validRegex",
                actual: "err.message",
                messages
            })}\n            }\n            return value;\n        `
        };
    })), validator_helpers_v.add("wysiwyg", string_default.a), validator_helpers_v.add("link", string_default.a), 
    validator_helpers_v.add("md-tgclient", string_default.a), validator_helpers_v.add("md-tgbotlegacy", string_default.a), 
    validator_helpers_v.add("files", array_default.a);
    const compile = validator_helpers_v.compile.bind(validator_helpers_v);
    function getFieldSchema(target, options, propertyName) {
        const {nullable, unsigned, array, zerofill, primary} = options;
        let type;
        try {
            type = options.type.name.toLowerCase();
        } catch {
            type = options.type.toString();
        }
        let vo = target.__validatorInfo && target.__validatorInfo[propertyName];
        const _enum = options.enum || vo && vo.enum;
        let enumPairs = void 0, enumValues = void 0;
        const generated = Object(external_typeorm_.getMetadataArgsStorage)().generations.some(v => v.target === target && v.propertyName === propertyName);
        if (Array.isArray(_enum)) enumValues = _enum, enumPairs = enumValues.map(v => [ v, v ]); else if (_enum) {
            const keys = new Set(Object.keys(_enum));
            for (;keys.size > 0; ) {
                const {value} = keys.values().next();
                let k = value, v = _enum[k];
                !Object(string_tools_.isValidVar)(k) || _enum[v] === k && Object(string_tools_.isValidVar)(v) && v.toUpperCase() == v && k.toUpperCase() !== k ? keys.delete(k) : (Array.isArray(enumPairs) || (enumPairs = [], 
                enumValues = []), keys.delete(v.toString()), keys.delete(k), enumPairs.push([ k, v ]), 
                enumValues.push(v));
            }
        }
        let integer = void 0;
        if (type.includes("int")) type = "number", integer = !0; else if (type.includes("date") || type.includes("time")) type = "date"; else if (type.includes("string") || type.includes("char") || type.includes("clob") || type.includes("text")) type = "string"; else if (type.includes("bool")) type = "boolean"; else {
            if (type.includes("blob") || type.includes("binary")) throw "Validator not implemented";
            "simple-array" === type || "simple-json" === type || "array" === type || array ? type = "array" : "enum" === type || (type = "number");
        }
        (!Array.isArray(enumValues) || enumValues.length < 1) && (enumValues = enumPairs = void 0);
        let state = vo ? 0 | vo.state : 0;
        ("array" === type || "enum" === type || enumValues) && (8 == (8 & state) || !enumValues || vo && vo.min > 1 ? (enumValues = null, 
        type = "array") : type = "enum", vo && (vo = {
            ...vo
        }, delete vo.enum));
        const isNumber = "number" === type, o = {
            type,
            enumPairs,
            enum: enumValues,
            values: enumValues,
            integer,
            min: isNumber && (unsigned || zerofill || vo && vo.positive) ? 0 : null,
            max: isNumber && vo && vo.negative ? 0 : null,
            optional: !generated && (nullable || null != options.default),
            default: zerofill && isNumber && null == options.default ? 0 : options.default,
            state
        }, r = function nullPrune(o) {
            for (let k in o) null == o[k] && delete o[k];
            return o;
        }(vo ? Object.assign(o, vo) : o);
        return "link" === r.type && (r.state = 34 | r.state), primary && (r.state = 18 | r.state | (generated ? 98 : 0)), 
        r;
    }
    function validate(o) {
        const r = this(o);
        if (!0 !== r) throw r;
    }
    function Access(allowedFor, displayInfo, hooks) {
        return entity => {
            const vInsert = Object.create(null), vUpdate = Object.create(null);
            for (let {propertyName, options} of function* getColumns(entity) {
                if (entity && entity !== Function.prototype) {
                    const selfColumns = Object(external_typeorm_.getMetadataArgsStorage)().filterColumns(entity);
                    yield* getColumns(Object.getPrototypeOf(entity));
                    for (let column of selfColumns) yield column;
                }
            }(entity)) {
                const schema = getFieldSchema(entity, options, propertyName);
                Object(types.a)(schema, 34) || (vInsert[propertyName] = schema), vUpdate[propertyName] = schema;
            }
            allowedFor = allowedFor || Object.create(null), allowedFor._ !== +allowedFor._ && (allowedFor._ = -1), 
            Object.freeze(allowedFor), Object.defineProperty(entity, "checkAccess", {
                value(action, user) {
                    if (!user) throw 401;
                    let t = allowedFor[action];
                    if (null == t && (t = allowedFor._), (t & user.role) !== t) throw 403;
                    return !0;
                }
            });
            for (const _ in vInsert) {
                Object.defineProperty(entity, "insertValidate", {
                    value: validate.bind(compile(vInsert))
                });
                break;
            }
            for (const _ in vUpdate) {
                Object.defineProperty(entity, "updateValidate", {
                    value: validate.bind(compile(vUpdate))
                });
                break;
            }
            return displayInfo || (displayInfo = Object.create(null)), displayInfo.display || (displayInfo.display = entity.asyncProvider ? "single" : "table"), 
            Object.defineProperty(entity, "rtti", {
                value: Object.freeze({
                    props: Object.freeze(vUpdate),
                    displayInfo: Object.freeze(displayInfo)
                })
            }), Object.defineProperty(entity, "hooks", {
                value: hooks || Object.create(null)
            }), delete entity.__validatorInfo, entity;
        };
    }
    function crud_v(validatorProps) {
        return ({constructor}, prop) => {
            (constructor.__validatorInfo || (constructor.__validatorInfo = Object.create(null)))[prop] = Object.freeze(validatorProps);
        };
    }
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "g", (function() {
        return REAL_HOSTNAME;
    })), __webpack_require__.d(__webpack_exports__, "d", (function() {
        return HOSTNAME;
    })), __webpack_require__.d(__webpack_exports__, "e", (function() {
        return PEM;
    })), __webpack_require__.d(__webpack_exports__, "f", (function() {
        return PORT;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return ADMIN;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return API;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return HEALTH;
    })), __webpack_require__.d(__webpack_exports__, "h", (function() {
        return STATIC;
    }));
    var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6), path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(23), net__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(41);
    const regulus = require(__dirname + "/../package.json").regulus || {}, REAL_HOSTNAME = regulus.hostname || "localhost", HOSTNAME = process.argv.includes("--local") && !Object(net__WEBPACK_IMPORTED_MODULE_2__.isIP)(REAL_HOSTNAME) && "localhost" != REAL_HOSTNAME ? "dev." + REAL_HOSTNAME : REAL_HOSTNAME, PEM = REAL_HOSTNAME !== HOSTNAME && Object(fs__WEBPACK_IMPORTED_MODULE_0__.existsSync)(__dirname + "/../REGULUS.pem") && Object(path__WEBPACK_IMPORTED_MODULE_1__.resolve)(__dirname + "/../REGULUS.pem") || "", PORT = REAL_HOSTNAME !== HOSTNAME ? regulus.devport || 7813 : regulus.port || 7812, routes = Object.assign({
        admin: "/adm",
        api: "/api",
        health: "/health"
    }, regulus.routes), ADMIN = routes.admin, API = routes.api, HEALTH = routes.health, STATIC = "/static";
}, function(module, exports) {
    module.exports = require("fs");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "b", (function() {
        return USERAGENT;
    }));
    var got__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(75), got__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(got__WEBPACK_IMPORTED_MODULE_0__), tough_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(66);
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return tough_cookie__WEBPACK_IMPORTED_MODULE_1__.CookieJar;
    }));
    const USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";
    __webpack_exports__.c = got__WEBPACK_IMPORTED_MODULE_0___default.a.extend({
        timeout: 6e4,
        ignoreInvalidCookies: !0,
        headers: {
            Accept: "*/*",
            "Cache-Control": "no-cache",
            Connection: "keep-alive",
            Pragma: "no-cache",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": USERAGENT
        },
        retry: 0,
        https: {
            rejectUnauthorized: !1
        }
    });
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return ExtendableError;
    }));
    class ExtendableError extends Error {
        constructor(message) {
            super(message), this.name = this.constructor.name, Error.captureStackTrace(this, this.constructor);
        }
    }
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    var appdirectory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(81), appdirectory__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(appdirectory__WEBPACK_IMPORTED_MODULE_0__), make_dir__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(20), path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(23), _utils_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5);
    let cwd = process.cwd();
    const path = function createDataDir(path) {
        try {
            return Object(make_dir__WEBPACK_IMPORTED_MODULE_1__.sync)(path), path;
        } catch (err) {
            return console.error.bind(console, "(./src/core/utils/data-path.ts:14)")(err), path.startsWith(cwd) ? (path = path.slice(cwd.length), 
            cwd = Object(path__WEBPACK_IMPORTED_MODULE_2__.join)(cwd, "./../"), createDataDir(Object(path__WEBPACK_IMPORTED_MODULE_2__.join)(cwd, "/", path))) : path;
        }
    }(Object(path__WEBPACK_IMPORTED_MODULE_2__.normalize)(new appdirectory__WEBPACK_IMPORTED_MODULE_0___default.a({
        appName: "beton-" + _utils_config__WEBPACK_IMPORTED_MODULE_3__.f,
        useRoaming: !0
    }).userData()));
    console.log("App directory: " + path), __webpack_exports__.a = function(...pathParts) {
        return pathParts.length > 0 ? Object(path__WEBPACK_IMPORTED_MODULE_2__.join)(path, "/", ...pathParts) : path;
    };
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "ClientSettings", (function() {
        return ClientSettings;
    })), __webpack_require__.d(__webpack_exports__, "ClientSettingsRepository", (function() {
        return ClientSettingsRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4), _ioc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(18);
    let ClientSettings = class ClientSettings {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 5,
        description: "Telegram телефон"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "phone", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 5,
        description: "VK телефон"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "vkPhone", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "VK пароль"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "vkPassword", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /^.+@.+$/,
        description: "Email (вида vasya@mail.ru)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "email", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Email пароль"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "emailPassword", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "IMAP хост (гуглить imap настройки почтового провайдера)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "imap.yandex.ru"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "imapHost", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        max: 65535,
        description: "IMAP порт (гуглить imap настройки почтового провайдера)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 993,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "imapPort", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        max: 86400,
        description: "Пауза перед повторым подключением в секундах после ошибки IMAP подключения"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 5,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "imapReconnectPause", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        max: 86400,
        description: "Максимальное время выполнения imap запроса в секундах (если не уложились пересоздание соединения)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 10,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "imapActionMaxTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 86400,
        description: "Пауза перед принудительным опросом IMAP в секундах после последнего удачного обновления (даже если не было уведомлений о новых сообщениях)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 20,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "imapBeforePingPause", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: 'Имя android устройста (регистр имеет значение), привязанного к аккаунтам на bet-hub [колонка "Устройство"](https://bet-hub.com/user/mobile_app_devices/)'
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "deviceName", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: 'Id android устройста (регистр имеет значение), привязанного к аккаунтам на bet-hub [поле "Hardware serial"](https://play.google.com/store/apps/details?id=onemlab.deviceid)'
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "deviceSerial", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Регулярное выражение для фильтрации (белый список) входящих сообщений."
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ".*"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "whiteFilter", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Регулярное выражение для фильтрации (черный список) входящих сообщений."
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "t\\.me\\/"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "blackFilter", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /^[0-9abcdef]{32}$/,
        description: "[captcha KEY](https://rucaptcha.com/setting) - [Не забудьте также включить уведомления о низком балансе](https://rucaptcha.com/profile/notif)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], ClientSettings.prototype, "ruCapchaKey", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 30,
        max: 3600,
        description: "Максимальное время на разгадывание капчи в секундах."
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 180,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "ruCapchaTimeout", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        max: 3600,
        description: "Максимальное время на отправку вложения в телеграм сообщение в секундах (без учета времени генерации или скачивания со стороннего ресурса). ***0*** - не ограничено"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 60,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "tgMaxImageUploadTime", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 86400,
        description: "Минимальная пауза между запросами к bet-hub.com в секундах."
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 60,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "minimumUpdateInterval", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        max: 86400,
        description: 'Максимальная пауза между запросами к bet-hub.com в секундах, после превышения которой на почту будет отправлено уведомление. Если ⩽ "минимальная пауза"  - не уведомлять.'
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 3600,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "alertInterval", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 86400,
        description: "Максимальная пауза между запросами к bet-hub.com в секундах."
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 10800,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "maximumUpdateInterval", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Удалить события из базы старше ***х*** дней (но не из телеграм канала). ***0*** - не удалять."
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 5,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "removeAfter", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        max: 9,
        description: "min_score google recapcha v3"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 7,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], ClientSettings.prototype, "capchaMinScore", void 0), 
    ClientSettings = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        PATCH: 0
    }, {
        display: "single",
        icon: "cog",
        order: 4
    }, {
        PATCH: "tgclient_save"
    }), Object(_ioc__WEBPACK_IMPORTED_MODULE_3__.a)() ], ClientSettings);
    class ClientSettingsRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    function has(item, state) {
        return (item.state & state) === state;
    }
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return has;
    }));
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "b", (function() {
        return escMdForce;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return BOT_TOKEN_RE;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return toChatId;
    }));
    __webpack_require__(7), __webpack_require__(25);
    (function getCommandParser(cmd) {
        const re = new RegExp("^\\/" + cmd + "(\\s+|$)", "i");
        return text => re.test(text) ? text.substr(cmd.length + 2).trim() : null;
    })("start");
    const escMdMapForce = {
        "*": "✶",
        "#": "♯",
        "[": "［",
        "]": "］",
        _: "＿",
        "\\": "﹨",
        "`": "’",
        "~": "～",
        "@": "@‍",
        "/": "/‍"
    };
    function escMdForce(s) {
        return s ? s.replace(/[~@\/\*\[\]\\_`#]/g, m => escMdMapForce[m]) : "";
    }
    const BOT_TOKEN_RE = /^\d{9,}:[\w-]{35}$/;
    function toChatId(supergroup_id) {
        return +("-100" + supergroup_id);
    }
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "User", (function() {
        return User;
    })), __webpack_require__.d(__webpack_exports__, "UserRepository", (function() {
        return UserRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
    let User = class User {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], User.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /\S+@\S+/
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unique: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], User.prototype, "email", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 5,
        max: 70
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        length: 60
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], User.prototype, "password", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 2147483647
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], User.prototype, "role", void 0), 
    User = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)(), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], User);
    class UserRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports, __webpack_require__) {
    "use strict";
    var pug_has_own_property = Object.prototype.hasOwnProperty;
    function pug_classes(val, escaping) {
        return Array.isArray(val) ? function pug_classes_array(val, escaping) {
            for (var className, classString = "", padding = "", escapeEnabled = Array.isArray(escaping), i = 0; i < val.length; i++) className = pug_classes(val[i]), 
            className && (escapeEnabled && escaping[i] && (className = pug_escape(className)), 
            classString = classString + padding + className, padding = " ");
            return classString;
        }(val, escaping) : val && "object" == typeof val ? function pug_classes_object(val) {
            var classString = "", padding = "";
            for (var key in val) key && val[key] && pug_has_own_property.call(val, key) && (classString = classString + padding + key, 
            padding = " ");
            return classString;
        }(val) : val || "";
    }
    function pug_style(val) {
        if (!val) return "";
        if ("object" == typeof val) {
            var out = "";
            for (var style in val) pug_has_own_property.call(val, style) && (out = out + style + ":" + val[style] + ";");
            return out;
        }
        return val + "";
    }
    function pug_attr(key, val, escaped, terse) {
        if (!1 === val || null == val || !val && ("class" === key || "style" === key)) return "";
        if (!0 === val) return " " + (terse ? key : key + '="' + key + '"');
        var type = typeof val;
        return "object" !== type && "function" !== type || "function" != typeof val.toJSON || (val = val.toJSON()), 
        "string" == typeof val || (val = JSON.stringify(val), escaped || -1 === val.indexOf('"')) ? (escaped && (val = pug_escape(val)), 
        " " + key + '="' + val + '"') : " " + key + "='" + val.replace(/'/g, "&#39;") + "'";
    }
    exports.merge = function pug_merge(a, b) {
        if (1 === arguments.length) {
            for (var attrs = a[0], i = 1; i < a.length; i++) attrs = pug_merge(attrs, a[i]);
            return attrs;
        }
        for (var key in b) if ("class" === key) {
            var valA = a[key] || [];
            a[key] = (Array.isArray(valA) ? valA : [ valA ]).concat(b[key] || []);
        } else if ("style" === key) {
            valA = pug_style(a[key]);
            valA = valA && ";" !== valA[valA.length - 1] ? valA + ";" : valA;
            var valB = pug_style(b[key]);
            valB = valB && ";" !== valB[valB.length - 1] ? valB + ";" : valB, a[key] = valA + valB;
        } else a[key] = b[key];
        return a;
    }, exports.classes = pug_classes, exports.style = pug_style, exports.attr = pug_attr, 
    exports.attrs = function pug_attrs(obj, terse) {
        var attrs = "";
        for (var key in obj) if (pug_has_own_property.call(obj, key)) {
            var val = obj[key];
            if ("class" === key) {
                val = pug_classes(val), attrs = pug_attr(key, val, !1, terse) + attrs;
                continue;
            }
            "style" === key && (val = pug_style(val)), attrs += pug_attr(key, val, !1, terse);
        }
        return attrs;
    };
    var pug_match_html = /["&<>]/;
    function pug_escape(_html) {
        var html = "" + _html, regexResult = pug_match_html.exec(html);
        if (!regexResult) return _html;
        var i, lastIndex, escape, result = "";
        for (i = regexResult.index, lastIndex = 0; i < html.length; i++) {
            switch (html.charCodeAt(i)) {
              case 34:
                escape = "&quot;";
                break;

              case 38:
                escape = "&amp;";
                break;

              case 60:
                escape = "&lt;";
                break;

              case 62:
                escape = "&gt;";
                break;

              default:
                continue;
            }
            lastIndex !== i && (result += html.substring(lastIndex, i)), lastIndex = i + 1, 
            result += escape;
        }
        return lastIndex !== i ? result + html.substring(lastIndex, i) : result;
    }
    exports.escape = pug_escape, exports.rethrow = function pug_rethrow(err, filename, lineno, str) {
        if (!(err instanceof Error)) throw err;
        if (!("undefined" == typeof window && filename || str)) throw err.message += " on line " + lineno, 
        err;
        try {
            str = str || __webpack_require__(6).readFileSync(filename, "utf8");
        } catch (ex) {
            pug_rethrow(err, null, lineno);
        }
        var context = 3, lines = str.split("\n"), start = Math.max(lineno - context, 0), end = Math.min(lines.length, lineno + context);
        context = lines.slice(start, end).map((function(line, i) {
            var curr = i + start + 1;
            return (curr == lineno ? "  > " : "    ") + curr + "| " + line;
        })).join("\n");
        throw err.path = filename, err.message = (filename || "Pug") + ":" + lineno + "\n" + context + "\n\n" + err.message, 
        err;
    };
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "i", (function() {
        return unPack;
    })), __webpack_require__.d(__webpack_exports__, "g", (function() {
        return toHtmlArray;
    })), __webpack_require__.d(__webpack_exports__, "h", (function() {
        return toTextArray;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return BannedError;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return AutoBidError;
    })), __webpack_require__.d(__webpack_exports__, "e", (function() {
        return ExpiredError;
    })), __webpack_require__.d(__webpack_exports__, "d", (function() {
        return CapchaError;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return Ban10MinutesError;
    })), __webpack_require__.d(__webpack_exports__, "f", (function() {
        return tableToForecast;
    }));
    var cheerio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(38), cheerio__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(cheerio__WEBPACK_IMPORTED_MODULE_0__), js_beautify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80), url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(45), vm__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(52), _entities_Forecast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(27), _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8), _utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(12);
    function unPack(code) {
        return Object(vm__WEBPACK_IMPORTED_MODULE_3__.runInNewContext)(code, {
            eval(c) {
                code = c;
            },
            window: {},
            document: {}
        }), Object(js_beautify__WEBPACK_IMPORTED_MODULE_1__.js)(code).replace(/(JsHttpRequest\.query|grecaptcha\.render|grecaptcha\.ready|grecaptcha\.execute|\$\.ajax|\$\("#\w+"\)\.click\()/g, "return $1").replace(/div\.innerHTML\s*=\s*/g, "return ");
    }
    const months = {
        января: "jan",
        февраля: "feb",
        марта: "mar",
        апреля: "apr",
        мая: "may",
        июня: "jun",
        июля: "jul",
        августа: "aug",
        сентября: "sep",
        октября: "oct",
        ноября: "nov",
        декабря: "dec"
    };
    function sportIcon(s) {
        switch (s) {
          case "Теннис":
            return "🎾 ";

          case "Футбол":
            return "⚽️ ";

          case "Волейбол":
            return "🏐 ";

          case "Бейсбол":
            return "⚾️ ";

          case "Баскетбол":
            return "🏀 ";

          case "Хоккей":
            return "🏒 ";

          case "Киберспорт":
            return "🎮 ";

          case "Боевые искусства":
            return "🤼‍♂️ ";

          case "Регби":
            return "🏈 ";

          case "Бокс":
            return "🥊 ";

          case "Гандбол":
            return "🤾‍♂️ ";
        }
        return "";
    }
    function toHtmlArray($, c) {
        return c.map((_, e) => $(e).html().trim()).toArray();
    }
    function toTextArray($, c) {
        return c.map((_, e) => $(e).text().trim()).toArray();
    }
    function TEXT(c) {
        return c.children().after("<span> </span>"), Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(c.text().trim());
    }
    class BannedError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__.a {}
    class AutoBidError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__.a {}
    class EmptyLinkError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__.a {}
    class InvalidSubscription extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__.a {}
    class ExpiredError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__.a {}
    class CapchaError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__.a {}
    class Ban10MinutesError extends CapchaError {
        constructor() {
            super(...arguments), this.expire = Date.now() + 72e4;
        }
    }
    async function tableToForecast(tg, html) {
        if (html.includes("Вы не смогли пройти капчу три раза в течение 10 минут")) throw new Ban10MinutesError;
        if ("RCV2" == html || html.includes("Recaptcha") || html.includes(" капч") || html.includes("токен безопасности")) throw new CapchaError(html);
        if (html.includes("Показ прогноза запрещён")) throw new BannedError(html);
        if (html.includes("Данный прогноз доступен только для авто-ставок")) throw new AutoBidError(html);
        if (html.includes("Использование прогноза невозможно: данное событие уже")) throw new ExpiredError(html);
        const tables = cheerio__WEBPACK_IMPORTED_MODULE_0___default.a.load(html, {
            decodeEntities: !1
        })("table"), cs = tables.find(".comment_sub_name");
        cs.find("div,span").remove();
        const subscription = cs.text().trimLeft().split(/\s/, 1)[0];
        if (!/\.(L|UT|U)$/.test(subscription)) throw new InvalidSubscription(html);
        const loc = TEXT(tables.find(".location")), sport = new Function('function alert(v){ return v.split("|",2)[1] }; return ' + tables.find(".sport").attr("onclick"))(), comment = (Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(tables.find(".center").text().trim()) + "\n" + TEXT(tables.find(".comment").children().first())).trim(), dates = new Function("function alert(v){ return v }; return " + tables.find(".header_td").children().first().attr("onclick"))().split(/\n+/), enterTime = dates.find(s => s.startsWith("Введено: ")), actionTime = dates.find(s => s.startsWith("Событие: ")), img = tables.find('img[src^="/cache/sales_protection/"]').attr("src"), f = new _entities_Forecast__WEBPACK_IMPORTED_MODULE_4__.Forecast;
        if (img) f.info = await tg.uploadAndUnlink(new url__WEBPACK_IMPORTED_MODULE_2__.URL("https://bet-hub.com" + img), 1, tg.parseMD(`${sportIcon(sport)}${Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(sport)}\n*${loc}* \n${actionTime ? Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(actionTime.replace("Событие: ", "")) : ""}\n\`Подписка\` ${Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(subscription)}\n${comment}`.trim())); else {
            const eventLink = tables.find(".event > a"), eventUrl = (eventLink.attr("href") || "").replace("/_seo/subs_mobile.php?q=", "https://bet-hub.com/sub/");
            if (!eventUrl) throw new EmptyLinkError(html);
            const event = TEXT(eventLink), book = tables.find(".book > a"), infoRow = book.parent().parent();
            let {bookLink, bookName} = new Function(`function go_book_mobile(bookLink, bookName) { return { bookLink, bookName } }; return {${book.attr("href") || 'javascript:go_book_mobile("https://bet-hub.com/","")'}}.javascript`)();
            bookLink = bookLink || "https://bet-hub.com/", bookName = bookName || new url__WEBPACK_IMPORTED_MODULE_2__.URL(bookLink).hostname, 
            f.info = tg.createTextInput(`${sportIcon(sport)}${Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(sport)}\n*${loc}* \n${actionTime ? Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(actionTime.replace("Событие: ", "")) : ""}\n\`Событие    \` [${event}](${eventUrl})\n\`Прогноз    \` ${TEXT(infoRow.find(".outcome"))}\n\`Ставка     \` ${TEXT(infoRow.find(".stake"))}\n\`Коэффициент\` ${TEXT(infoRow.find(".odds"))}\n\`Контора    \` [${Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(bookName)}](${bookLink})\n\`Подписка   \` [${Object(_utils_tg_utils__WEBPACK_IMPORTED_MODULE_6__.b)(subscription)}](${eventUrl.replace(/\/picks\/.+/, "")})\n${comment}`.trim(), !0);
        }
        return f.added = enterTime ? function parseDate(s) {
            return new Date(s.replace(/января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря/i, m => months[m]).replace(/[^\w:\+\s]+/g, "").replace(/\s+/g, " "));
        }(enterTime) : new Date, f.subscription = subscription, f;
    }
}, function(module, exports) {
    module.exports = require("koa-body");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Settings", (function() {
        return entities_Settings_Settings;
    })), __webpack_require__.d(__webpack_exports__, "SettingsRepository", (function() {
        return entities_Settings_SettingsRepository;
    }));
    var tslib_es6 = __webpack_require__(0), external_typeorm_ = __webpack_require__(2), ioc = __webpack_require__(18), rnd = __webpack_require__(43);
    let Settings_Settings = class Settings {
        async updateLinkSecret() {
            this.linkSecret = await Object(rnd.a)();
        }
    };
    Object(tslib_es6.a)([ Object(external_typeorm_.PrimaryGeneratedColumn)(), Object(tslib_es6.b)("design:type", Number) ], Settings_Settings.prototype, "id", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: Object(rnd.b)()
    }), Object(tslib_es6.b)("design:type", String) ], Settings_Settings.prototype, "secret", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: Object(rnd.b)()
    }), Object(tslib_es6.b)("design:type", String) ], Settings_Settings.prototype, "linkSecret", void 0), 
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: !1
    }), Object(tslib_es6.b)("design:type", Boolean) ], Settings_Settings.prototype, "installed", void 0), 
    Settings_Settings = Object(tslib_es6.a)([ Object(ioc.a)(null, !0) ], Settings_Settings);
    external_typeorm_.Repository;
    let entities_Settings_Settings = class Settings extends Settings_Settings {};
    Object(tslib_es6.a)([ Object(external_typeorm_.Column)({
        default: "{}",
        type: "simple-json"
    }), Object(tslib_es6.b)("design:type", Object) ], entities_Settings_Settings.prototype, "vk", void 0), 
    entities_Settings_Settings = Object(tslib_es6.a)([ Object(ioc.a)() ], entities_Settings_Settings);
    class entities_Settings_SettingsRepository extends external_typeorm_.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return SingletonEntity;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return SingletonRepository;
    }));
    var typeorm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2), typescript_ioc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1), _ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(46);
    function SingletonEntity(options, disable) {
        if (disable) return function(target) {
            return target;
        };
        {
            const ewrap = Object(typeorm__WEBPACK_IMPORTED_MODULE_0__.Entity)(options);
            return function(target) {
                if (ewrap(target), !target.asyncProvider) {
                    const provider = Object(_ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__.b)(async connection => {
                        const repository = connection.getRepository(target);
                        return await repository.findOne() || await repository.createQueryBuilder().insert().values([ {} ]).execute() && await repository.findOneOrFail();
                    });
                    target.asyncProvider = provider, Object(typescript_ioc__WEBPACK_IMPORTED_MODULE_1__.Provided)(provider)(target);
                }
                return target;
            };
        }
    }
    function SingletonRepository(e) {
        return function(target) {
            return target.provider || Object(typescript_ioc__WEBPACK_IMPORTED_MODULE_1__.Provided)(target.provider = Object(_ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__.a)(connection => connection.getRepository(e)))(target), 
            target;
        };
    }
}, function(module, exports) {
    module.exports = require("child_process");
}, function(module, exports) {
    module.exports = require("make-dir");
}, , function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "BetonAccount", (function() {
        return BetonAccount;
    })), __webpack_require__.d(__webpack_exports__, "BetonAccountRepository", (function() {
        return BetonAccountRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), cheerio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(38), cheerio__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(cheerio__WEBPACK_IMPORTED_MODULE_1__), crypto__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(26), key_file_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(77), key_file_storage__WEBPACK_IMPORTED_MODULE_3___default = __webpack_require__.n(key_file_storage__WEBPACK_IMPORTED_MODULE_3__), node_libcurl__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(60), typeorm__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4), _taraflex_decode_html__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(51), _taraflex_decode_html__WEBPACK_IMPORTED_MODULE_7___default = __webpack_require__.n(_taraflex_decode_html__WEBPACK_IMPORTED_MODULE_7__), _taraflex_string_tools__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3), _utils_beton_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(15), _utils_data_path__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(9), _utils_extendable_error__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(8), _utils_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(7);
    const tokens = key_file_storage__WEBPACK_IMPORTED_MODULE_3___default()(Object(_utils_data_path__WEBPACK_IMPORTED_MODULE_10__.a)("tokens"));
    class AuthRequiredError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_11__.a {}
    class LoginRequiredError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_11__.a {}
    class PasswordRequiredError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_11__.a {}
    class WarnPageError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_11__.a {}
    let BetonAccount = class BetonAccount {
        get hash() {
            if (!this.login) throw new LoginRequiredError;
            if (!this.password) throw new PasswordRequiredError;
            return this.login + "_" + Object(crypto__WEBPACK_IMPORTED_MODULE_2__.createHash)("md5").update(this.password).digest("hex");
        }
        async dropInvalidToken() {
            await async function isValidToken(token) {
                try {
                    if (token) return "valid" == (await Object(_utils_http__WEBPACK_IMPORTED_MODULE_12__.c)("https://bet-hub.com/mobile_check_token/" + token).json()).status;
                } catch {}
                return !1;
            }(tokens[this.hash]) || delete tokens[this.hash];
        }
        async getTokenPayload() {
            const {hash} = this;
            let token = tokens[hash];
            if (token) return token;
            const response = await Object(node_libcurl__WEBPACK_IMPORTED_MODULE_4__.curly)("https://bet-hub.com/mobile_get_token/", {
                httpProxyTunnel: 1,
                httpVersion: node_libcurl__WEBPACK_IMPORTED_MODULE_4__.CurlHttpVersion.V1_1,
                customRequest: "PUT",
                timeout: 60,
                sslVerifyPeer: 0,
                httpHeader: [ "Accept: application/json", "Authorization: Basic " + Buffer.from(this.login + ":" + this.password).toString("base64"), "Content-Type: application/json", "User-Agent: Java/15", "Host: bet-hub.com", "Connection: keep-alive", "Content-Length: 43" ],
                postFields: '{"sKey":"nS96#F@o4y|Pxa7LOgr5eg2JP|#IH8%x"}'
            });
            try {
                token = JSON.parse(response.data).token;
            } catch {}
            if (!token) throw new AuthRequiredError(Object(_taraflex_string_tools__WEBPACK_IMPORTED_MODULE_8__.stringify)(response));
            return tokens[hash] = token, token;
        }
        async loadDom(url, settings, cookieJar) {
            url += (url.includes("?") ? "&" : "?") + "token=" + await this.getTokenPayload() + "&width=1080", 
            settings && (url += "&sr=" + encodeURIComponent((settings.deviceName + settings.deviceSerial).replace(/\s+/g, "")));
            const {body, headers} = await Object(_utils_http__WEBPACK_IMPORTED_MODULE_12__.c)({
                url,
                cookieJar,
                responseType: "buffer"
            }), $ = cheerio__WEBPACK_IMPORTED_MODULE_1___default.a.load(_taraflex_decode_html__WEBPACK_IMPORTED_MODULE_7___default()(body, headers["content-type"]), {
                decodeEntities: !1
            }), warn = $(".picks_warning").text().trim();
            if (warn && 'Нажимая кнопку "Показать" Вы соглашаетесь с фактом использования прогноза. Если кнопка "Показать" неактивна, обновите страницу.' != warn) throw new WarnPageError(warn);
            return $;
        }
        async loadFavoritesLinks(settings, cookieJar) {
            const favs = await this.loadDom("https://bet-hub.com/user/subs_favorites/mobile.php", settings, cookieJar);
            return favs(".capper_L,.capper_UT,.capper_U").map((function(_, e) {
                return {
                    url: `https://bet-hub.com/_seo/subs_mobile.php?q=${e.parent.attribs.href.replace(/beton:\/\/betonapp_(statsall|pickfree)\//, "")}/picks/`,
                    title: favs(e).text()
                };
            })).toArray();
        }
        async actualizeSubscriptions(settings) {
            await this.getTokenPayload();
            const newSubs = new Set((await Promise.all([ "https://bet-hub.com/user/orders_active/mobile.php", "https://bet-hub.com/user/subs_favorites/mobile.php" ].map(url => this.loadDom(url, settings)))).flatMap(doc => Object(_utils_beton_utils__WEBPACK_IMPORTED_MODULE_9__.h)(doc, doc(".capper_L,.capper_UT,.capper_U"))));
            return !(newSubs.size > 0 && this.subscriptions && this.subscriptions.length === newSubs.size && this.subscriptions.every(s => newSubs.has(s))) && (this.subscriptions = Array.from(newSubs), 
            !0);
        }
        async beforeFilter() {
            try {
                await this.actualizeSubscriptions();
            } catch (err) {
                throw await this.dropInvalidToken(), console.error.bind(console, "(./src/entities/BetonAccount.ts:160)")(err), 
                [ {
                    field: "login",
                    message: "Invalid bet-hub Login or Password"
                }, {
                    field: "password",
                    message: "Invalid bet-hub Login or Password"
                } ];
            }
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_5__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], BetonAccount.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_6__.b)({
        empty: !1
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_5__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], BetonAccount.prototype, "login", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_6__.b)({
        empty: !1
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_5__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], BetonAccount.prototype, "password", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_6__.b)({
        state: 34
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_5__.Column)({
        default: "[]",
        type: "simple-json"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], BetonAccount.prototype, "subscriptions", void 0), 
    BetonAccount = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_6__.a)({
        _: 0
    }, {
        icon: "bold",
        order: 5
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_5__.Entity)() ], BetonAccount);
    class BetonAccountRepository extends typeorm__WEBPACK_IMPORTED_MODULE_5__.Repository {}
}, function(module, exports) {
    module.exports = require("path");
}, function(module, exports) {
    module.exports = require("worker_threads");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_exports__.a = () => Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
}, function(module, exports) {
    module.exports = require("crypto");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Forecast", (function() {
        return Forecast;
    })), __webpack_require__.d(__webpack_exports__, "ForecastRepository", (function() {
        return ForecastRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
    let Forecast = class Forecast {
        get subscriptionTitle() {
            return this.subscription.split("│", 1)[0];
        }
        get publishedChannels() {
            return Object.entries(this.publishedTo).filter(p => p[1] > 0).map(p => p[0]);
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Forecast.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Date) ], Forecast.prototype, "added", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Index)(), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Forecast.prototype, "subscription", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98,
        type: "object"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unique: !0,
        default: "{}",
        type: "text",
        transformer: {
            to: JSON.stringify,
            from: JSON.parse
        }
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Object) ], Forecast.prototype, "info", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Index)(), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: !1
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Boolean) ], Forecast.prototype, "published", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98,
        type: "object"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "{}",
        type: "text",
        transformer: {
            to: JSON.stringify,
            from: JSON.parse
        }
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Object) ], Forecast.prototype, "publishedTo", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Forecast.prototype, "replyTo", void 0), 
    Forecast = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        DELETE: 0
    }, {
        icon: "bell",
        order: 15
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], Forecast);
    class ForecastRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports) {
    module.exports = require("koa-router");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    var _taraflex_cancelation_token__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
    function timeFn(connection, resolve) {
        connection.dispose(), resolve();
    }
    __webpack_exports__.a = function(time, cancelationToken) {
        return new Promise(cancelationToken ? (resolve, reject) => {
            const connection = cancelationToken.connect(() => {
                clearTimeout(timeout), reject(new _taraflex_cancelation_token__WEBPACK_IMPORTED_MODULE_0__.CancelationTokenError);
            }), timeout = setTimeout(timeFn, time, connection, resolve);
        } : resolve => {
            setTimeout(resolve, time);
        });
    };
}, function(module, exports) {
    module.exports = require("@taraflex/cancelation-token");
}, function(module, exports) {
    module.exports = require("bcryptjs");
}, function(module, exports) {
    module.exports = require("koa-passport");
}, , function(module, exports) {
    module.exports = require("p-map");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return AsyncUniqQueue;
    }));
    class AsyncUniqQueue {
        constructor() {
            this.objects = [], this.resolvers = [];
        }
        [Symbol.asyncIterator]() {
            return this;
        }
        get size() {
            return this.objects.length;
        }
        add(value, first) {
            this.resolvers.length > 0 ? this.resolvers.shift()({
                done: !1,
                value
            }) : first ? (this.delete(value), this.objects.unshift(value)) : this.objects.includes(value) || this.objects.push(value);
        }
        delete(value) {
            const i = this.objects.indexOf(value);
            i > -1 && this.objects.splice(i, 1);
        }
        next() {
            return new Promise(resolve => {
                this.objects.length > 0 ? resolve({
                    done: !1,
                    value: this.objects.shift()
                }) : this.resolvers.push(resolve);
            });
        }
    }
}, function(module, exports) {
    module.exports = require("base32-encode");
}, function(module, exports) {
    module.exports = require("util");
}, function(module, exports) {
    module.exports = require("cheerio");
}, function(module, exports) {
    module.exports = require("param-case");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Channel", (function() {
        return Channel;
    })), __webpack_require__.d(__webpack_exports__, "ChannelRepository", (function() {
        return ChannelRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
    let Channel = class Channel {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Channel.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 34
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Channel.prototype, "title", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        type: "link"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", String) ], Channel.prototype, "comment", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        state: 98
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "bigint",
        unique: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Channel.prototype, "supergroupId", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        unsigned: !0,
        default: 0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Number) ], Channel.prototype, "timeout", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        remote: "subscription.load",
        items: {
            type: "string",
            empty: !1
        },
        state: 8
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "[]",
        type: "simple-json"
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.b)("design:type", Array) ], Channel.prototype, "subscriptions", void 0), 
    Channel = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.a)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        DELETE: 0,
        PATCH: 0
    }, {
        display: "table",
        icon: "bullhorn",
        order: 10
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], Channel);
    class ChannelRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports) {
    module.exports = require("net");
}, function(module, exports) {
    module.exports = require("ws");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return rndString;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return seedRndString;
    }));
    var base32_encode__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(36), base32_encode__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(base32_encode__WEBPACK_IMPORTED_MODULE_0__), crypto__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(26), node_machine_id__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(82), random_bytes_seed__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(83), random_bytes_seed__WEBPACK_IMPORTED_MODULE_3___default = __webpack_require__.n(random_bytes_seed__WEBPACK_IMPORTED_MODULE_3__), random_seed__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(84), util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(37), _config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5);
    const rndBytes = Object(util__WEBPACK_IMPORTED_MODULE_5__.promisify)(crypto__WEBPACK_IMPORTED_MODULE_1__.randomBytes), STRING_SEED = "beton" + _config__WEBPACK_IMPORTED_MODULE_6__.f + Object(node_machine_id__WEBPACK_IMPORTED_MODULE_2__.machineIdSync)(!0);
    async function rndString() {
        return base32_encode__WEBPACK_IMPORTED_MODULE_0___default()(await rndBytes(23 + ~~(7 * Math.random())), "Crockford").toLowerCase();
    }
    function seedRndString(seed = STRING_SEED) {
        const b = random_bytes_seed__WEBPACK_IMPORTED_MODULE_3___default()(seed)(Object(random_seed__WEBPACK_IMPORTED_MODULE_4__.create)(seed).intBetween(23, 30));
        return base32_encode__WEBPACK_IMPORTED_MODULE_0___default()(b, "Crockford").toLowerCase();
    }
}, , function(module, exports) {
    module.exports = require("url");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    function createAsync(initCb) {
        let instance = null;
        return {
            init: async (...args) => instance || (instance = await initCb(...args)),
            get: () => instance
        };
    }
    function create(initCb) {
        let instance;
        return {
            init: (...args) => instance || (instance = initCb(...args)),
            get: () => instance
        };
    }
    __webpack_require__.d(__webpack_exports__, "b", (function() {
        return createAsync;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return create;
    }));
}, function(module, exports) {
    module.exports = require("sanitize-filename-ts");
}, function(module, exports) {
    module.exports = require("fastest-validator/lib/rules/string");
}, function(module, exports) {
    module.exports = require("builtin-status-codes");
}, function(module, exports) {
    module.exports = require("timers");
}, function(module, exports) {
    module.exports = require("@taraflex/decode-html");
}, function(module, exports) {
    module.exports = require("vm");
}, function(module, exports) {
    module.exports = require("async-mutex");
}, function(module, exports) {
    module.exports = require("tmp-promise");
}, function(module, exports) {
    module.exports = require("pluralize");
}, function(module, exports) {
    module.exports = require("etag");
}, function(module, exports) {
    module.exports = require("msgpack-lite");
}, function(module, exports) {
    module.exports = require("sentence-case");
}, function(module, exports) {
    module.exports = require("marked");
}, function(module, exports) {
    module.exports = require("node-libcurl");
}, function(module, exports) {
    module.exports = require("node-zone");
}, function(module, exports) {
    module.exports = require("aes-es");
}, function(module, exports) {
    module.exports = require("set-utils");
}, function(module, exports) {
    module.exports = require("playwright-firefox");
}, function(module, exports) {
    module.exports = require("@vk-io/authorization");
}, function(module, exports) {
    module.exports = require("tough-cookie");
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, SAVE, can, ctx, isEditableField, isExtendableArrayField, isVisibleField, props) {
            const {sentenceCase} = __webpack_require__(58);
            let marked = __webpack_require__(59);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>";
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.component = pug_interp = function(v, i, row) {
                this && this.block, this && this.attributes;
                switch (row = row || "item", v.type) {
                  case "string":
                    pug_mixins.input(v, row, i);
                    break;

                  case "array":
                    pug_mixins.array(v, row, i);
                    break;

                  case "files":
                    pug_mixins.files(v, row, i);
                    break;

                  case "date":
                    pug_mixins.date(v, row, i);
                    break;

                  case "number":
                    pug_mixins.number(v, row, i);
                    break;

                  case "enum":
                    pug_mixins.enum(v, row, i);
                    break;

                  case "boolean":
                    pug_mixins.boolean(v, row, i);
                    break;

                  case "wysiwyg":
                    pug_mixins.wysiwyg(v, row, i);
                    break;

                  case "md-tgclient":
                    pug_mixins.code(v, row, i, "md-tgclient");
                    break;

                  case "md-tgbotlegacy":
                    pug_mixins.code(v, row, i, "md-tgbotlegacy");
                    break;

                  case "regex":
                    pug_mixins.code(v, row, i, "regex");
                    break;

                  case "javascript":
                    pug_mixins.mn(v, row, i, "javascript");
                    break;

                  case "link":
                    pug_mixins.link(v, row, i);
                    break;

                  default:
                    pug_html += "<span>Unsupported type</span>";
                }
            }, pug_html += '<div class="rg-data_single" v-if="!!values[0]">';
            let i = 0;
            (function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isVisibleField(v) && (pug_html = pug_html + "<div> <p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                    pug_mixins.component(v, i, "values[0]"), pug_html += "</div>"), ++i;
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isVisibleField(v) && (pug_html = pug_html + "<div> <p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                        pug_mixins.component(v, i, "values[0]"), pug_html += "</div>"), ++i;
                    }
                }
            }).call(this), can.PATCH && (pug_html = pug_html + '<p> <el-button @click="save(values[0])" type="primary"' + pug.attr("round", !0, !0, !0) + ' icon="el-icon-check"' + pug.attr(":loading", "values[0].state==" + SAVE, !0, !0) + ">Save</el-button></p>"), 
            pug_html += "</div>";
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "SAVE" in locals_for_with ? locals_for_with.SAVE : "undefined" != typeof SAVE ? SAVE : void 0, "can" in locals_for_with ? locals_for_with.can : "undefined" != typeof can ? can : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0, "isVisibleField" in locals_for_with ? locals_for_with.isVisibleField : "undefined" != typeof isVisibleField ? isVisibleField : void 0, "props" in locals_for_with ? locals_for_with.props : "undefined" != typeof props ? props : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, NONE, Object, REMOVE, SAVE, can, ctx, findPrimaryIndex, isEditableField, isExtendableArrayField, isFullwidthField, isNormalField, props) {
            const {sentenceCase} = __webpack_require__(58);
            let marked = __webpack_require__(59);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, $s = state => `(item.state==${state})`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            let display = null;
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                "table" === display ? (pug_html = pug_html + "<td" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></td>") : (pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>");
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.component = pug_interp = function(v, i, row) {
                this && this.block, this && this.attributes;
                switch (row = row || "item", v.type) {
                  case "string":
                    pug_mixins.input(v, row, i);
                    break;

                  case "array":
                    pug_mixins.array(v, row, i);
                    break;

                  case "files":
                    pug_mixins.files(v, row, i);
                    break;

                  case "date":
                    pug_mixins.date(v, row, i);
                    break;

                  case "number":
                    pug_mixins.number(v, row, i);
                    break;

                  case "enum":
                    pug_mixins.enum(v, row, i);
                    break;

                  case "boolean":
                    pug_mixins.boolean(v, row, i);
                    break;

                  case "wysiwyg":
                    pug_mixins.wysiwyg(v, row, i);
                    break;

                  case "md-tgclient":
                    pug_mixins.code(v, row, i, "md-tgclient");
                    break;

                  case "md-tgbotlegacy":
                    pug_mixins.code(v, row, i, "md-tgbotlegacy");
                    break;

                  case "regex":
                    pug_mixins.code(v, row, i, "regex");
                    break;

                  case "javascript":
                    pug_mixins.mn(v, row, i, "javascript");
                    break;

                  case "link":
                    pug_mixins.link(v, row, i);
                    break;

                  default:
                    pug_html += "<span>Unsupported type</span>";
                }
            }, pug_mixins.controlls = pug_interp = function() {
                var state;
                this && this.block, this && this.attributes;
                (can.CHANGE || can.DELETE) && (pug_html += '<td class="rg-data__small-cell"><el-button-group>', 
                can.CHANGE && (pug_html += "<keep-alive>", can.PATCH && (pug_html = pug_html + "<el-button" + pug.attr("v-if", $s(NONE), !0, !0) + ' @click="edit(item)" type="primary" icon="el-icon-edit"' + pug.attr(":disabled", $s(REMOVE), !0, !0) + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + ">                           </el-button>"), 
                pug_html = pug_html + "<el-button" + pug.attr("v-if", (state = NONE, `(item.state!=${state})`), !0, !0) + ' @click="save(item)" type="primary" icon="el-icon-check"' + pug.attr(":loading", $s(SAVE), !0, !0) + pug.attr(":disabled", $s(REMOVE), !0, !0) + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + "></el-button></keep-alive>"), 
                can.DELETE && (pug_html = pug_html + '<el-button @click="remove(item, index)" type="danger" icon="el-icon-delete"' + pug.attr(":disabled", $s(SAVE), !0, !0) + pug.attr(":loading", $s(REMOVE), !0, !0) + pug.attr("circle", can.CHANGE, !0, !0) + pug.attr("round", !can.CHANGE, !0, !0) + "></el-button>"), 
                pug_html += "</el-button-group></td>");
            }, pug_html += '<div class="rg-data" v-loading="!inited" ref="table"><table>';
            const hasNormalFields = Object.values(props).some(isNormalField), hasFullWidth = Object.values(props).some(isFullwidthField), hasNormalRow = can.CHANGE || can.DELETE || hasNormalFields;
            let i = 0;
            hasNormalRow && !hasFullWidth ? (pug_html = pug_html + '<tbody><tr class="rg-draggable" v-for="(item, index) in values"' + pug.attr(":key", "item[" + findPrimaryIndex(props) + "]", !0, !0) + ">       ", 
            display = "table", function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var v = $$obj[pug_index2];
                    isNormalField(v) && pug_mixins.component(v, i), ++i;
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        v = $$obj[pug_index2];
                        isNormalField(v) && pug_mixins.component(v, i), ++i;
                    }
                }
            }.call(this), pug_mixins.controlls(), pug_html += "</tr></tbody>") : (pug_html = pug_html + '<tbody class="rg-draggable" v-for="(item, index) in values"' + pug.attr(":key", "item[" + findPrimaryIndex(props) + "]", !0, !0) + "> ", 
            hasNormalRow && (pug_html += "<tr>       ", display = "table", i = 0, function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var v = $$obj[pug_index3];
                    isNormalField(v) && pug_mixins.component(v, i), ++i;
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        v = $$obj[pug_index3];
                        isNormalField(v) && pug_mixins.component(v, i), ++i;
                    }
                }
            }.call(this), pug_mixins.controlls(), pug_html += "</tr>"), hasFullWidth && (display = "single", 
            i = 0, function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isFullwidthField(v) && (pug_html = pug_html + '<tr><td colspan="999"><div class="rg-subtitle">' + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</div>", 
                    pug_mixins.component(v, i), pug_html += "</td></tr>"), ++i;
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isFullwidthField(v) && (pug_html = pug_html + '<tr><td colspan="999"><div class="rg-subtitle">' + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</div>", 
                        pug_mixins.component(v, i), pug_html += "</td></tr>"), ++i;
                    }
                }
            }.call(this)), pug_html += "</tbody>"), hasNormalFields && (pug_html += '<thead ref="tableHeader"><tr>', 
            function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isNormalField(v) && (pug_html = pug_html + "<th>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</th>");
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isNormalField(v) && (pug_html = pug_html + "<th>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</th>");
                    }
                }
            }.call(this), (can.CHANGE || can.DELETE) && (pug_html += '<td class="rg-data__small-cell"><el-button-group v-if="values.length&gt;1">', 
            can.CHANGE && (pug_html = pug_html + '<el-button :disabled="!hasUnsaved" @click="saveAll" type="primary" icon="el-icon-check"' + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + "></el-button>"), 
            can.DELETE && (pug_html = pug_html + '<el-button @click="removeAll" type="danger" icon="el-icon-delete"' + pug.attr("circle", can.CHANGE, !0, !0) + pug.attr("round", !can.CHANGE, !0, !0) + "></el-button>"), 
            pug_html += "</el-button-group></td>"), pug_html += "</tr></thead>"), pug_html += '</table><div class="rg-spacer"></div>', 
            can.PUT && (pug_html += '<div class="rg-controlls-panel"><div class="rg-spacer"></div><el-button @click="add" type="primary" round icon="el-icon-plus">New</el-button></div>'), 
            pug_html += "</div>";
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "NONE" in locals_for_with ? locals_for_with.NONE : "undefined" != typeof NONE ? NONE : void 0, "Object" in locals_for_with ? locals_for_with.Object : "undefined" != typeof Object ? Object : void 0, "REMOVE" in locals_for_with ? locals_for_with.REMOVE : "undefined" != typeof REMOVE ? REMOVE : void 0, "SAVE" in locals_for_with ? locals_for_with.SAVE : "undefined" != typeof SAVE ? SAVE : void 0, "can" in locals_for_with ? locals_for_with.can : "undefined" != typeof can ? can : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "findPrimaryIndex" in locals_for_with ? locals_for_with.findPrimaryIndex : "undefined" != typeof findPrimaryIndex ? findPrimaryIndex : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0, "isFullwidthField" in locals_for_with ? locals_for_with.isFullwidthField : "undefined" != typeof isFullwidthField ? isFullwidthField : void 0, "isNormalField" in locals_for_with ? locals_for_with.isNormalField : "undefined" != typeof isNormalField ? isNormalField : void 0, "props" in locals_for_with ? locals_for_with.props : "undefined" != typeof props ? props : void 0), 
        pug_html;
    };
}, function(module, exports) {
    module.exports = require("global-agent");
}, function(module, exports) {
    module.exports = require("dns");
}, function(module, exports) {
    module.exports = require("http");
}, function(module, exports) {
    module.exports = require("koa-mount");
}, function(module, exports) {
    module.exports = require("koa-send");
}, function(module, exports) {
    module.exports = require("os");
}, function(module, exports) {
    module.exports = require("got");
}, function(module, exports) {
    module.exports = require("fast-levenshtein");
}, function(module, exports) {
    module.exports = require("key-file-storage");
}, function(module, exports) {
    module.exports = require("fastest-validator");
}, function(module, exports) {
    module.exports = require("fastest-validator/lib/rules/array");
}, function(module, exports) {
    module.exports = require("js-beautify");
}, function(module, exports) {
    module.exports = require("appdirectory");
}, function(module, exports) {
    module.exports = require("node-machine-id");
}, function(module, exports) {
    module.exports = require("random-bytes-seed");
}, function(module, exports) {
    module.exports = require("random-seed");
}, function(module, exports) {
    module.exports = require("raw-body");
}, function(module, exports) {
    module.exports = require("tunnel");
}, function(module, exports) {
    module.exports = require("backo2");
}, function(module, exports) {
    module.exports = require("last-match");
}, function(module, exports) {
    module.exports = require("p-filter");
}, function(module, exports) {
    module.exports = require("emittery");
}, function(module, exports) {
    module.exports = require("stream/promises");
}, function(module, exports) {
    module.exports = require("base32768");
}, function(module, exports) {
    module.exports = require("prebuilt-tdlib");
}, function(module, exports) {
    module.exports = require("tdl");
}, function(module, exports) {
    module.exports = require("tdl-tdlib-ffi");
}, function(module, exports) {
    module.exports = require("@msgpack/msgpack");
}, function(module, exports) {
    module.exports = require("koa-compress");
}, function(module, exports) {
    module.exports = require("path-to-regexp");
}, function(module, exports) {
    module.exports = require("koa-compose");
}, function(module, exports) {
    module.exports = require("koa-session");
}, function(module, exports) {
    module.exports = require("passport-local");
}, function(module, exports) {
    module.exports = require("koa");
}, function(module, exports, __webpack_require__) {
    var H = __webpack_require__(130);
    module.exports = function() {
        var T = new H.Template({
            code: function(c, p, i) {
                var t = this;
                return t.b(i = i || ""), t.b("window.Paths = Object.freeze({"), t.b("\n" + i), t.s(t.f("Paths", c, p, 1), c, p, 0, 45, 78, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                    t.b("    '"), t.b(t.t(t.f("name", c, p, 0))), t.b("':"), t.b(t.t(t.f("url", c, p, 0))), 
                    t.b(","), t.b("\n" + i);
                })), c.pop()), t.b("});"), t.b("\n" + i), t.b("window.UserInfo = Object.freeze({email:'"), 
                t.b(t.t(t.f("email", c, p, 0))), t.b("'});"), t.b("\n" + i), t.b("window.Flash = Object.freeze("), 
                t.b(t.t(t.f("Flash", c, p, 0))), t.b(");"), t.b("\n" + i), t.b("window.EDITOR_WORKER = '"), 
                t.b(t.t(t.f("EDITOR_WORKER", c, p, 0))), t.b("';"), t.b("\n" + i), t.b("window.TYPESCRIPT_WORKER = '"), 
                t.b(t.t(t.f("TYPESCRIPT_WORKER", c, p, 0))), t.b("';"), t.b("\n" + i), t.b("window.HasPty = "), 
                t.b(t.t(t.f("HasPty", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("window.EntitiesFactory = function(mixins, debounce) {"), 
                t.b("\n" + i), t.b("    return ["), t.b("\n" + i), t.s(t.f("components", c, p, 1), c, p, 0, 404, 1317, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                    t.b("Object.assign({"), t.b("\n" + i), t.b("    name:'"), t.b(t.t(t.f("name", c, p, 0))), 
                    t.b("',"), t.b("\n" + i), t.b("    icon:'"), t.b(t.t(t.f("icon", c, p, 0))), t.b("',"), 
                    t.b("\n" + i), t.b("    order:"), t.b(t.t(t.f("order", c, p, 0))), t.b(","), t.b("\n" + i), 
                    t.b("    mixins:mixins,"), t.b("\n" + i), t.b("    data:function(){return{"), t.b("\n" + i), 
                    t.b("        draggableOptions:Object.freeze({disabled:!"), t.b(t.t(t.f("sortable", c, p, 0))), 
                    t.b(",draggable:'.rg-draggable',ghostClass:'rg-drag-ghost',animation:0}),"), t.b("\n" + i), 
                    t.b("        values:[],"), t.b("\n" + i), t.s(t.f("remote", c, p, 1), c, p, 0, 708, 755, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                        t.b("        "), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), t.b(t.t(t.f("method", c, p, 0))), 
                        t.b("_data:[],"), t.b("\n" + i);
                    })), c.pop()), t.b("        inited:false"), t.b("\n" + i), t.b("    }},"), t.b("\n" + i), 
                    t.b("    beforeCreate:function(){this.endPoint='"), t.b(t.t(t.f("endPoint", c, p, 0))), 
                    t.b("';this.hooks=Object.freeze("), t.b(t.t(t.f("hooks", c, p, 0))), t.b(");this.can=Object.freeze("), 
                    t.b(t.t(t.f("can", c, p, 0))), t.b(");this.rtti=Object.freeze("), t.b(t.t(t.f("rtti", c, p, 0))), 
                    t.b(");},"), t.b("\n" + i), t.b("    methods:{"), t.b("\n" + i), t.s(t.f("remote", c, p, 1), c, p, 0, 999, 1281, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                        t.b("        'call_"), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), t.b(t.t(t.f("method", c, p, 0))), 
                        t.b("':debounce(function(){"), t.b("\n" + i), t.b("            var self = this;"), 
                        t.b("\n" + i), t.b("            this.$rpc.remote('"), t.b(t.t(t.f("c", c, p, 0))), 
                        t.b("')."), t.b(t.t(t.f("method", c, p, 0))), t.b(".apply(this.$rpc,arguments).then(function(data){"), 
                        t.b("\n" + i), t.b("                self."), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), 
                        t.b(t.t(t.f("method", c, p, 0))), t.b("_data=data;"), t.b("\n" + i), t.b("            });"), 
                        t.b("\n" + i), t.b("        }, 500),"), t.b("\n" + i);
                    })), c.pop()), t.b("    }"), t.b("\n" + i), t.b("}, "), t.b(t.t(t.f("render", c, p, 0))), 
                    t.b("),"), t.b("\n" + i);
                })), c.pop()), t.b("    ]"), t.b("\n" + i), t.b("};"), t.fl();
            },
            partials: {},
            subs: {}
        });
        return T.render.apply(T, arguments);
    };
}, function(module, exports) {
    module.exports = require("vue-template-compiler");
}, function(module, exports) {
    module.exports = require("vue-template-es2015-compiler");
}, function(module, exports) {
    module.exports = require("alphanum-sort");
}, function(module, exports) {
    module.exports = require("fast-glob");
}, function(module, exports) {
    module.exports = require("time-stamp");
}, function(module, exports) {
    module.exports = require("mailparser");
}, function(module, exports) {
    module.exports = require("stream");
}, function(module, exports) {
    module.exports = require("vk-io");
}, , , function(module, exports) {
    module.exports = require("pretty-error");
}, function(module, exports) {
    module.exports = require("node-pty");
}, function(module, exports) {
    module.exports = require("reflect-metadata");
}, function(module, exports, __webpack_require__) {
    var map = {
        "./base.pug": 118,
        "./baseform.pug": 119,
        "./checkpass.pug": 120,
        "./components/mixins.pug": 121,
        "./components/single.pug": 67,
        "./components/table.pug": 68,
        "./index.pug": 122,
        "./install.pug": 123,
        "./login.pug": 124,
        "./newpassword.pug": 125,
        "./resetemail.pug": 126,
        "./resetpass-sended.pug": 127,
        "./resetpass.pug": 128,
        "./submitform.pug": 129
    };
    function webpackContext(req) {
        var id = webpackContextResolve(req);
        return __webpack_require__(id);
    }
    function webpackContextResolve(req) {
        if (!__webpack_require__.o(map, req)) {
            var e = new Error("Cannot find module '" + req + "'");
            throw e.code = "MODULE_NOT_FOUND", e;
        }
        return map[req];
    }
    webpackContext.keys = function webpackContextKeys() {
        return Object.keys(map);
    }, webpackContext.resolve = webpackContextResolve, module.exports = webpackContext, 
    webpackContext.id = 117;
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(assetsUrl, favicon, scripts, styles, title) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html += "</head><body>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var s = $$obj[pug_index1];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index1];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, title, warning) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            title && (pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>"), 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html += "</form></div></div></div></div>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Repeate password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.passwordField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, ctx, isEditableField, isExtendableArrayField) {
            const {sentenceCase} = __webpack_require__(58);
            let marked = __webpack_require__(59);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>";
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && "call_" + v.remote.replace(".", "_"), !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            };
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(assetsUrl, favicon, inlineScript, scripts, styles, title) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html += '</head><body><div id="app"></div>', inlineScript && (pug_html = pug_html + "<script>" + (null == (pug_interp = inlineScript) ? "" : pug_interp) + "<\/script>"), 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var s = $$obj[pug_index1];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index1];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "inlineScript" in locals_for_with ? locals_for_with.inlineScript : "undefined" != typeof inlineScript ? inlineScript : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Installing";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField("Your email address (for password recovery)"), 
            pug_mixins.passwordField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = "Login") ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = "Login") ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = "Login".toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField(), pug_mixins.passwordField(), pug_html += '<div class="form-group"><a href="resetpass" style="width:100%;text-align:center;display:inline-block;text-decoration:none;">Forgot password?</a></div><br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "New password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.passwordField("New password (5-70 symbols)"), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "New email";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField("New email"), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, service, styles, warning) {
            pug_html += "<!DOCTYPE html><html>";
            const title = "Reset password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html = pug_html + '<div class="form-group">Письмо с инструкциями по изменению пароля отправлено на ' + pug.escape(null == (pug_interp = email) ? "" : pug_interp) + "</div>", 
            service && (pug_html = pug_html + '<div class="form-group"><a class="btn btn-block btn-primary btn-lg"' + pug.attr("href", service.url, !0, !0) + ">Открыть " + pug.escape(null == (pug_interp = service.title) ? "" : pug_interp) + "</a></div>"), 
            pug_html += "</form></div></div></div></div>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "service" in locals_for_with ? locals_for_with.service : "undefined" != typeof service ? service : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Reset password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(14);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, title, warning) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            title && (pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>"), 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports) {
    module.exports = require("hogan.js");
}, , function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    var tslib_es6 = __webpack_require__(0), external_child_process_ = __webpack_require__(19), external_child_process_default = __webpack_require__.n(external_child_process_), external_global_agent_ = __webpack_require__(69), config = __webpack_require__(5);
    const DEV_NULL = process.platform.startsWith("win") ? "nul" : "/dev/null", cmd = "grep -i '^Port\\|^BasicAuth' /etc/tinyproxy/tinyproxy.conf", {Port, BasicAuth} = JSON.parse("{" + Object(external_child_process_.execSync)(config.e ? `ssh -o UserKnownHostsFile=${DEV_NULL} -o StrictHostKeyChecking=no -o BatchMode=yes -i "${config.e}" root@${config.g} "${cmd}"` : cmd, {
        encoding: "utf-8",
        windowsHide: !0
    }).trim().split("\n").map(v => v.replace(/\s+/, ":").replace(/([^:]+)/g, '"$1"')) + "}"), PROXY_HOST = config.g, PROXY_PORT = Port, PROXY_AUTH = BasicAuth.replace(/\s+/, ":"), PROXY_URL = `http://${PROXY_AUTH}@${PROXY_HOST}:${PROXY_PORT}`;
    if ("object" != typeof v8debug && !/--debug|--inspect/.test(process.execArgv.join(" "))) {
        const PrettyError = __webpack_require__(114), pe = new PrettyError;
        pe.withoutColors(), pe.start();
    }
    config.e ? (global.SSH_TUNNEL = Object(external_child_process_.spawn)("ssh", [ "-o", "UserKnownHostsFile=" + DEV_NULL, "-o", "StrictHostKeyChecking=no", "-o", "BatchMode=yes", "-gnNT", "-R", config.f + ":localhost:" + config.f, "-i", config.e, "root@" + config.g ], {
        windowsHide: !0,
        stdio: "inherit"
    }).once("exit", code => process.exit(code)), Object(external_global_agent_.bootstrap)(), 
    global.GLOBAL_AGENT.NO_PROXY = "localhost,127.0.0.1,ipecho.net,ident.me,icanhazip.com", 
    console.log.bind(console, "(./src/core/preload.ts:24)")("Active proxy url: " + (global.GLOBAL_AGENT.HTTP_PROXY = PROXY_URL))) : console.log.bind(console, "(./src/core/preload.ts:26)")("Proxy url: " + PROXY_URL);
    var external_dns_ = __webpack_require__(70), external_http_ = __webpack_require__(71), external_koa_mount_ = __webpack_require__(72), external_koa_mount_default = __webpack_require__.n(external_koa_mount_), external_koa_send_ = __webpack_require__(73), external_koa_send_default = __webpack_require__.n(external_koa_send_), external_path_ = __webpack_require__(23), external_timers_ = __webpack_require__(50), external_typescript_ioc_ = __webpack_require__(1), external_util_ = __webpack_require__(37), external_ws_ = __webpack_require__(42), external_ws_default = __webpack_require__.n(external_ws_), string_tools_ = __webpack_require__(3), external_os_ = __webpack_require__(74);
    const shell = Object(external_os_.platform)().includes("win") ? "cmd.exe" : function hasTmux() {
        try {
            return Object(external_child_process_.execSync)("command -v tmux", {
                windowsHide: !0
            }), !0;
        } catch {
            return !1;
        }
    }() ? "tmux" : "sh";
    let spawn = null;
    try {
        spawn = __webpack_require__(115).spawn;
    } catch {}
    const AVALIBLE = !!spawn;
    var external_msgpack_lite_ = __webpack_require__(57);
    const opts = {
        codec: Object(external_msgpack_lite_.createCodec)({
            preset: !0,
            safe: !1,
            useraw: !1,
            int64: !1,
            binarraybuffer: !0,
            uint8array: !1,
            usemap: !1
        })
    };
    function decode(data) {
        return Object(external_msgpack_lite_.decode)(data, opts);
    }
    function encode(o) {
        return Object(external_msgpack_lite_.encode)(o, opts);
    }
    var rnd_id = __webpack_require__(25);
    class RPC_AnswerChain extends class Chain {
        write(o) {
            this.readResolve && (this.readResolve(o), this.readResolve = null);
        }
        read() {
            return new Promise(resolve => this.readResolve = resolve);
        }
    } {
        constructor(i, timeout) {
            super(), this.i = i, this.timeout = timeout, this.created = Date.now();
        }
    }
    const CALLBACK_SOURCES = new Map, CALLS = new Map, chainDropInterval = setInterval(() => {
        const now = Date.now();
        CALLS.forEach(c => {
            c.timeout && now - c.created > c.timeout && c.write({
                i: c.i,
                e: "RPC call timeout"
            });
        });
    }, 1e4);
    chainDropInterval.unref();
    class RPC_RemoteHandler {
        constructor(key, rpc, timeout) {
            this.key = key, this.rpc = rpc, this.timeout = timeout;
        }
        get(_, f) {
            return (...a) => {
                let i = 0;
                do {
                    i = Object(rnd_id.a)();
                } while (CALLS.has(i));
                const h = new RPC_AnswerChain(i, 1e3 * this.timeout);
                try {
                    CALLS.set(i, h), this.rpc.send(encode({
                        i,
                        c: this.key,
                        f,
                        a
                    }));
                } catch (e) {
                    throw CALLS.delete(i), e;
                }
                return h.read().then(answer => {
                    if (null != answer.e) throw answer.e;
                    return answer.r;
                }).finally(() => CALLS.delete(i));
            };
        }
    }
    const EMPTY_OBJECT = Object.freeze(Object.create(null));
    class RPC_RPC {
        static register(key, o) {
            CALLBACK_SOURCES.set(key, o);
        }
        static unregister(key) {
            CALLBACK_SOURCES.delete(key);
        }
        remote(key, timeout) {
            return new Proxy(EMPTY_OBJECT, new RPC_RemoteHandler(key, this, timeout || 0));
        }
        async process(message) {
            if (message) if (message.constructor === String) this.term && this.term.write(message); else {
                const data = decode(new Uint8Array(message, 0));
                if (data.f) {
                    const {i, f, c, a} = data;
                    try {
                        if ("RPC" === c) {
                            if ("enableTerminal" !== f) throw `Deny method [${f}] called on RPC`;
                            this.send(encode({
                                i,
                                r: this.enableTerminal(a[0])
                            }));
                        } else this.send(encode({
                            i,
                            r: await this.rcall(CALLBACK_SOURCES.get(c), f, a)
                        }));
                    } catch (e) {
                        this.send(encode({
                            i,
                            e
                        }));
                    }
                } else CALLS.get(data.i).write(data), CALLS.delete(data.i);
            }
        }
    }
    const RPCServers = new Map;
    function pingServers(timeout) {
        RPCServers.forEach(m => m.forEach(rpc => rpc.ping(timeout)));
    }
    function fromCtx(ctx) {
        return RPCServers.get(ctx.state.user.id).get(parseInt(ctx.headers["x-tab"]));
    }
    class ServerRPCHandler {
        constructor(server) {
            this.server = server;
        }
        get(target, prop) {
            return "__rpc" == prop ? this.server : target[prop];
        }
    }
    class ServerRPC_ServerRPC extends RPC_RPC {
        constructor(socket, tab, browserId, user, terminalSize) {
            if (super(), this.socket = socket, this.tab = tab, this.browserId = browserId, this.user = user, 
            this.lastActive = Date.now(), this.onWSMessage = message => {
                this.lastActive = Date.now(), this.process(message).catch(console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:49)"));
            }, this.onWSClose = () => {
                try {
                    this.dispose();
                } catch (err) {
                    console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:56)")(err);
                }
            }, this.onWSPong = () => {
                this.lastActive = Date.now();
            }, tab !== +tab || !tab) throw "Invalid websocket tab: " + Object(string_tools_.stringify)(tab);
            let remoteTabs = RPCServers.get(user.id);
            remoteTabs || RPCServers.set(user.id, remoteTabs = new Map), remoteTabs.set(tab, this), 
            socket.on("close", this.onWSClose), socket.on("pong", this.onWSPong), socket.on("message", this.onWSMessage), 
            terminalSize && this.enableTerminal(terminalSize);
        }
        getActual() {
            var _a;
            const r = null === (_a = RPCServers.get(this.user.id)) || void 0 === _a ? void 0 : _a.get(this.tab);
            return r !== this && this.dispose(), r;
        }
        send(data) {
            var _a;
            null === (_a = this.getActual()) || void 0 === _a || _a.socket.send(data);
        }
        enableTerminal(terminalSize) {
            if (this.term) this.term.resize(terminalSize.cols, terminalSize.rows); else {
                const term = function create(options) {
                    return AVALIBLE ? spawn(shell, "tmux" === shell ? [ "new", "-A", "-s", "beton-" + config.f ] : [], {
                        name: "xterm-color",
                        cwd: process.env.HOME,
                        env: process.env,
                        ...options
                    }) : null;
                }(terminalSize);
                term && (this.term = term, this.socket.send(String.fromCharCode(27) + "c"), term.onData(data => {
                    try {
                        this.socket.send(data);
                    } catch (err) {
                        console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:116)")(err);
                    }
                }));
            }
        }
        rcall(o, fname, args) {
            return o[fname].apply(new Proxy(o, new ServerRPCHandler(this)), args);
        }
        dispose() {
            this.term && (this.term.kill(), this.term = void 0);
            const c = RPCServers.get(this.user.id);
            (null == c ? void 0 : c.get(this.tab)) === this && c.delete(this.tab), this.socket.readyState != external_ws_default.a.CLOSING && this.socket.readyState != external_ws_default.a.CLOSED && this.socket.close();
        }
        ping(timeout) {
            Date.now() - this.lastActive >= timeout && this.socket.readyState === external_ws_default.a.OPEN && this.socket.ping();
        }
    }
    var external_net_ = __webpack_require__(41), http = __webpack_require__(7), get_my_ip = async function() {
        let ip = "";
        try {
            if (ip = (await Object(http.c)("http://ipecho.net/plain").text()).trim(), !Object(external_net_.isIP)(ip)) throw null;
        } catch {
            try {
                if (ip = (await Object(http.c)("http://ident.me").text()).trim(), !Object(external_net_.isIP)(ip)) throw null;
            } catch {
                if (ip = (await Object(http.c)("http://icanhazip.com").text()).trim(), !Object(external_net_.isIP)(ip)) throw "Invalid ip: " + ip;
            }
        }
        return ip;
    }, external_fast_levenshtein_ = __webpack_require__(76), types = __webpack_require__(11), BetonAccount = __webpack_require__(22), ClientSettings = __webpack_require__(10), Settings = __webpack_require__(17), external_raw_body_ = __webpack_require__(85), external_raw_body_default = __webpack_require__.n(external_raw_body_);
    var body_parse_msgpack = async (ctx, next) => {
        const {method} = ctx.request;
        if ("PATCH" === method || "PUT" === method || "POST" === method) {
            const body = await external_raw_body_default()(ctx.req, {
                limit: 5242880
            });
            ctx.request.body = Object.assign(body && body.length > 0 ? decode(body) : null, ctx.request.query);
        }
        return next();
    }, is_ajax = function(ctx) {
        return "XMLHttpRequest" === ctx.request.headers["x-requested-with"];
    }, check_entity_access = ({checkAccess}) => function(ctx, next) {
        if (!is_ajax(ctx)) throw 403;
        return checkAccess(ctx.request.method, ctx.state.user), next();
    }, external_url_ = __webpack_require__(45), resolve_url = function(ctx, target, localhost) {
        return Object(external_url_.resolve)(ctx.protocol + "://" + (localhost && !ctx.secure ? "127.0.0.1:" + config.f : ctx.host), target.replace(/[\/\\]{2,}/g, "/"));
    };
    function setNoStoreHeader(ctx) {
        ctx.set("Cache-Control", "no-store, no-cache, max-age=0");
    }
    var no_store = function(ctx, next) {
        return setNoStoreHeader(ctx), next();
    };
    const routsCache = Object.create(null), secureRoutsCache = Object.create(null);
    var smart_redirect = (ctx, next) => {
        const rcache = ctx.secure ? secureRoutsCache : routsCache;
        return ctx.resolve = (routeName, params) => !params && ctx.router.stack.find(r => r.name === routeName).paramNames.length < 1 ? rcache[routeName] || (rcache[routeName] = resolve_url(ctx, ctx.router.url(routeName, null))) : resolve_url(ctx, ctx.router.url(routeName, Object.assign({}, ctx.params, params))), 
        ctx.namedRedirect = (routName, delay, params) => {
            setNoStoreHeader(ctx);
            const target = ctx.resolve(routName || "root", params);
            delay > 0 ? (ctx.type = "html", ctx.body = `<html><head><meta http-equiv="refresh" content="${delay};url=${target}" /></head></html>`) : ctx.redirect(target);
        }, next();
    }, external_koa_router_ = __webpack_require__(28), external_koa_router_default = __webpack_require__.n(external_koa_router_), external_param_case_ = __webpack_require__(39);
    function route(method, params, ...middlewares) {
        return (target, prop) => {
            (target.constructor.__routes_info__ || (target.constructor.__routes_info__ = [])).push({
                method,
                name: params && params.name,
                path: params && params.path || "/" + Object(external_param_case_.paramCase)(prop),
                prop,
                middlewares
            });
        };
    }
    function applyRoutes(router, routes) {
        for (let {method, name, path, prop, middlewares} of routes.constructor.__routes_info__) "function" == typeof path && (path = path.call(routes)), 
        name ? router[method](name, path, ...middlewares, routes[prop].bind(routes)) : router[method](path, ...middlewares, routes[prop].bind(routes));
        return router;
    }
    const get = route.bind(null, "get"), post = (route.bind(null, "head"), route.bind(null, "post")), put = route.bind(null, "put");
    route.bind(null, "delete"), route.bind(null, "patch"), route.bind(null, "all");
    var User = __webpack_require__(13);
    async function sendEmail(email, subject, message) {
        try {
            await Object(http.c)("https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec", {
                method: "POST",
                form: {
                    email,
                    body: message,
                    subject
                },
                throwHttpErrors: !1,
                resolveBodyOnly: !0
            });
        } catch (err) {
            throw err && console.error.bind(console, "(./src/core/utils/send-email.ts:21)")(err), 
            "Email sending error. Try again later.";
        }
    }
    async function sendErrorEmail(message) {
        try {
            const ur = external_typescript_ioc_.Container.get(User.UserRepository);
            await sendEmail((await ur.findOne()).email, "Ошибка в работе бота http://" + config.d, message);
        } catch {}
    }
    var external_base32_encode_ = __webpack_require__(36), external_base32_encode_default = __webpack_require__.n(external_base32_encode_), external_crypto_ = __webpack_require__(26), external_koa_body_ = __webpack_require__(16), external_koa_body_default = __webpack_require__.n(external_koa_body_), external_tunnel_ = __webpack_require__(86), external_tunnel_default = __webpack_require__.n(external_tunnel_), utils_delay = __webpack_require__(29), extendable_error = __webpack_require__(8);
    class deffered_DefferTimeout extends extendable_error.a {}
    function makeDeffer(timeout) {
        const o = {
            createTime: Date.now(),
            promise: null,
            fulfill: null,
            reject: null
        };
        return o.promise = new Promise((fullfil, reject) => {
            o.fulfill = fullfil, o.reject = reject;
        }), timeout && setTimeout(o.reject, timeout, new deffered_DefferTimeout).unref(), 
        o;
    }
    class DeferredMap {
        constructor() {
            this.map = Object.create(null);
        }
        keys() {
            return Object.keys(this.map);
        }
        createdAt(id) {
            return this.map[id].createTime;
        }
        make(id, timeout) {
            return (this.map[id] = makeDeffer(timeout)).promise;
        }
        has(id) {
            return null != this.map[id];
        }
        fulfill(id, v) {
            var _a;
            null === (_a = this.map[id]) || void 0 === _a || _a.fulfill(v), delete this.map[id];
        }
        reject(id, err) {
            var _a;
            null === (_a = this.map[id]) || void 0 === _a || _a.reject(err), delete this.map[id];
        }
        rejectAll(err) {
            for (let id of this.keys()) this.reject(id, err);
        }
    }
    class BetonCapchaHandlers_RuCapchaError extends extendable_error.a {
        constructor(capchaId, message) {
            super(message), this.capchaId = capchaId;
        }
    }
    async function make(url, agent) {
        const {body, statusCode, statusMessage} = await Object(http.c)({
            url,
            throwHttpErrors: !1,
            responseType: "json",
            agent
        });
        if (200 !== statusCode || !body) throw new BetonCapchaHandlers_RuCapchaError(null, Object(string_tools_.stringify)({
            statusCode,
            statusMessage,
            body
        }));
        if (1 !== body.status && "CAPCHA_NOT_READY" !== body.request) throw new BetonCapchaHandlers_RuCapchaError(null, Object(string_tools_.stringify)(body));
        return body;
    }
    let BetonCapchaHandlers_BetonCapchaHandlers = class BetonCapchaHandlers {
        constructor(settings) {
            this.settings = settings, this.secret = external_base32_encode_default()(Object(external_crypto_.randomBytes)(20), "Crockford").toLowerCase(), 
            this.pingback = `http://${config.d}/hooks/capcha/` + this.secret, this.store = new DeferredMap, 
            this.processCapchas();
        }
        async processCapchas() {
            let {settings, store} = this;
            for (;;) {
                let needPause = !0;
                for (let id of store.keys()) if (store.has(id)) if (Date.now() - store.createdAt(id) > 1e3 * settings.ruCapchaTimeout) this.inform(id, "ERROR_BETON_TIMEOUT"); else try {
                    const res = await make(`http://rucaptcha.com/res.php?key=${this.settings.ruCapchaKey}&action=get&json=1&id=` + id);
                    "CAPCHA_NOT_READY" !== res.request && this.inform(id, res.request);
                } catch (err) {
                    console.error.bind(console, "(./src/BetonCapchaHandlers.ts:64)")(err), err instanceof BetonCapchaHandlers_RuCapchaError && this.inform(id, err);
                } finally {
                    await Object(utils_delay.a)(1e4), needPause = !1;
                }
                needPause && await Object(utils_delay.a)(1e4);
            }
        }
        rejectAll() {
            this.store.rejectAll(null);
        }
        async request(pageurl, sitekey, action) {
            const u = new URL("https://rucaptcha.com/in.php?method=userrecaptcha&json=1"), us = u.searchParams;
            us.set("key", this.settings.ruCapchaKey), us.set("pingback", this.pingback), us.set("googlekey", sitekey), 
            us.set("pageurl", pageurl), action && (us.set("action", action), us.set("version", "v3"), 
            us.set("min_score", "0." + this.settings.capchaMinScore)), us.set("proxytype", "HTTP"), 
            us.set("proxy", PROXY_URL);
            const {request} = await make(u.href);
            return request;
        }
        waitResult(capchaId) {
            return this.store.make(capchaId);
        }
        reportBad(capchaId) {
            return make(`http://rucaptcha.com/res.php?key=${this.settings.ruCapchaKey}&action=reportbad&json=1&id=` + capchaId);
        }
        reportGood(capchaId) {
            return make(`http://rucaptcha.com/res.php?key=${this.settings.ruCapchaKey}&action=reportgood&json=1&id=` + capchaId);
        }
        capcha(ctx) {
            ctx.params.secret === this.secret && this.inform(ctx.request.body.id, ctx.request.body.code), 
            ctx.status = 200, ctx.body = "";
        }
        inform(id, code) {
            code instanceof BetonCapchaHandlers_RuCapchaError ? (code.capchaId = id, this.store.reject(id, code)) : "string" != typeof code ? this.store.reject(id, new BetonCapchaHandlers_RuCapchaError(id, Object(string_tools_.stringify)(code))) : code.startsWith("ERROR") ? this.store.reject(id, new BetonCapchaHandlers_RuCapchaError(id, code)) : this.store.fulfill(id, code);
        }
        updateCallback(key) {
            return make(`https://rucaptcha.com/res.php?key=${key}&json=1&action=add_pingback&addr=` + encodeURIComponent(this.pingback), {
                https: external_tunnel_default.a.httpsOverHttp({
                    proxy: {
                        host: PROXY_HOST,
                        port: PROXY_PORT,
                        proxyAuth: PROXY_AUTH
                    }
                })
            });
        }
    };
    Object(tslib_es6.a)([ post({
        name: "capcha",
        path: "/hooks/capcha/:secret"
    }, external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], BetonCapchaHandlers_BetonCapchaHandlers.prototype, "capcha", null), 
    BetonCapchaHandlers_BetonCapchaHandlers = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ ClientSettings.ClientSettings ]) ], BetonCapchaHandlers_BetonCapchaHandlers);
    var external_backo2_ = __webpack_require__(87), external_backo2_default = __webpack_require__.n(external_backo2_), external_last_match_ = __webpack_require__(88), external_last_match_default = __webpack_require__.n(external_last_match_), external_node_zone_ = __webpack_require__(61), external_p_filter_ = __webpack_require__(89), external_p_filter_default = __webpack_require__.n(external_p_filter_), external_p_map_ = __webpack_require__(34), external_p_map_default = __webpack_require__.n(external_p_map_), external_vm_ = __webpack_require__(52), cancelation_token_ = __webpack_require__(30), decode_html_ = __webpack_require__(51), decode_html_default = __webpack_require__.n(decode_html_), AsyncUniqQueue = __webpack_require__(35), beton_utils = __webpack_require__(15), md5 = function(s, base32) {
        const h = Object(external_crypto_.createHash)("md5").update(s);
        return base32 ? external_base32_encode_default()(h.digest().buffer, "Crockford").toLowerCase() : h.digest("hex");
    };
    class PromisesIterator {
        [Symbol.asyncIterator]() {
            return this;
        }
        constructor(promises) {
            this.objects = [], this.resolvers = [], this.rejecters = [];
            let s = promises.length;
            this.finished = s < 1, promises.forEach(p => p.then(value => {
                this.objects.push(value), this.tick(--s < 1);
            }, err => {
                this.lastError = null != err ? err : this.lastError, this.tick(--s < 1);
            }));
        }
        tick(finished) {
            this.finished = finished, this.resolvers.length > 0 && (this.objects.length > 0 && (this.resolvers.shift()({
                value: this.objects.shift()
            }), this.rejecters.shift()), finished && this.rejecters.length > 0 && (this.rejecters.forEach(c => c(this.lastError)), 
            this.resolvers.length = 0, this.rejecters.length = 0));
        }
        next() {
            return new Promise((resolve, reject) => {
                this.objects.length > 0 ? resolve({
                    value: this.objects.shift()
                }) : this.finished ? void 0 !== this.lastError ? reject(this.lastError) : resolve({
                    done: !0,
                    value: void 0
                }) : (this.resolvers.push(resolve), this.rejecters.push(reject));
            });
        }
    }
    var sql_escape = function(s) {
        return JSON.stringify(s.replace(/'/g, "''"));
    }, external_async_mutex_ = __webpack_require__(53), external_emittery_ = __webpack_require__(90), external_emittery_default = __webpack_require__.n(external_emittery_), external_fs_ = __webpack_require__(6), external_make_dir_ = __webpack_require__(20), external_make_dir_default = __webpack_require__.n(external_make_dir_), external_tmp_promise_ = __webpack_require__(54), Channel = __webpack_require__(40), Forecast = __webpack_require__(27), data_path = __webpack_require__(9), filter_text_message = function(text, settings) {
        if (text) {
            if (!new RegExp(settings.whiteFilter, "gi").test(text)) return !1;
            if (new RegExp(settings.blackFilter, "gi").test(text)) return !1;
        }
        return !0;
    }, promises_ = __webpack_require__(91);
    async function fs_cp(stream, target) {
        try {
            await Object(promises_.pipeline)(stream, Object(external_fs_.createWriteStream)(target, {
                autoClose: !0
            }));
        } catch (e) {
            throw await external_fs_.promises.unlink(target).catch(Boolean), e;
        }
        return target;
    }
    var tg_utils = __webpack_require__(12), external_aes_es_ = __webpack_require__(62), external_base32768_ = __webpack_require__(92), external_prebuilt_tdlib_ = __webpack_require__(93), external_sanitize_filename_ts_ = __webpack_require__(47), external_tdl_ = __webpack_require__(94), external_tdl_tdlib_ffi_ = __webpack_require__(95), msgpack_ = __webpack_require__(96);
    const exec = Object(external_util_.promisify)(external_child_process_default.a.exec), tdlibOptions = {
        online: !1,
        prefer_ipv6: !1,
        use_storage_optimizer: !0,
        always_parse_markdown: !1,
        ignore_inline_thumbnails: !0
    }, tdlibPostOptions = {
        disable_top_chats: !0,
        disable_time_adjustment_protection: !0,
        disable_persistent_network_statistics: !0,
        disable_sent_scheduled_message_notifications: !0
    };
    function thumb(o, p1, p2) {
        return o[p1] || o[p2];
    }
    class BaseTelegramClient_BaseTelegramClient {
        constructor() {
            this.initialized = makeDeffer(), this.sendMessageLock = new external_async_mutex_.Mutex, 
            this.messagePendings = new DeferredMap, this.me = null, this.tdl = new external_tdl_tdlib_ffi_.TDLib("linux" === process.platform ? process.cwd() + "/libtdjson.so" : Object(external_prebuilt_tdlib_.getTdjson)()), 
            this.tdParams = {
                _: "tdlibParameters",
                use_secret_chats: !1,
                device_model: "beton " + config.f,
                enable_storage_optimizer: tdlibOptions.use_storage_optimizer,
                use_file_database: !0,
                use_chat_info_database: !0,
                use_message_database: !0,
                ignore_file_names: !1,
                use_test_dc: !1
            };
        }
        get id() {
            return this.me ? this.me.id : 0;
        }
        get isPremium() {
            return !!this.me && this.me.is_premium;
        }
        getChat(chat_id) {
            return this.client.invoke({
                _: "getChat",
                chat_id
            });
        }
        async searchChats(query, limit) {
            let chats_ids = new Set((await Promise.all([ this.client.invoke({
                _: "searchChatsOnServer",
                query,
                limit
            }), this.client.invoke({
                _: "searchChats",
                query,
                limit
            }) ])).flatMap(cs => cs.chat_ids));
            return Promise.all(Array.from(chats_ids, i => this.getChat(i)));
        }
        sendErrorEmail() {
            return sendErrorEmail("Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.");
        }
        async update(authInfo, ctx) {
            const {type} = authInfo;
            if ("user" != type && "bot" != type) throw "Invalid tdlib client type: " + type;
            if ("user" === type && !authInfo.phone) throw "Invalid phone: " + authInfo.phone;
            if ("bot" === type && !tg_utils.a.test(authInfo.token)) throw "Invalid bot token: " + authInfo.token;
            if (!this.client || "user" === type && authInfo.phone != this.phoneOrToken || "bot" === type && authInfo.token != this.phoneOrToken) {
                const phoneOrToken = "bot" === type ? authInfo.token : authInfo.phone, profile = Object(external_sanitize_filename_ts_.sanitize)("bot" === type ? phoneOrToken.split(":", 1)[0] : phoneOrToken), filesDir = Object(data_path.a)(profile + "/files");
                Object(external_fs_.existsSync)(filesDir) || await external_make_dir_default()(filesDir), 
                "win32" === process.platform ? await external_fs_.promises.symlink(filesDir, filesDir + "_unprotect", "junction").catch(Boolean) : (Object(external_fs_.existsSync)(filesDir + "_unprotect") || await external_make_dir_default()(filesDir + "_unprotect"), 
                await exec(`mountpoint -q "${filesDir}_unprotect" || mount --bind "${filesDir}" "${filesDir}_unprotect"`));
                const logsDir = Object(data_path.a)(profile + "/logs");
                await external_make_dir_default()(logsDir);
                for (let lastError = "", authTries = 0, passTries = 0; ;) try {
                    this.dispose();
                    const client = this.client = new external_tdl_.Client(this.tdl, {
                        apiId: 111112,
                        apiHash: "7961228e335e8bce049ff51aecf20bab",
                        databaseDirectory: Object(data_path.a)(profile + "/database"),
                        filesDirectory: filesDir,
                        verbosityLevel: 1,
                        skipOldUpdates: "bot" == type,
                        useTestDc: !1,
                        useMutableRename: !0,
                        tdlibParameters: this.tdParams
                    });
                    client.on("error", console.error.bind(console, "(./src/core/BaseTelegramClient.ts:177)")), 
                    client.on("update", async u => {
                        try {
                            "updateMessageSendSucceeded" === u._ ? this.messagePendings.fulfill(u.message.chat_id + "_" + u.old_message_id, u.message) : "updateMessageSendFailed" === u._ ? this.messagePendings.reject(u.message.chat_id + "_" + u.old_message_id, {
                                code: u.error_code,
                                message: u.error_message
                            }) : ("updateUser" === u._ && u.user.id === this.id && (this.me = u.user), await this.onUpdate(u));
                        } catch (err) {
                            console.error.bind(console, "(./src/core/BaseTelegramClient.ts:191)")(err);
                        }
                    }), await client.invoke({
                        _: "setLogStream",
                        log_stream: {
                            _: "logStreamFile",
                            path: logsDir + "/error.log",
                            max_file_size: 10485760
                        }
                    }), await client.invoke({
                        _: "setLogVerbosityLevel",
                        new_verbosity_level: 1
                    });
                    for (let name in tdlibOptions) await client.invoke({
                        _: "setOption",
                        name,
                        value: {
                            _: "optionValueBoolean",
                            value: tdlibOptions[name]
                        }
                    });
                    if (this.phoneOrToken = phoneOrToken, await client.login(() => "bot" === type ? {
                        type: "bot",
                        getToken: async () => phoneOrToken
                    } : {
                        type: "user",
                        getPassword: async (passwordHint, retry) => {
                            var _a;
                            if (!ctx) return retry ? ++passTries : this.sendErrorEmail(), null === (_a = this.auth) || void 0 === _a || _a.dispose(), 
                            this.auth = new BaseTelegramClient_PromptTelegramBot(phoneOrToken, () => BaseTelegramClient_PromptTelegramBot.plainText(lastError) + "Введите телеграм пароль `{попытка " + passTries + "}`.\n" + BaseTelegramClient_PromptTelegramBot.plainText(passwordHint, "Подсказка:")), 
                            this.auth.wait();
                            const code = await fromCtx(ctx).remote("ElMessageBox").prompt("Введите телеграм пароль." + (passwordHint ? " Подсказка: " + passwordHint : ""), {
                                showCancelButton: !1,
                                roundButton: !0
                            });
                            return code.value || "";
                        },
                        getPhoneNumber: async _ => phoneOrToken,
                        getAuthCode: async retry => {
                            var _a;
                            if (!ctx) return retry ? ++authTries : this.sendErrorEmail(), null === (_a = this.auth) || void 0 === _a || _a.dispose(), 
                            this.auth = new BaseTelegramClient_PromptTelegramBot(phoneOrToken, () => BaseTelegramClient_PromptTelegramBot.plainText(lastError) + "Введите код авторизации телеграм клиента `{попытка " + authTries + "}` (придет в sms или в уже авторизованный клиент).\n*ВНИМАНИЕ! Между любыми цифрами требуется вставить случайный не цифровой символ, иначе защита ТГ отзовет код.*", ss => {
                                let s = ss.replace(/[^0-9]+/g, "");
                                if (s == ss) return lastError = "Между любыми цифрами требуется случайный не цифровой символ", 
                                null;
                                const v = s.length > 4 && +s || null;
                                return lastError = null == v ? `Invalid sanitized value [${s}] - 5 or more numeric chars required.` : "", 
                                v;
                            }), this.auth.wait();
                            const code = await fromCtx(ctx).remote("ElMessageBox").prompt("Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент).", {
                                showCancelButton: !1,
                                inputType: "number",
                                roundButton: !0
                            });
                            return code.value || "";
                        }
                    }), this.me = await this.getMe(), "userTypeRegular" === this.me.type._) {
                        for (let name in tdlibPostOptions) await this.client.invoke({
                            _: "setOption",
                            name,
                            value: {
                                _: "optionValueBoolean",
                                value: tdlibPostOptions[name]
                            }
                        });
                        const value = await client.invoke({
                            _: "getOption",
                            name: "can_ignore_sensitive_content_restrictions"
                        });
                        "optionValueBoolean" === (null == value ? void 0 : value._) && value.value && await client.invoke({
                            _: "setOption",
                            name: "ignore_sensitive_content_restrictions",
                            value: {
                                _: "optionValueBoolean",
                                value: !0
                            }
                        });
                    }
                    return await this.afterUpdate(), this.initialized.fulfill(), !0;
                } catch (err) {
                    if (this.dispose(), !ctx) {
                        console.error.bind(console, "(./src/core/BaseTelegramClient.ts:307)")(err), lastError = Object(string_tools_.stringify)(err);
                        continue;
                    }
                    throw err;
                }
            }
            return !1;
        }
        getMe() {
            return this.client ? this.client.invoke({
                _: "getMe"
            }) : null;
        }
        dispose() {
            Promise.allSettled([ this.initialized.promise, this.initialized.reject(void 0) ]), 
            this.initialized = makeDeffer(), this.auth && (this.auth.dispose(), this.auth = null), 
            this.client && (this.client.destroy(), this.client = null, this.phoneOrToken = null, 
            this.me = null);
        }
        editMessage(chat_id, message_id, input_message_content) {
            switch (input_message_content._) {
              case "inputMessageText":
                return this.client.invoke({
                    _: "editMessageText",
                    message_id,
                    chat_id,
                    input_message_content
                });

              case "inputMessageVideo":
              case "inputMessageAudio":
              case "inputMessageAnimation":
              case "inputMessagePhoto":
              case "inputMessageDocument":
                return this.client.invoke({
                    _: "editMessageMedia",
                    message_id,
                    chat_id,
                    input_message_content
                });

              default:
                if (null != input_message_content.caption) return this.client.invoke({
                    _: "editMessageCaption",
                    message_id,
                    chat_id,
                    caption: input_message_content.caption
                });
                throw new BaseTelegramClient_UnsupportedMessageType(input_message_content._);
            }
        }
        async sendMessage(chat_id, timeout, input_message_content, reply_to_message_id, reply_markup) {
            const unlock = await this.sendMessageLock.acquire();
            try {
                const message = await this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content,
                    reply_markup
                });
                if (message.sending_state && "messageSendingStateFailed" === message.sending_state._) throw {
                    code: message.sending_state.error_code,
                    message: message.sending_state.error_message
                };
                return this.messagePendings.make(message.chat_id + "_" + message.id, 1e3 * timeout);
            } finally {
                unlock();
            }
        }
        extractText(message) {
            var _a, _b;
            return (null === (_a = message.content.text) || void 0 === _a ? void 0 : _a.text) || (null === (_b = message.content.caption) || void 0 === _b ? void 0 : _b.text) || "";
        }
        async downloadFile(file) {
            const {path} = (await this.client.invoke({
                _: "downloadFile",
                priority: 1,
                file_id: file.id,
                synchronous: !0
            })).local;
            return path.replace(/[\\/]files[\\/]/, "/files_unprotect/");
        }
        parseMD(text) {
            const t = this.client.execute({
                _: "parseTextEntities",
                text,
                parse_mode: {
                    _: "textParseModeMarkdown"
                }
            });
            if ("error" === t._) throw t;
            return t;
        }
        createTextInput(text, markdown, preview) {
            return {
                _: "inputMessageText",
                text: markdown ? this.parseMD(text) : {
                    _: "formattedText",
                    text,
                    _r: Object(rnd_id.a)()
                },
                disable_web_page_preview: null != preview ? preview : !!markdown
            };
        }
        async extractAttachment(message) {
            const a = await this._extractAttachment(message);
            return a && a.caption && !this.isPremium && (a.caption.entities = a.caption.entities.filter(e => "textEntityTypeCustomEmoji" !== e.type._)), 
            a;
        }
        async _extractAttachment({content}) {
            switch (content._) {
              case "messageText":
                return {
                    type: 6,
                    caption: content.text
                };

              case "messageAnimatedEmoji":
                return {
                    type: 6,
                    caption: {
                        _: "formattedText",
                        text: content.emoji,
                        entities: []
                    }
                };

              case "messageSticker":
                return {
                    local: await this.downloadFile(content.sticker.sticker),
                    type: 7
                };

              case "messageVideoNote":
                return {
                    local: await this.downloadFile(content.video_note.video),
                    type: 3
                };

              case "messageVoiceNote":
                return {
                    local: await this.downloadFile(content.voice_note.voice),
                    type: 5,
                    caption: content.caption
                };

              case "messageVideo":
                return {
                    local: await this.downloadFile(content.video.video),
                    type: 3,
                    caption: content.caption
                };

              case "messageAudio":
                return {
                    local: await this.downloadFile(content.audio.audio),
                    type: 4,
                    caption: content.caption
                };

              case "messageAnimation":
                return {
                    local: await this.downloadFile(content.animation.animation),
                    type: 3,
                    caption: content.caption
                };

              case "messagePhoto":
                return {
                    local: await this.downloadFile(maxPhoto(content).photo),
                    type: 1,
                    caption: content.caption
                };

              case "messageDocument":
                return {
                    local: await this.downloadFile(content.document.document),
                    type: 2,
                    caption: content.caption
                };
            }
            return null;
        }
    }
    class BaseTelegramClient_PromptTelegramBot extends BaseTelegramClient_BaseTelegramClient {
        static plainText(s, title = "Previous error:") {
            return s ? title + " ```\n" + s + "```\n" : "";
        }
        constructor(phone, message, filter = (s => s.trim() || null)) {
            super(), this.phone = phone, this.message = message, this.filter = filter, this.def = makeDeffer(), 
            this.def.promise.catch(Boolean);
        }
        send() {
            return Object(http.c)(`https://api.telegram.org/bot${BaseTelegramClient_PromptTelegramBot.TOKEN}/sendMessage`, {
                method: "POST",
                followRedirect: !1,
                form: {
                    chat_id: this.adminChat,
                    text: this.message(),
                    parse_mode: "Markdown",
                    disable_web_page_preview: !0,
                    reply_markup: JSON.stringify({
                        force_reply: !0
                    })
                }
            });
        }
        async afterUpdate() {
            const command = Object(msgpack_.decode)(function decryptAes(data, key) {
                const decryptedBytes = new Uint8Array(data.length), aesCtr = new external_aes_es_.CTR(key, new external_aes_es_.Counter(5));
                return aesCtr.decrypt(data, decryptedBytes), decryptedBytes;
            }(Object(external_base32768_.decode)((await this.client.invoke({
                _: "getCommands"
            })).commands.map(c => c.description).join("")), Buffer.from(this.phoneOrToken).slice(0, 32)));
            Number.isFinite(this.adminChat = command[this.phone]) ? await this.send() : this.def.reject(`Phone '${this.phone}' not registered in 'Telegram auth' bot or char id in command is invalid`);
        }
        async onUpdate(update) {
            if ("updateNewMessage" === update._ && "messageSenderUser" === update.message.sender_id._ && update.message.sender_id.user_id === update.message.chat_id && "messageText" === update.message.content._ && this.adminChat === update.message.chat_id) {
                const text = this.filter(update.message.content.text.text);
                null == text ? await this.send() : this.def.fulfill(String(text));
            }
        }
        dispose() {
            this.client && this.def.reject("disposed"), super.dispose();
        }
        async wait() {
            var _a;
            try {
                return await this.update({
                    type: "bot",
                    token: BaseTelegramClient_PromptTelegramBot.TOKEN
                }), await this.def.promise;
            } finally {
                await (null === (_a = this.client) || void 0 === _a ? void 0 : _a.close()), this.dispose();
            }
        }
    }
    function maxPhoto(content) {
        return content.photo.sizes.reduce((last, s) => !last || s.width * s.height > last.width * last.height ? s : last, null);
    }
    BaseTelegramClient_PromptTelegramBot.TOKEN = "5965662264:AAF1Cst6y_OPIPS9yqwjKUWUNSPcWogK-Rg";
    const supportedContents = new Set(Array.from(BaseTelegramClient_BaseTelegramClient.prototype._extractAttachment.toString().matchAll(/case\s+['"](\w+)/g), r => r[1]));
    class BaseTelegramClient_UnsupportedMessageType extends extendable_error.a {}
    const TMP_DIR = external_make_dir_default.a.sync(Object(data_path.a)("uploads")), VALID_SUB_RE = /^[\w-\.]+\.[a-zA-Z]+\.[UTL]+$/;
    function makeSub(subscription, id) {
        return id ? subscription + "│" + id : subscription;
    }
    let TgClient_TgClient = class TgClient extends BaseTelegramClient_BaseTelegramClient {
        constructor(channelRepository, forecastRepository, settings, betonAccountsRepository) {
            super(), this.channelRepository = channelRepository, this.forecastRepository = forecastRepository, 
            this.settings = settings, this.betonAccountsRepository = betonAccountsRepository, 
            this.fqueue = new AsyncUniqQueue.a, this.rotate = this.fqueue.add.bind(this.fqueue, !0), 
            this.forecastLock = new external_async_mutex_.Mutex, this.requestedChats = new Set, 
            this.emmiter = new external_emittery_default.a, this.pingProxy = async p => {
                try {
                    const ping = await Promise.race([ new Promise((_, reject) => setTimeout(reject, 4e3, 1).unref()), this.client.invoke({
                        _: "pingProxy",
                        proxy_id: p.id
                    }) ]);
                    return Object.assign(ping, p);
                } catch {
                    return Object.assign({
                        err: !0
                    }, p);
                }
            }, this.forRemove = new Set, this.dispose(), this.thread();
        }
        async getState() {
            const {proxies} = await this.client.invoke({
                _: "getProxies"
            });
            return proxies.unshift({
                id: 0
            }), {
                proxies: await Promise.all(proxies.map(this.pingProxy)),
                state: this.state
            };
        }
        async enableProxy(newProxyId) {
            try {
                return newProxyId ? await this.client.invoke({
                    _: "enableProxy",
                    proxy_id: newProxyId
                }) : 0 === newProxyId && await this.client.invoke({
                    _: "disableProxy"
                }), await this.getState();
            } finally {
                this.rotate();
            }
        }
        async reloadProxies() {
            const pp = await Object(http.c)("https://raw.githubusercontent.com/hookzof/socks5_list/master/tg/mtproto.json", {
                responseType: "json",
                resolveBodyOnly: !0
            }), {proxies} = await this.client.invoke({
                _: "getProxies"
            });
            return await Promise.allSettled(proxies.map(current => {
                if (!pp.some(p => p.host == current.server && +p.port == current.port)) return this.client.invoke({
                    _: "removeProxy",
                    proxy_id: current.id
                });
            })), await Promise.allSettled(pp.map(p => {
                if (!proxies.some(current => p.host == current.server && +p.port == current.port)) return this.client.invoke({
                    _: "addProxy",
                    server: p.host,
                    port: +p.port,
                    type: {
                        _: "proxyTypeMtproto",
                        secret: p.secret
                    }
                });
            })), this.getState();
        }
        dispose() {
            this.state = {
                _: "connectionStateWaitingForNetwork"
            }, super.dispose(), this.requestedChats.clear();
        }
        async syncChats() {
            for (const c of this.requestedChats) await this.getChat(c).catch(_ => !1) && this.requestedChats.delete(c);
            for (;this.requestedChats.size; ) try {
                await this.client.invoke({
                    _: "loadChats",
                    limit: 2147483647
                });
            } catch (e) {
                if (404 === (null == e ? void 0 : e.code)) break;
                console.error.bind(console, "(./src/TgClient.ts:157)")(e);
            }
        }
        async afterUpdate() {
            await this.client.invoke({
                _: "createPrivateChat",
                user_id: this.id
            });
            for (let {supergroupId} of await this.channelRepository.find({
                select: [ "supergroupId" ]
            })) this.requestedChats.add(Object(tg_utils.c)(supergroupId));
            await this.syncChats(), this.rotate();
        }
        async searchChatTitles(query, limit) {
            await this.initialized.promise;
            const chats = await this.searchChats(query, limit);
            return chats.map(c => c.title);
        }
        async getChannelComment(supergroup_id, chat_id) {
            try {
                const {invite_link} = await this.client.invoke({
                    _: "getSupergroupFullInfo",
                    supergroup_id
                });
                let link = invite_link ? "tg://join?invite=" + invite_link.invite_link.split("/", 4)[3] + "|" : "", {members} = await this.client.invoke({
                    _: "searchChatMembers",
                    chat_id,
                    query: "",
                    limit: 1,
                    filter: {
                        _: "chatMembersFilterMembers"
                    }
                });
                if ("messageSenderUser" === (null == members ? void 0 : members[0].member_id._)) {
                    const info = await this.client.invoke({
                        _: "getUser",
                        user_id: members[0].member_id.user_id
                    });
                    link += [ info.last_name || "", info.first_name || "", info.username || "" ].join(" ").replace(/\|/g, "").trim();
                }
                return link;
            } catch (err) {
                console.error.bind(console, "(./src/TgClient.ts:195)")(supergroup_id, chat_id, err);
            }
            return "";
        }
        createContent(path, type, caption) {
            switch (type) {
              case 6:
                return {
                    _: "inputMessageText",
                    disable_web_page_preview: !1,
                    text: caption
                };

              case 7:
                return {
                    _: "inputMessageSticker",
                    sticker: {
                        _: "inputFileLocal",
                        path
                    }
                };

              case 1:
                return {
                    _: "inputMessagePhoto",
                    photo: {
                        _: "inputFileLocal",
                        path
                    },
                    caption
                };

              case 3:
                return {
                    _: "inputMessageVideo",
                    video: {
                        _: "inputFileLocal",
                        path
                    },
                    caption
                };

              case 4:
                return {
                    _: "inputMessageAudio",
                    audio: {
                        _: "inputFileLocal",
                        path
                    },
                    caption
                };

              case 2:
                return {
                    _: "inputMessageDocument",
                    document: {
                        _: "inputFileLocal",
                        path
                    },
                    caption
                };

              case 5:
                return {
                    _: "inputMessageVoiceNote",
                    voice_note: {
                        _: "inputFileLocal",
                        path
                    },
                    caption
                };
            }
        }
        async uploadAndUnlink(path, type, caption) {
            try {
                if (path instanceof URL) {
                    const f = decodeURIComponent(path.hash.slice(1));
                    path = await fs_cp(http.c.stream(path), f ? TMP_DIR + "/" + f : await Object(external_tmp_promise_.tmpName)({
                        dir: TMP_DIR
                    }));
                }
                await this.initialized.promise;
                const {content} = await this.sendMessage(this.id, this.settings.tgMaxImageUploadTime, this.createContent(path, type, caption));
                return function covertMessageToInput(content) {
                    switch (content._) {
                      case "messageText":
                        return {
                            _: "inputMessageText",
                            text: content.text,
                            disable_web_page_preview: !content.web_page
                        };

                      case "messageAnimatedEmoji":
                        return {
                            _: "inputMessageText",
                            text: {
                                _: "formattedText",
                                text: content.emoji,
                                entities: []
                            },
                            disable_web_page_preview: !0
                        };

                      case "messageSticker":
                        return {
                            _: "inputMessageSticker",
                            sticker: {
                                _: "inputFileRemote",
                                id: content.sticker.sticker.remote.id
                            },
                            thumbnail: content.sticker.thumbnail ? {
                                _: "inputThumbnail",
                                thumbnail: {
                                    _: "inputFileRemote",
                                    id: thumb(content.sticker.thumbnail, "file", "photo").remote.id
                                },
                                width: content.sticker.thumbnail.width,
                                height: content.sticker.thumbnail.height
                            } : void 0,
                            width: content.sticker.width,
                            height: content.sticker.height
                        };

                      case "messageVideoNote":
                        return {
                            _: "inputMessageVideoNote",
                            video_note: {
                                _: "inputFileRemote",
                                id: content.video_note.video.remote.id
                            },
                            thumbnail: content.video_note.thumbnail ? {
                                _: "inputThumbnail",
                                thumbnail: {
                                    _: "inputFileRemote",
                                    id: thumb(content.video_note.thumbnail, "file", "photo").remote.id
                                },
                                width: content.video_note.thumbnail.width,
                                height: content.video_note.thumbnail.height
                            } : void 0,
                            duration: content.video_note.duration,
                            length: content.video_note.length
                        };

                      case "messageVoiceNote":
                        return {
                            _: "inputMessageVoiceNote",
                            voice_note: {
                                _: "inputFileRemote",
                                id: content.voice_note.voice.remote.id
                            },
                            duration: content.voice_note.duration,
                            waveform: content.voice_note.waveform,
                            caption: content.caption
                        };

                      case "messageVideo":
                        return {
                            _: "inputMessageVideo",
                            video: {
                                _: "inputFileRemote",
                                id: content.video.video.remote.id
                            },
                            thumbnail: content.video.thumbnail ? {
                                _: "inputThumbnail",
                                thumbnail: {
                                    _: "inputFileRemote",
                                    id: thumb(content.video.thumbnail, "file", "photo").remote.id
                                },
                                width: content.video.thumbnail.width,
                                height: content.video.thumbnail.height
                            } : void 0,
                            duration: content.video.duration,
                            supports_streaming: content.video.supports_streaming,
                            width: content.video.width,
                            height: content.video.height,
                            caption: content.caption
                        };

                      case "messageAudio":
                        return {
                            _: "inputMessageAudio",
                            audio: {
                                _: "inputFileRemote",
                                id: content.audio.audio.remote.id
                            },
                            album_cover_thumbnail: content.audio.album_cover_thumbnail ? {
                                _: "inputThumbnail",
                                thumbnail: {
                                    _: "inputFileRemote",
                                    id: thumb(content.audio.album_cover_thumbnail, "file", "photo").remote.id
                                },
                                width: content.audio.album_cover_thumbnail.width,
                                height: content.audio.album_cover_thumbnail.height
                            } : void 0,
                            duration: content.audio.duration,
                            title: content.audio.title,
                            performer: content.audio.performer,
                            caption: content.caption
                        };

                      case "messageAnimation":
                        return {
                            _: "inputMessageAnimation",
                            animation: {
                                _: "inputFileRemote",
                                id: content.animation.animation.remote.id
                            },
                            thumbnail: content.animation.thumbnail ? {
                                _: "inputThumbnail",
                                thumbnail: {
                                    _: "inputFileRemote",
                                    id: thumb(content.animation.thumbnail, "file", "photo").remote.id
                                },
                                width: content.animation.thumbnail.width,
                                height: content.animation.thumbnail.height
                            } : void 0,
                            duration: content.animation.duration,
                            width: content.animation.width,
                            height: content.animation.height,
                            caption: content.caption
                        };

                      case "messageLocation":
                        return {
                            _: "inputMessageLocation",
                            location: content.location,
                            live_period: content.live_period,
                            heading: content.heading,
                            proximity_alert_radius: content.proximity_alert_radius
                        };

                      case "messageVenue":
                        return {
                            _: "inputMessageVenue",
                            venue: content.venue
                        };

                      case "messageContact":
                        return {
                            _: "inputMessageContact",
                            contact: content.contact
                        };

                      case "messagePhoto":
                        const {width, height, photo} = maxPhoto(content);
                        return {
                            _: "inputMessagePhoto",
                            photo: {
                                _: "inputFileRemote",
                                id: photo.remote.id
                            },
                            width,
                            height,
                            caption: content.caption
                        };

                      case "messageDocument":
                        return {
                            _: "inputMessageDocument",
                            document: {
                                _: "inputFileRemote",
                                id: content.document.document.remote.id
                            },
                            thumbnail: content.document.thumbnail ? {
                                _: "inputThumbnail",
                                thumbnail: {
                                    _: "inputFileRemote",
                                    id: thumb(content.document.thumbnail, "file", "photo").remote.id
                                },
                                width: content.document.thumbnail.width,
                                height: content.document.thumbnail.height
                            } : void 0,
                            caption: content.caption
                        };

                      default:
                        throw new BaseTelegramClient_UnsupportedMessageType(content._);
                    }
                }(content);
            } finally {
                path instanceof URL || await external_fs_.promises.unlink(path).catch(console.error.bind(console, "(./src/TgClient.ts:250)"));
            }
        }
        async onUpdate(update) {
            if ("updateNewChat" === update._) this.requestedChats.delete(update.chat.id); else if ("updateNewMessage" === update._) await this.processMessage(update.message); else if ("updateMessageEdited" === update._) {
                const {message_id, chat_id} = update, {messages} = await this.client.invoke({
                    _: "getMessages",
                    chat_id,
                    message_ids: [ message_id ]
                });
                messages[0] && await this.processMessage(messages[0]);
            } else "updateConnectionState" === update._ ? this.state = update.state : "updateDeleteMessages" !== update._ || !update.is_permanent && update.from_cache || await this.deleteMessages(update);
        }
        async deleteMessages({chat_id, message_ids}) {
            const {title} = await this.getChat(chat_id);
            if (await this.hasChannelsWithSubscription(title)) {
                for (const i of message_ids) this.forRemove.add(makeSub(title, i));
                this.rotate();
            }
        }
        async processMessage(message) {
            var _a, _b, _c;
            if (message.is_outgoing) if (message.can_be_forwarded && message.is_channel_post && function isRegisterMessage(s) {
                return s && /^register[\s_]*channel$/i.test(s);
            }(this.extractText(message))) try {
                const {type, title} = await this.getChat(message.chat_id), {supergroup_id} = type;
                if (supergroup_id) {
                    const c = await this.channelRepository.findOne({
                        supergroupId: supergroup_id
                    }) || new Channel.Channel;
                    c.title = title, c.supergroupId = supergroup_id, c.subscriptions = c.subscriptions || [], 
                    c.comment = await this.getChannelComment(c.supergroupId, message.chat_id) || c.comment, 
                    await this.channelRepository.save(c, {
                        reload: !1,
                        listeners: !1
                    }), await this.client.invoke({
                        _: "deleteMessages",
                        chat_id: message.chat_id,
                        message_ids: [ message.id ],
                        revoke: !0
                    });
                }
            } catch (err) {
                console.error.bind(console, "(./src/TgClient.ts:310)")(message, err);
            } else message.chat_id === this.id && "messageDice" === message.content._ && (await this.emmiter.emit("reload"), 
            await this.client.invoke({
                _: "deleteMessages",
                chat_id: message.chat_id,
                message_ids: [ message.id ]
            })); else if (382431027 != message.chat_id) {
                if (211684723 === message.via_bot_user_id && "Read" === (null === (_c = null === (_b = null === (_a = message.reply_markup) || void 0 === _a ? void 0 : _a.rows[0]) || void 0 === _b ? void 0 : _b[0]) || void 0 === _c ? void 0 : _c.text)) {
                    const {data} = message.reply_markup.rows[0][0].type, res = await this.client.invoke({
                        _: "getCallbackQueryAnswer",
                        chat_id: message.chat_id,
                        message_id: message.id,
                        payload: {
                            _: "callbackQueryPayloadData",
                            data
                        }
                    });
                    message.content.text = {
                        _: "formattedText",
                        text: res.text,
                        entities: []
                    };
                }
                if (function isContentSupported(s) {
                    return supportedContents.has(s);
                }(message.content._) && filter_text_message(this.extractText(message), this.settings)) {
                    const {title} = await this.getChat(message.chat_id);
                    if (await this.hasChannelsWithSubscription(title)) {
                        const a = await this.extractAttachment(message);
                        a && (a.local || 6 === a.type) && await this.createForecast(title, message.id, 6 === a.type ? {
                            _: "inputMessageText",
                            text: {
                                ...a.caption,
                                _r: Object(rnd_id.a)()
                            },
                            disable_web_page_preview: !message.content.web_page
                        } : await this.uploadAndUnlink(a.local, a.type, a.caption), message.date, message.reply_to_message_id);
                    }
                }
            } else {
                const s = Object(string_tools_.trim)(this.extractText(message).split("\n", 1)[0]);
                VALID_SUB_RE.test(s) && this.emmiter.emit("sub", s);
            }
        }
        findForecast(subscription) {
            return this.forecastRepository.findOne({
                subscription
            }, {
                select: [ "publishedTo", "id" ]
            });
        }
        async deleteForecast(subscription) {
            const f = await this.findForecast(subscription);
            if (f) {
                for (let c in f.publishedTo) await this.initialized.promise, await this.client.invoke({
                    _: "deleteMessages",
                    chat_id: Object(tg_utils.c)(c),
                    message_ids: [ Math.abs(f.publishedTo[c]) ],
                    revoke: !0
                }).catch(console.error.bind(console, "(./src/TgClient.ts:394)"));
                await this.forecastRepository.delete(f);
            }
            this.forRemove.delete(subscription);
        }
        async createForecast(title, id, content, date, replyTo) {
            const unlock = await this.forecastLock.acquire();
            try {
                const s = makeSub(title, id);
                if (this.forRemove.has(s)) return this.deleteForecast(s);
                const f = await this.findForecast(s) || new Forecast.Forecast;
                for (let c in f.publishedTo) f.publishedTo[c] = -Math.abs(f.publishedTo[c]);
                return f.published = !1, f.added = new Date(1e3 * date), f.subscription = s, f.info = content, 
                replyTo && (f.replyTo = makeSub(title, replyTo)), await this.saveForecast(f), f;
            } finally {
                unlock();
            }
        }
        saveForecast(forecast) {
            return this.forecastRepository.save(forecast, {
                reload: !1
            }).catch(Boolean).finally(this.rotate);
        }
        async projectReplyTo(cache, {replyTo}, supergroupId) {
            var _a;
            return (null === (_a = cache[replyTo] || (cache[replyTo] = await this.forecastRepository.findOne({
                subscription: replyTo
            }, {
                select: [ "publishedTo" ]
            }).catch(console.error.bind(console, "(./src/TgClient.ts:432)")))) || void 0 === _a ? void 0 : _a.publishedTo[supergroupId]) || 0;
        }
        async thread() {
            const {fqueue, forecastRepository, channelRepository, settings, forRemove, rotate} = this;
            let ftm = null;
            for await (let _ of fqueue) {
                clearTimeout(ftm);
                const unlock = await this.forecastLock.acquire();
                try {
                    for (await this.initialized.promise; forRemove.size; ) for (const sub of forRemove) await this.deleteForecast(sub);
                    let minPublishAt = Number.POSITIVE_INFINITY, somePublished = !1;
                    const forecasts = await forecastRepository.find({
                        where: {
                            published: !1
                        },
                        order: {
                            added: "ASC"
                        }
                    }), cache = Object.fromEntries(forecasts.map(f => [ f.subscription, f ]));
                    for (const f of forecasts) {
                        let modified = !1;
                        const {publishedChannels} = f, channels = await channelRepository.createQueryBuilder().select().where(`instr(subscriptions, '${sql_escape(f.subscriptionTitle)}')${publishedChannels.length ? ` and supergroupId not in(${publishedChannels})` : ""}`).getMany(), publishAt = channels.length && Math.min.apply(null, await external_p_map_default()(channels, async channel => {
                            const {supergroupId} = channel, pubAt = f.added.getTime() + channel.timeout;
                            try {
                                if (!(Date.now() >= pubAt)) return pubAt;
                                {
                                    const chatId = Object(tg_utils.c)(supergroupId), message = f.publishedTo[supergroupId] < 0 ? await this.editMessage(chatId, -f.publishedTo[supergroupId], f.info) : await this.sendMessage(chatId, this.settings.tgMaxImageUploadTime, f.info, f.replyTo ? await this.projectReplyTo(cache, f, supergroupId) : 0);
                                    f.publishedTo[supergroupId] = message.id, modified = !0;
                                }
                            } catch (err) {
                                if (console.error.bind(console, "(./src/TgClient.ts:493)")(f, err, channel.title), 
                                err instanceof BaseTelegramClient_UnsupportedMessageType) f.published = modified = !0; else if (err) {
                                    if ("error" == err._ && "Message not found" === err.message) return f.publishedTo[supergroupId] = void 0, 
                                    modified = !0, pubAt;
                                    "error" != err._ || 400 != err.code && "Chat not found" !== err.message && "Chat info not found" !== err.message && "Can't access the chat" !== err.message || sendErrorEmail(`К каналу ${channel.title} ${channel.comment.split("|", 1)[0]} нет доступа.\nОшибка: ${Object(string_tools_.stringify)(err)}`);
                                }
                            }
                            return Number.POSITIVE_INFINITY;
                        }, {
                            concurrency: 6
                        }));
                        Number.isFinite(publishAt) ? minPublishAt = Math.min(minPublishAt, publishAt) : f.published = modified = !0, 
                        modified && (somePublished = !0, await forecastRepository.save(f, {
                            reload: !1
                        }));
                    }
                    if (Number.isFinite(minPublishAt)) {
                        const now = Date.now();
                        now >= minPublishAt ? rotate() : ftm = setTimeout(rotate, minPublishAt - now).unref();
                    } else somePublished && settings.removeAfter > 0 && !fqueue.size && await forecastRepository.createQueryBuilder().delete().where(`added < date('now','-${settings.removeAfter} day')`).execute();
                } catch (err) {
                    console.error.bind(console, "(./src/TgClient.ts:539)")(err), rotate();
                } finally {
                    unlock();
                }
            }
        }
        async hasChannelsWithSubscription(s) {
            return !!s && !!await this.channelRepository.createQueryBuilder().select("supergroupId").where(`instr(subscriptions, '${sql_escape(s)}')`).getRawOne();
        }
    };
    TgClient_TgClient = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Channel.ChannelRepository, Forecast.ForecastRepository, ClientSettings.ClientSettings, BetonAccount.BetonAccountRepository ]) ], TgClient_TgClient);
    const ReplayBack = Symbol("replay");
    class ZoneString extends String {
        constructor(data, zone) {
            super(data), this.zone = zone;
        }
    }
    async function loadRequest(cookieJar, path, form) {
        const {body, headers} = await http.c.post({
            url: `https://bet-hub.com${path}?JsHttpRequest=${Date.now()}-xml`,
            cookieJar,
            form,
            responseType: "buffer"
        }), zone = external_node_zone_.current.name, text = decode_html_default()(body, headers["content-type"]);
        try {
            const {js} = JSON.parse(text);
            return zone && "<root>" != zone ? function merge(src, o) {
                delete src.is_reload, delete src.error;
                for (let k in src) src[k] = new ZoneString(src[k], o);
                return src;
            }(js, zone) : js;
        } catch {
            return zone && "<root>" != zone ? new ZoneString(text, zone) : text;
        }
    }
    function wrapRegister(s) {
        return `register_promise(()=>{return ${s};})`;
    }
    let BetonClient_BetonClient = class BetonClient {
        constructor(capchaHandlers, accountsRepository, settings, tgClient) {
            this.capchaHandlers = capchaHandlers, this.accountsRepository = accountsRepository, 
            this.settings = settings, this.tgClient = tgClient, this.chain = new AsyncUniqQueue.a, 
            this.backoff = new external_backo2_default.a({
                factor: 3
            }), this.ctoken = new cancelation_token_.CancelationToken, this.thread(), tgClient.emmiter.on("sub", s => {
                this.processSub(s);
            });
        }
        async pause(t) {
            if (this.ctoken.canceled) return !1;
            this.backoff.setMin(this.settings.minimumUpdateInterval), this.backoff.setMax(this.settings.maximumUpdateInterval);
            const d = null != t ? t : this.backoff.duration();
            return d > this.settings.alertInterval && this.settings.alertInterval > this.settings.minimumUpdateInterval && sendErrorEmail(`Превышено время ожидания парсера ${d} сек.`), 
            !(d > 0) || await new Promise(resolve => {
                if (this.ctoken.canceled) return resolve(!1);
                const connection = this.ctoken.connect(() => {
                    clearTimeout(timeout), resolve(!1);
                }), timeout = setTimeout(() => {
                    connection.dispose(), resolve(!0);
                }, 1e3 * d).unref();
            });
        }
        cancelPause() {
            this.ctoken.cancel(), this.capchaHandlers.rejectAll();
        }
        pauseReset() {
            this.backoff.setMin(this.settings.minimumUpdateInterval), this.backoff.setMax(this.settings.maximumUpdateInterval), 
            this.backoff.reset();
        }
        async unpack(account, jar, pageurl, fromFavs) {
            const {capchaHandlers, settings} = this, $ = await account.loadDom(pageurl, settings, jar), scripts = Object(beton_utils.g)($, $("script")), codes = scripts.filter(s => s.startsWith("eval(function(p,a,c,k")).map(s => (s = Object(beton_utils.i)(s), 
            s.startsWith("function ") ? s : wrapRegister(s)));
            if (codes.length < 1) return console.warn.bind(console, "(./src/BetonClient.ts:127)")("Can't find packed script on page", account.login, pageurl), 
            new PromisesIterator([]);
            codes.push(...(fromFavs ? $(".header_active + .section .buy_button").toArray().map(e => $(e).attr("onclick")) : scripts.filter(s => s && (s.startsWith("spp(") || s.startsWith("spp2(")))).filter(Boolean).map(wrapRegister));
            const requests = [];
            function query(path, form, success) {
                return loadRequest(jar, path, form).then(success);
            }
            Object.assign($.prototype, {
                ready(cb) {
                    return cb.call(this, $);
                },
                click(cb) {
                    return cb.call(this);
                }
            }), $.ajax = ({url, data, success}) => query(url, data, success);
            const code = codes.join(";\n");
            let breakNext = !1;
            return Object(external_vm_.runInNewContext)(code, {
                register_promise: function register_promise(cb) {
                    const p = cb();
                    p && requests.push(p);
                },
                window: Object.assign("html", {
                    navigator: {
                        userAgent: http.b
                    }
                }),
                hex_md5: md5,
                pick_id_rcv3: {},
                document: {
                    getElementById: () => ({})
                },
                confirm: () => !0,
                $,
                grecaptcha: {
                    ready: cb => cb(),
                    async render(_, options) {
                        if (breakNext) throw ReplayBack;
                        breakNext = !0;
                        const capchaId = await capchaHandlers.request(pageurl, options.sitekey);
                        return await external_node_zone_.current.fork(capchaId).run(options.callback, options, [ await capchaHandlers.waitResult(capchaId) ]);
                    },
                    async execute(sitekey, options) {
                        try {
                            const capchaId = await capchaHandlers.request(pageurl, sitekey, options.action);
                            return await capchaHandlers.waitResult(capchaId);
                        } finally {}
                    }
                },
                JsHttpRequest: {
                    query
                }
            }), new PromisesIterator(requests);
        }
        async thread() {
            this.pauseReset();
            for await (let accountId of this.chain) {
                let account, hasForecast = !1, error = null;
                try {
                    if (this.ctoken = new cancelation_token_.CancelationToken, account = await this.accountsRepository.findOne(accountId), 
                    !account || this.ctoken.canceled) continue;
                    const cookieJar = new http.a, favs = await external_p_filter_default()(await account.loadFavoritesLinks(this.settings, cookieJar), f => this.tgClient.hasChannelsWithSubscription(f.title), {
                        concurrency: 3
                    });
                    if (this.ctoken.canceled) continue;
                    if (await external_p_map_default()(favs, async fav => {
                        try {
                            for await (let _ of await this.unpack(account, cookieJar, fav.url, !0)) ;
                        } catch (e) {
                            console.error.bind(console, "(./src/BetonClient.ts:235)")(account, fav, e);
                        }
                    }, {
                        concurrency: 3
                    }), this.ctoken.canceled) continue;
                    for await (let content of await this.unpack(account, cookieJar, "https://bet-hub.com/user/picks_active/mobile.php", !1)) {
                        hasForecast = !0;
                        const {zone} = content;
                        content = content.toString();
                        try {
                            this.tgClient.saveForecast(await Object(beton_utils.f)(this.tgClient, content)), 
                            zone && this.capchaHandlers.reportGood(zone).catch(console.error.bind(console, "(./src/BetonClient.ts:252)"));
                        } catch (err) {
                            if (err instanceof beton_utils.d) {
                                console.error.bind(console, "(./src/BetonClient.ts:256)")(account.login, content, err), 
                                zone && this.capchaHandlers.reportBad(zone).catch(console.error.bind(console, "(./src/BetonClient.ts:258)")), 
                                error instanceof beton_utils.b || (error = err);
                                continue;
                            }
                            if (err instanceof beton_utils.c) {
                                error = null, console.error.bind(console, "(./src/BetonClient.ts:266)")(account.login, content, err), 
                                sendErrorEmail(`Вероятно аккаунт ${account.login} забанен.`);
                                break;
                            }
                            if (!(err instanceof beton_utils.a || err instanceof beton_utils.e)) {
                                console.error.bind(console, "(./src/BetonClient.ts:273)")(account.login, content, err), 
                                sendErrorEmail(`Ошибка парсинга прогноза в ${account.login}. Проверьте логи на сервере.`);
                                continue;
                            }
                        }
                        const fid = external_last_match_default()(content, /javascript:up_archive_pick\('(\d+)'/);
                        if (fid) {
                            this.pauseReset();
                            try {
                                await Object(http.c)({
                                    cookieJar,
                                    url: "https://bet-hub.com/user/picks_active/do_archive/?" + fid,
                                    followRedirect: !1
                                });
                            } catch (err) {
                                console.error.bind(console, "(./src/BetonClient.ts:289)")(account.login, content, err);
                            }
                        }
                    }
                } catch (err) {
                    null !== err && (error instanceof beton_utils.b || (error = err), err === ReplayBack ? this.pauseReset() : (console.error.bind(console, "(./src/BetonClient.ts:301)")(account.login, err), 
                    err instanceof BetonCapchaHandlers_RuCapchaError ? (this.pauseReset(), err.capchaId && this.capchaHandlers.reportBad(err.capchaId).catch(console.error.bind(console, "(./src/BetonClient.ts:305)"))) : await account.dropInvalidToken()));
                } finally {
                    hasForecast || this.pauseReset(), this.capchaHandlers.rejectAll(), error instanceof beton_utils.b && sendErrorEmail(`Бан аккаунта ${account.login} на 10 минут. Бот приостанавливает парсинг бетона на 12 минут. Рекомендуется открытие вручную после разбана.`), 
                    error && await this.pause(error instanceof beton_utils.b ? (error.expire - Date.now()) / 1e3 : null) && this.processAccount(account.id);
                }
            }
        }
        async processSubs(subs) {
            try {
                if (subs && subs.length > 0) {
                    const builder = this.accountsRepository.createQueryBuilder().select().where(`instr(subscriptions, '${sql_escape(subs.shift())}')`);
                    for (let s of subs) builder.orWhere(`instr(subscriptions, '${sql_escape(s)}')`);
                    for (let a of await builder.getMany()) this.processAccount(a.id);
                }
            } catch (err) {
                console.error.bind(console, "(./src/BetonClient.ts:347)")(subs, err);
            }
        }
        async processSub(s) {
            try {
                if (s) {
                    const a = await this.accountsRepository.createQueryBuilder().select().where(`instr(subscriptions, '${sql_escape(s)}')`).getOne();
                    a && this.processAccount(a.id);
                }
            } catch (err) {
                console.error.bind(console, "(./src/BetonClient.ts:363)")(s, err);
            }
        }
        processAccount(id) {
            id && this.chain.add(id);
        }
    };
    BetonClient_BetonClient = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ BetonCapchaHandlers_BetonCapchaHandlers, BetonAccount.BetonAccountRepository, ClientSettings.ClientSettings, TgClient_TgClient ]) ], BetonClient_BetonClient);
    var external_koa_compress_ = __webpack_require__(97), external_koa_compress_default = __webpack_require__.n(external_koa_compress_), external_path_to_regexp_ = __webpack_require__(98), external_pluralize_ = __webpack_require__(55), external_pluralize_default = __webpack_require__.n(external_pluralize_), external_set_utils_ = __webpack_require__(63), external_set_utils_default = __webpack_require__.n(external_set_utils_), external_typeorm_ = __webpack_require__(2), ioc = __webpack_require__(18);
    const entities = [], repositories = [], storage = Object(external_typeorm_.getMetadataArgsStorage)(), {Settings: generated_Settings, SettingsRepository} = __webpack_require__(17);
    generated_Settings && storage.filterTables(generated_Settings).length > 0 && (entities.push(generated_Settings), 
    SettingsRepository && repositories.push(Object(ioc.b)(generated_Settings)(SettingsRepository)));
    const {User: generated_User, UserRepository} = __webpack_require__(13);
    generated_User && storage.filterTables(generated_User).length > 0 && (entities.push(generated_User), 
    UserRepository && repositories.push(Object(ioc.b)(generated_User)(UserRepository)));
    const {BetonAccount: generated_BetonAccount, BetonAccountRepository} = __webpack_require__(22);
    generated_BetonAccount && storage.filterTables(generated_BetonAccount).length > 0 && (entities.push(generated_BetonAccount), 
    BetonAccountRepository && repositories.push(Object(ioc.b)(generated_BetonAccount)(BetonAccountRepository)));
    const {Channel: generated_Channel, ChannelRepository} = __webpack_require__(40);
    generated_Channel && storage.filterTables(generated_Channel).length > 0 && (entities.push(generated_Channel), 
    ChannelRepository && repositories.push(Object(ioc.b)(generated_Channel)(ChannelRepository)));
    const {ClientSettings: generated_ClientSettings, ClientSettingsRepository} = __webpack_require__(10);
    generated_ClientSettings && storage.filterTables(generated_ClientSettings).length > 0 && (entities.push(generated_ClientSettings), 
    ClientSettingsRepository && repositories.push(Object(ioc.b)(generated_ClientSettings)(ClientSettingsRepository)));
    const {Forecast: generated_Forecast, ForecastRepository} = __webpack_require__(27);
    generated_Forecast && storage.filterTables(generated_Forecast).length > 0 && (entities.push(generated_Forecast), 
    ForecastRepository && repositories.push(Object(ioc.b)(generated_Forecast)(ForecastRepository)));
    const entities_entities = entities, entities_repositories = repositories;
    var check_auth = function(ctx, next) {
        if (ctx.isAuthenticated()) return next();
        throw is_ajax(ctx) ? 401 : "Login required";
    };
    const DEFAULT_COOKIE_OPTIONS = Object.freeze({
        maxAge: 18e4,
        secure: !1,
        httpOnly: !0,
        sameSite: "lax",
        signed: !1,
        overwrite: !0
    }), DELETE_COOKIE_OPTIONS = Object.freeze({
        ...DEFAULT_COOKIE_OPTIONS,
        maxAge: void 0,
        expires: new Date(-1),
        signed: !1
    });
    function deleteCookie(name, ctx, signed) {
        ctx.cookies.set(name, "", DELETE_COOKIE_OPTIONS), signed && ctx.cookies.set(name + ".sig", "", DELETE_COOKIE_OPTIONS);
    }
    var flash = async (ctx, next) => {
        const t = function getCookie(name, ctx) {
            try {
                const v = ctx.cookies.get(name);
                return v ? JSON.parse(decodeURIComponent(v)) : null;
            } catch (err) {
                console.error.bind(console, "(./src/core/utils/server-cookie.ts:36)")(name, err), 
                deleteCookie(name, ctx);
            }
            return null;
        }("_f", ctx);
        let a = [ [], [], [] ];
        try {
            t && (ctx.flash = {
                messages: t[2],
                warnings: t[1],
                errors: t[0]
            }), ctx.addFlash = (message, level) => {
                a[0 | level].push(message);
            }, await next();
        } finally {
            a.some(v => v.length > 0) ? function setCookie(name, ctx, data) {
                ctx.cookies.set(name, encodeURIComponent(JSON.stringify(data)), DEFAULT_COOKIE_OPTIONS);
            }("_f", ctx, a) : t && deleteCookie("_f", ctx);
        }
    };
    let root = "", rootSlash = "";
    var normalize_traling_slashes = function(ctx, next) {
        if ("GET" === ctx.method) {
            const {path} = ctx;
            if (root || (root = new URL(ctx.resolve("root")).pathname.replace(/\/$/, ""), rootSlash = root + "/"), 
            "/" !== path) {
                if (root === path) return ctx.status = 301, ctx.redirect(rootSlash + (ctx.querystring ? "?" + ctx.querystring : ""));
                if (path.endsWith("/") && path !== rootSlash) return ctx.status = 301, ctx.redirect(path.slice(0, -1) + (ctx.querystring ? "?" + ctx.querystring : ""));
            }
        }
        return next();
    }, external_etag_ = __webpack_require__(56), external_etag_default = __webpack_require__.n(external_etag_);
    const assetsUrls = new Array(2);
    var pug = function(ctx, next) {
        return ctx.pug = (file, locals, noCommon) => {
            ctx.type = "html";
            const template = "function" == typeof file ? file : __webpack_require__(117)("./" + file + ".pug"), r = Object.assign({}, ...[ !noCommon && "common" ].concat(null == locals ? void 0 : locals.entries).map(e => e && require(__dirname + "/" + e + "-assets.json"))), styles = Object.values(r).map(r => r.css).filter(Boolean), scripts = Object.values(r).map(r => r.js).filter(Boolean), body = template({
                title: "Beton",
                assetsUrl: assetsUrls[+ctx.secure] || (assetsUrls[+ctx.secure] = resolve_url(ctx, config.h, !ctx.secure && config.d != config.g)),
                favicon: "favicon.ico",
                ...ctx.flash,
                ...locals,
                styles,
                scripts
            });
            ctx.set("Cache-Control", "no-cache, max-age=31536000");
            const et = external_etag_default()(body);
            ctx.set("ETag", et), ctx.headers["if-none-match"] && ctx.headers["if-none-match"].endsWith(et) ? ctx.status = 304 : ctx.body = body;
        }, next();
    }, external_bcryptjs_ = __webpack_require__(31), external_koa_compose_ = __webpack_require__(99), external_koa_compose_default = __webpack_require__.n(external_koa_compose_), external_koa_passport_ = __webpack_require__(32), external_koa_passport_default = __webpack_require__.n(external_koa_passport_), external_koa_session_ = __webpack_require__(100), external_koa_session_default = __webpack_require__.n(external_koa_session_), external_passport_local_ = __webpack_require__(101), external_koa_ = __webpack_require__(102), external_koa_default = __webpack_require__.n(external_koa_);
    let Koa_Koa = class Koa extends external_koa_default.a {
        constructor(settings) {
            super(), this.proxy = !0, this.keys = [ settings.secret ];
        }
    };
    Koa_Koa = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Settings.Settings ]) ], Koa_Koa);
    function sessionSig(ctx) {
        return ctx.cookies.get("_s.sig");
    }
    function destroySession(ctx) {
        const id = ctx.state.user && ctx.state.user.id, bId = sessionSig(ctx);
        id && ctx.res.once("finish", () => function destroyByUserId(id, browserId) {
            const m = RPCServers.get(id);
            m && (browserId ? m.forEach(v => v.browserId == browserId && v.dispose()) : m.forEach(v => v.dispose()));
        }(id, bId)), ctx.session = null;
    }
    let AuthRegister_AuthRegister = class AuthRegister {
        constructor(userRepository, app) {
            external_koa_passport_default.a.serializeUser((user, done) => {
                done(null, user.id);
            }), external_koa_passport_default.a.deserializeUser((id, done) => {
                userRepository.findOneOrFail(id).then(user => done(null, user)).catch(err => done(err, !1));
            }), external_koa_passport_default.a.use(new external_passport_local_.Strategy({
                usernameField: "email",
                passwordField: "password"
            }, async (email, password, done) => {
                try {
                    email = Object(string_tools_.trim)(email), password = Object(string_tools_.trim)(password);
                    const user = await userRepository.findOneOrFail({
                        email
                    });
                    await Object(external_bcryptjs_.compare)(password, user.password) ? done(null, user) : done(null, !1);
                } catch (err) {
                    done(err, !1);
                }
            })), this.session = external_koa_session_default()({
                ...DEFAULT_COOKIE_OPTIONS,
                key: "_s",
                maxAge: 31536e6,
                renew: !0,
                signed: !0
            }, app), this.passport = external_koa_passport_default.a.initialize(), this.passportSession = external_koa_passport_default.a.session(), 
            this.passportComposed = external_koa_compose_default()([ this.session, this.passport, this.passportSession ]);
        }
    };
    AuthRegister_AuthRegister = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ User.UserRepository, Koa_Koa ]) ], AuthRegister_AuthRegister);
    var redirect_after_error = redirectRouteName => async function(ctx, next) {
        if (is_ajax(ctx)) await next(); else try {
            await next();
        } catch (err) {
            err instanceof Error ? (console.error.bind(console, "(./src/core/middlewares/redirect-after-error.ts:16)")(err), 
            ctx.status = 500, destroySession(ctx), ctx.namedRedirect(redirectRouteName, 1)) : (err && ctx.addFlash && ("function" == typeof err.forEach ? err.forEach(e => ctx.addFlash(Object(string_tools_.stringify)(e))) : ctx.addFlash(Object(string_tools_.stringify)(err))), 
            ctx.namedRedirect(redirectRouteName));
        }
    }, index_mustache = __webpack_require__(103), index_mustache_default = __webpack_require__.n(index_mustache), external_vue_template_compiler_ = __webpack_require__(104), external_vue_template_es2015_compiler_ = __webpack_require__(105), external_vue_template_es2015_compiler_default = __webpack_require__.n(external_vue_template_es2015_compiler_);
    const templates = {
        single: __webpack_require__(67),
        table: __webpack_require__(68)
    };
    function isExtendableArrayField(v) {
        return Object(types.a)(v, 8);
    }
    function isVisibleField(v) {
        return !Object(types.a)(v, 98);
    }
    function isEditableField(v) {
        return !Object(types.a)(v, 34);
    }
    function isNormalField(v) {
        return !Object(types.a)(v, 98) && !Object(types.a)(v, 4);
    }
    function isFullwidthField(v) {
        return !Object(types.a)(v, 98) && Object(types.a)(v, 4);
    }
    function findPrimaryIndex(props) {
        let i = 0;
        for (let k in props) {
            if (Object(types.a)(props[k], 18)) return i;
            ++i;
        }
        throw new Error("Can't find primary field.");
    }
    function toFunction(code) {
        return `(function(){${code}})`;
    }
    var rtti_to_vue_component = ({props, displayInfo}, can, ctx) => {
        const vueTemplate = templates[displayInfo.display]({
            props,
            EDIT: 1,
            NONE: 0,
            REMOVE: 3,
            SAVE: 2,
            can,
            ctx,
            isExtendableArrayField,
            findPrimaryIndex,
            isVisibleField,
            isEditableField,
            isNormalField,
            isFullwidthField
        }), {render, staticRenderFns} = Object(external_vue_template_compiler_.compile)(vueTemplate, {
            preserveWhitespace: !1
        });
        return external_vue_template_es2015_compiler_default()(`(function(){\nreturn {staticRenderFns:[${staticRenderFns.map(toFunction)}],render:${toFunction(render)}};\n})()`);
    }, external_alphanum_sort_ = __webpack_require__(106), external_alphanum_sort_default = __webpack_require__.n(external_alphanum_sort_), external_fast_glob_ = __webpack_require__(107), external_fast_glob_default = __webpack_require__.n(external_fast_glob_), external_time_stamp_ = __webpack_require__(108), external_time_stamp_default = __webpack_require__.n(external_time_stamp_), singleton_providers_factory = __webpack_require__(46);
    let ValidateSubscriber = class ValidateSubscriber {
        async beforeInsert({entity}) {
            const f = entity.constructor.insertValidate;
            f && await f(entity);
        }
        async beforeUpdate({entity}) {
            const f = entity.constructor.updateValidate;
            f && await f(entity);
        }
    };
    ValidateSubscriber = Object(tslib_es6.a)([ Object(external_typeorm_.EventSubscriber)() ], ValidateSubscriber);
    let BetonAccountsSubscriber_BetonAccountsSubscriber = class BetonAccountsSubscriber {
        listenTo() {
            return BetonAccount.BetonAccount;
        }
        afterUpdate({entity}) {
            entity.subscriptions && entity.subscriptions.length > 0 && external_typescript_ioc_.Container.get(BetonClient_BetonClient).processAccount(entity.id);
        }
        afterInsert({entity}) {
            entity.subscriptions && entity.subscriptions.length > 0 && external_typescript_ioc_.Container.get(BetonClient_BetonClient).processAccount(entity.id);
        }
    };
    BetonAccountsSubscriber_BetonAccountsSubscriber = Object(tslib_es6.a)([ Object(external_typeorm_.EventSubscriber)() ], BetonAccountsSubscriber_BetonAccountsSubscriber);
    let ChannelSubscriber_ChannelSubscriber = class ChannelSubscriber {
        listenTo() {
            return Channel.Channel;
        }
        beforeInsert({entity}) {
            entity.subscriptions && (entity.subscriptions = entity.subscriptions.map(v => v && v.trim()).filter(Boolean));
        }
        beforeUpdate({entity}) {
            entity.subscriptions && (entity.subscriptions = entity.subscriptions.map(v => v && v.trim()).filter(Boolean));
        }
        afterUpdate({entity}) {
            external_typescript_ioc_.Container.get(BetonClient_BetonClient).processSubs(entity.subscriptions);
        }
    };
    ChannelSubscriber_ChannelSubscriber = Object(tslib_es6.a)([ Object(external_typeorm_.EventSubscriber)() ], ChannelSubscriber_ChannelSubscriber);
    var generated = [ ValidateSubscriber, BetonAccountsSubscriber_BetonAccountsSubscriber, ChannelSubscriber_ChannelSubscriber ], subscribers = generated;
    function createDBFilename() {
        return "beton." + external_time_stamp_default()("YYYY.MM.DD[HH.mm.ss]") + ".sqlite3";
    }
    async function getConnection(database, migrations) {
        const hasMigrations = Array.isArray(migrations) && migrations.length > 0, connection = await Object(external_typeorm_.createConnection)({
            type: "sqlite",
            database,
            synchronize: !hasMigrations && !Object(external_fs_.existsSync)(database),
            logging: [ "error", "migration" ],
            entities: entities_entities,
            subscribers,
            migrationsRun: hasMigrations,
            migrations
        });
        return Object.defineProperty(connection, "filename", {
            value: database
        }), connection;
    }
    let sqliteFilename = null;
    const provider = Object(singleton_providers_factory.b)(async () => {
        const alldb = external_alphanum_sort_default()(await external_fast_glob_default()("beton.*].sqlite3", {
            cwd: Object(data_path.a)(),
            absolute: !0,
            extglob: !1,
            braceExpansion: !1,
            caseSensitiveMatch: !1,
            baseNameMatch: !0,
            globstar: !1
        })).reverse();
        sqliteFilename = alldb.find(f => {
            const stat = Object(external_fs_.statSync)(f);
            return stat.size > 4096;
        }) || Object(data_path.a)(createDBFilename());
        try {
            await Promise.all(alldb.map(f => f !== sqliteFilename && external_fs_.promises.unlink(f)));
        } catch (err) {
            console.error.bind(console, "(./src/core/DBConnection.ts:80)")(err);
        }
        let connection = await getConnection(sqliteFilename);
        const migrations = await async function generateMigrations(connection) {
            const {upQueries, downQueries} = await connection.driver.createSchemaBuilder().log();
            if (upQueries.length > 0) {
                const migration = new Function(`return function _${Date.now()}() { }`)(), s = upQueries.reduce((s, q) => s + +q.query.startsWith("DROP INDEX "), 0);
                if (s !== upQueries.length / 2 || s !== upQueries.reduce((s, q) => s + +q.query.startsWith("CREATE INDEX "), 0)) return migration.prototype.up = async function(queryRunner) {
                    console.log("Start migration:");
                    for (let {query} of upQueries) await queryRunner.query(query);
                }, migration.prototype.down = async function(queryRunner) {
                    console.log("Rollback migration:");
                    for (let {query} of downQueries) await queryRunner.query(query);
                }, [ migration ];
            }
            return [];
        }(connection);
        migrations.length > 0 && (await connection.close(), connection = await getConnection(sqliteFilename, migrations)), 
        entities_repositories.forEach(r => r.provider.init(connection));
        for (let e of entities_entities) e.asyncProvider && await e.asyncProvider.init(connection);
        return connection;
    }), dbInit = provider.init;
    let DBConnection_DBConnection = class DBConnection extends external_typeorm_.Connection {};
    DBConnection_DBConnection = Object(tslib_es6.a)([ Object(external_typescript_ioc_.Provided)(provider) ], DBConnection_DBConnection);
    var allow_private_network = function(ctx, next) {
        return ctx.headers.origin && (ctx.set("Access-Control-Allow-Credentials", "true"), 
        ctx.set("Access-Control-Allow-Private-Network", "true"), ctx.set("Access-Control-Allow-Origin", ctx.headers.origin)), 
        next();
    }, external_builtin_status_codes_ = __webpack_require__(49), external_builtin_status_codes_default = __webpack_require__.n(external_builtin_status_codes_);
    function convertError(err) {
        return Object(string_tools_.isJsonable)(err) ? err : Object(string_tools_.stringify)(err);
    }
    var catch_error = async (ctx, next) => {
        try {
            await next();
        } catch (err) {
            const ajax = is_ajax(ctx);
            if (ctx.type = ajax ? "json" : "text", Array.isArray(err)) ctx.status = 400, ctx.body = ajax ? JSON.stringify({
                errors: err.map(convertError)
            }) : err.map(string_tools_.stringify).join("\n"); else {
                if (external_builtin_status_codes_default.a[err]) ctx.status = +err, err = external_builtin_status_codes_default.a[err]; else {
                    const s = 0 | +err.status || 500;
                    ctx.status = s, s >= 500 ? (console.error.bind(console, "(./src/core/middlewares/catch-error.ts:30)")(err), 
                    err = external_builtin_status_codes_default.a[s] || external_builtin_status_codes_default.a[500]) : delete err.status;
                }
                ctx.body = ajax ? JSON.stringify({
                    errors: [ convertError(err) ]
                }) : Object(string_tools_.stringify)(err);
            }
        }
    };
    let MainRouter_MainRouter = class MainRouter extends external_koa_router_default.a {
        constructor() {
            super(), config.d.startsWith("dev.") && this.use(allow_private_network), this.use(catch_error).get("health", config.c, no_store, ctx => {
                ctx.type = "json", ctx.body = '{"success":"ok"}';
            }).get("index", "/", no_store, ctx => {
                ctx.status = 200, ctx.body = "";
            });
        }
    };
    MainRouter_MainRouter = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.b)("design:paramtypes", []) ], MainRouter_MainRouter);
    let checkpass_CheckPassRoutes = class CheckPassRoutes {
        checkpass(ctx) {
            const {action} = ctx.params;
            ctx.pug("checkpass", {
                action: ctx.resolve("checkpass", {
                    action
                })
            });
        }
        async checkpassPost(ctx) {
            ctx.session.confirmed = 0;
            let {password} = ctx.request.body;
            if (!await Object(external_bcryptjs_.compare)(Object(string_tools_.trim)(password), ctx.state.user.password)) throw "Wrong password";
            ctx.session.confirmed = Date.now(), ctx.namedRedirect(ctx.params.action);
        }
    };
    Object(tslib_es6.a)([ get({
        name: "checkpass",
        path: "/checkpass/:action"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], checkpass_CheckPassRoutes.prototype, "checkpass", null), 
    Object(tslib_es6.a)([ post({
        path: "/checkpass/:action"
    }, redirect_after_error("checkpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], checkpass_CheckPassRoutes.prototype, "checkpassPost", null), 
    checkpass_CheckPassRoutes = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton ], checkpass_CheckPassRoutes);
    let install_InstallRoutes = class InstallRoutes {
        constructor(settings, userRepository, settingsRepository) {
            this.settings = settings, this.userRepository = userRepository, this.settingsRepository = settingsRepository;
        }
        install(ctx) {
            this.settings.installed ? ctx.isAuthenticated() ? ctx.namedRedirect("root") : (ctx.addFlash("Already installed. Login please.", 1), 
            ctx.namedRedirect("login")) : ctx.pug("install");
        }
        async installPost(ctx) {
            if (this.settings.installed) ctx.namedRedirect("install"); else {
                let {email, password} = ctx.request.body;
                email = Object(string_tools_.trim)(email), password = Object(string_tools_.trim)(password), 
                User.User.insertValidate({
                    email,
                    password
                });
                let user = await this.userRepository.findOne({
                    email
                });
                if (user) throw "Пользователь с таким email уже существует.";
                user = new User.User, user.email = email, user.password = await Object(external_bcryptjs_.hash)(password, 10), 
                await this.userRepository.insert(user), this.settings.installed = !0, await this.settingsRepository.save(this.settings), 
                await ctx.login(user), ctx.namedRedirect("root");
            }
        }
    };
    Object(tslib_es6.a)([ get({
        name: "install"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], install_InstallRoutes.prototype, "install", null), 
    Object(tslib_es6.a)([ post({
        path: "/install"
    }, redirect_after_error("install"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], install_InstallRoutes.prototype, "installPost", null), 
    install_InstallRoutes = Object(tslib_es6.a)([ Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Settings.Settings, User.UserRepository, Settings.SettingsRepository ]) ], install_InstallRoutes);
    class login_LoginRoutes {
        login(ctx) {
            ctx.pug("login");
        }
        loginPost(ctx, next) {
            return external_koa_passport_default.a.authenticate("local", async (_, user, info) => {
                user ? (await ctx.login(user), ctx.namedRedirect("root")) : (ctx.addFlash(info && info.message || "Invalid login data"), 
                ctx.namedRedirect("login"));
            })(ctx, next);
        }
    }
    Object(tslib_es6.a)([ get({
        name: "login"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], login_LoginRoutes.prototype, "login", null), 
    Object(tslib_es6.a)([ post({
        path: "/login"
    }, external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object, Function ]), Object(tslib_es6.b)("design:returntype", void 0) ], login_LoginRoutes.prototype, "loginPost", null);
    let resetemail_ResetEmailRoutes = class ResetEmailRoutes {
        constructor(userRepository) {
            this.userRepository = userRepository;
        }
        resetemail(ctx) {
            Date.now() - (ctx.session.confirmed || 0) < 12e4 ? ctx.pug("resetemail") : ctx.namedRedirect("checkpass", 0, {
                action: "resetemail"
            });
        }
        async resetemailPost(ctx) {
            let {email} = ctx.request.body;
            email = Object(string_tools_.trim)(email);
            const user = {
                ...ctx.state.user,
                email
            };
            User.User.updateValidate(user), await this.userRepository.save(user), await ctx.login(user), 
            ctx.namedRedirect("root");
        }
    };
    Object(tslib_es6.a)([ get({
        name: "resetemail"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], resetemail_ResetEmailRoutes.prototype, "resetemail", null), 
    Object(tslib_es6.a)([ post({
        path: "/resetemail"
    }, redirect_after_error("resetemail"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], resetemail_ResetEmailRoutes.prototype, "resetemailPost", null), 
    resetemail_ResetEmailRoutes = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ User.UserRepository ]) ], resetemail_ResetEmailRoutes);
    const services = Object.freeze({
        "mail.ru": Object.freeze({
            title: "почту Mail.Ru",
            url: "https://e.mail.ru/"
        }),
        "bk.ru": Object.freeze({
            title: "почту Mail.Ru (bk.ru)",
            url: "https://e.mail.ru/"
        }),
        "list.ru": Object.freeze({
            title: "почту Mail.Ru (list.ru)",
            url: "https://e.mail.ru/"
        }),
        "inbox.ru": Object.freeze({
            title: "почту Mail.Ru (inbox.ru)",
            url: "https://e.mail.ru/"
        }),
        "yandex.ru": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ru/"
        }),
        "ya.ru": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ru/"
        }),
        "yandex.ua": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ua/"
        }),
        "yandex.by": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.by/"
        }),
        "yandex.kz": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.kz/"
        }),
        "yandex.com": Object.freeze({
            title: "Yandex.Mail",
            url: "https://mail.yandex.com/"
        }),
        "gmail.com": Object.freeze({
            title: "Gmail",
            url: "https://mail.google.com/"
        }),
        "googlemail.com": Object.freeze({
            title: "Gmail",
            url: "https://mail.google.com/"
        }),
        "outlook.com": Object.freeze({
            title: "Outlook.com",
            url: "https://mail.live.com/"
        }),
        "hotmail.com": Object.freeze({
            title: "Outlook.com (Hotmail)",
            url: "https://mail.live.com/"
        }),
        "live.ru": Object.freeze({
            title: "Outlook.com (live.ru)",
            url: "https://mail.live.com/"
        }),
        "live.com": Object.freeze({
            title: "Outlook.com (live.com)",
            url: "https://mail.live.com/"
        }),
        "me.com": Object.freeze({
            title: "iCloud Mail",
            url: "https://www.icloud.com/"
        }),
        "icloud.com": Object.freeze({
            title: "iCloud Mail",
            url: "https://www.icloud.com/"
        }),
        "rambler.ru": Object.freeze({
            title: "Рамблер/почта",
            url: "https://mail.rambler.ru/"
        }),
        "yahoo.com": Object.freeze({
            title: "Yahoo! Mail",
            url: "https://mail.yahoo.com/"
        }),
        "ukr.net": Object.freeze({
            title: "почту ukr.net",
            url: "https://mail.ukr.net/"
        }),
        "i.ua": Object.freeze({
            title: "почту I.UA",
            url: "http://mail.i.ua/"
        }),
        "bigmir.net": Object.freeze({
            title: "почту Bigmir.net",
            url: "http://mail.bigmir.net/"
        }),
        "tut.by": Object.freeze({
            title: "почту tut.by",
            url: "https://mail.tut.by/"
        }),
        "inbox.lv": Object.freeze({
            title: "Inbox.lv",
            url: "https://www.inbox.lv/"
        }),
        "mail.kz": Object.freeze({
            title: "почту mail.kz",
            url: "http://mail.kz/"
        })
    });
    var detect_email_service = email => {
        const m = email.split("@"), domain = m && m.length > 1 ? m[m.length - 1] : null;
        return services[domain] || "";
    }, rnd = __webpack_require__(43);
    let resetpass_ResetPassRoutes = class ResetPassRoutes {
        constructor(userRepository) {
            this.userRepository = userRepository, this.resetPasswordTokens = Object.create(null), 
            Object(external_timers_.setInterval)(() => {
                const NOW = Date.now();
                for (let token in this.resetPasswordTokens) NOW - this.resetPasswordTokens[token].time > 12e5 && delete this.resetPasswordTokens[token];
            }, 12e5).unref();
        }
        validateToken(token) {
            let tokenInfo = this.resetPasswordTokens[token];
            if (!tokenInfo) throw "Invalid or expiried recovery url";
            const {time} = tokenInfo;
            if (Date.now() - time > 12e5) throw delete this.resetPasswordTokens[token], "Expiried recovery url";
            return tokenInfo;
        }
        resetpass(ctx) {
            ctx.pug("resetpass", {
                email: ctx.isAuthenticated() ? ctx.state.user.email : ""
            });
        }
        async resetpassPost(ctx) {
            let {email} = ctx.request.body;
            email = Object(string_tools_.trim)(email);
            const user = await this.userRepository.findOne({
                email
            });
            if (!user) throw "User not found";
            const {id} = user;
            for (let t in this.resetPasswordTokens) this.resetPasswordTokens[t].id === id && delete this.resetPasswordTokens[t];
            const token = await Object(rnd.a)();
            this.resetPasswordTokens[token] = {
                id,
                time: Date.now()
            }, await sendEmail(email, "Изменение пароля " + ctx.resolve("root"), "Для продолжения смены пароля перейдите на " + ctx.resolve("newpassword", {
                token
            })), ctx.pug("resetpass-sended", {
                email,
                service: detect_email_service(email)
            });
        }
        newpassword(ctx) {
            const {token} = ctx.params;
            this.validateToken(token), ctx.pug("newpassword", {
                action: ctx.resolve("newpassword", {
                    token
                })
            });
        }
        async newpasswordPost(ctx) {
            const {token} = ctx.params, {id} = this.validateToken(token);
            delete this.resetPasswordTokens[token];
            const user = id && await this.userRepository.findOne(id);
            if (!user) throw "User not found. Попробуйте другой аккаунт.";
            let {password} = ctx.request.body;
            password = Object(string_tools_.trim)(password), User.User.insertValidate({
                email: user.email,
                password
            }), user.password = await Object(external_bcryptjs_.hash)(password, 10), await this.userRepository.save(user), 
            await ctx.login(user), ctx.namedRedirect("root");
        }
    };
    function restart(ctx) {
        ctx.type = "json", ctx.body = '{"success":"ok"}', ctx.res.once("finish", () => process.exit(2));
    }
    Object(tslib_es6.a)([ get({
        name: "resetpass"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], resetpass_ResetPassRoutes.prototype, "resetpass", null), 
    Object(tslib_es6.a)([ post({
        path: "/resetpass"
    }, redirect_after_error("resetpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], resetpass_ResetPassRoutes.prototype, "resetpassPost", null), 
    Object(tslib_es6.a)([ get({
        name: "newpassword",
        path: "/newpassword/:token"
    }, redirect_after_error("resetpass")), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", void 0) ], resetpass_ResetPassRoutes.prototype, "newpassword", null), 
    Object(tslib_es6.a)([ post({
        path: "/newpassword/:token"
    }, redirect_after_error("resetpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], resetpass_ResetPassRoutes.prototype, "newpasswordPost", null), 
    resetpass_ResetPassRoutes = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ User.UserRepository ]) ], resetpass_ResetPassRoutes);
    const EXT_COMPS = new Set([ "wysiwyg", "javascript" ]), WORKER_INFO = require(__dirname + "/workers-assets.json"), COMP_ENT_MAP = {
        javascript: "monaco",
        wysiwyg: "wysiwyg"
    };
    let AdminRouter_AdminRouter = class AdminRouter extends external_koa_router_default.a {
        constructor(settings, dbConnection, installRoutes, loginRoutes, resetPassRoutes, checkPassRoutes, resetEmailRoutes, {session, passport, passportSession}) {
            super(), this.use(flash, smart_redirect, normalize_traling_slashes, redirect_after_error("login"), session, passport, passportSession, pug), 
            this.put("logout", "/logout", ctx => {
                ctx.status = 204, destroySession(ctx);
            }), applyRoutes(this, installRoutes), settings.installed || this.use((settings => function(ctx, next) {
                if (settings.installed) return next();
                ctx.namedRedirect("install");
            })(settings)), applyRoutes(this, loginRoutes), applyRoutes(this, resetPassRoutes), 
            this.use(check_auth), applyRoutes(this, checkPassRoutes), applyRoutes(this, resetEmailRoutes);
            const componentsCache = {};
            this.get("root", "/", ctx => {
                const user = ctx.state.user;
                if (!componentsCache[user.role]) {
                    const components = [], validators = new Set;
                    for (let entity of entities_entities) {
                        let {name, rtti, checkAccess, hooks} = entity;
                        name = external_pluralize_default()(name);
                        const can = Object.create(null);
                        for (let action of [ "GET", "PATCH", "PUT", "DELETE" ]) try {
                            can[action] = checkAccess(action, ctx.state.user);
                        } catch {}
                        if (can.GET && rtti.displayInfo.display) {
                            can.CHANGE = can.PUT || can.PATCH;
                            const render = rtti_to_vue_component(rtti, can, ctx), remote = Object.values(rtti.props).filter(v => v.remote).map(v => {
                                const i = v.remote.lastIndexOf("."), c = v.remote.slice(0, i), method = v.remote.slice(i + 1);
                                if (!Object(string_tools_.isValidVar)(c) || !Object(string_tools_.isValidVar)(method) || method == v.remote) throw "Invalid property " + Object(string_tools_.tosource)(v);
                                return {
                                    c,
                                    method
                                };
                            });
                            for (const p in rtti.props) validators.add(rtti.props[p].type);
                            components.push({
                                name,
                                sortable: !(!can.PATCH || !rtti.displayInfo.sortable || "single" === rtti.displayInfo.display),
                                order: 0 | rtti.displayInfo.order,
                                icon: rtti.displayInfo.icon,
                                render,
                                can: Object(string_tools_.tosource)(can, null, ""),
                                rtti: Object(string_tools_.tosource)(rtti, null, ""),
                                hooks: Object(string_tools_.tosource)(hooks, null, ""),
                                endPoint: Object(external_param_case_.paramCase)(name),
                                remote
                            });
                        }
                    }
                    const classes = [], m = external_typescript_ioc_.Scope.Singleton.constructor.instances;
                    m.forEach((v, k) => classes.push({
                        name: k.name,
                        props: Object.keys(v).filter(p => !/^[_#]/.test(p))
                    }));
                    const {stack} = external_typescript_ioc_.Container.get(MainRouter_MainRouter);
                    componentsCache[user.role] = {
                        components,
                        Paths: stack.filter(l => l.name).map(l => {
                            if (l.paramNames.length > 0) {
                                const tokens = Object(external_path_to_regexp_.parse)(l.path);
                                return {
                                    name: l.name,
                                    url: `function (${l.paramNames.map(p => p.name).join(", ")}) { return ${tokens.map(p => p.constructor == String ? JSON.stringify(p) : p.name).join(' + "/" + ')}}`
                                };
                            }
                            return {
                                name: l.name,
                                url: JSON.stringify(l.path)
                            };
                        }),
                        entries: Array.from(external_set_utils_default.a.map(external_set_utils_default.a.intersect(validators, EXT_COMPS), c => COMP_ENT_MAP[c])).concat("index"),
                        HasPty: AVALIBLE,
                        EDITOR_WORKER: WORKER_INFO["editor.worker"].js,
                        TYPESCRIPT_WORKER: WORKER_INFO["typescript.worker"].js
                    };
                }
                ctx.pug("index", {
                    entries: componentsCache[user.role].entries,
                    inlineScript: index_mustache_default()({
                        ...componentsCache[user.role],
                        email: user.email,
                        Flash: JSON.stringify(ctx.flash)
                    })
                }, !0);
            }), this.get("backup", "/backup", no_store, external_koa_compress_default()(), ctx => {
                ctx.attachment(createDBFilename()), ctx.body = Object(external_fs_.createReadStream)(dbConnection.filename);
            }), this.put("restore", "/restore", async ctx => {
                await fs_cp(ctx.req, Object(data_path.a)(createDBFilename())), restart(ctx);
            }), this.put("restart", "/restart", restart);
        }
    };
    async function data2Model(ctx, model, repository) {
        const {body} = ctx.request, {props} = model.constructor.rtti;
        for (let k in body) {
            const p = props[k];
            p && !Object(types.a)(p, 34) && (model[k] = body[k]);
        }
        model.beforeFilter && await model.beforeFilter();
        const o = await repository.save(model);
        return o.afterFilter && await o.afterFilter(), o;
    }
    function msgpack(ctx, data) {
        ctx.type = "application/x-msgpack";
        const body = Array.isArray(data) ? data.length > 0 ? encode({
            fields: Object.keys(data[0]),
            values: data.map(v => Object.values(v))
        }) : null : null != data ? encode(data) : null;
        if ("GET" === ctx.method) {
            const et = body && external_etag_default()(body);
            if (et) {
                if (ctx.set("Cache-Control", "no-cache, max-age=31536000"), ctx.set("ETag", et), 
                ctx.headers["if-none-match"] && ctx.headers["if-none-match"].endsWith(et)) return void (ctx.status = 304);
            } else setNoStoreHeader(ctx);
        }
        ctx.body = body;
    }
    AdminRouter_AdminRouter = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.c)(6, external_typescript_ioc_.Inject), Object(tslib_es6.c)(7, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Settings.Settings, DBConnection_DBConnection, install_InstallRoutes, login_LoginRoutes, resetpass_ResetPassRoutes, checkpass_CheckPassRoutes, resetemail_ResetEmailRoutes, AuthRegister_AuthRegister ]) ], AdminRouter_AdminRouter);
    let ApiRouter_ApiRouter = class ApiRouter extends external_koa_router_default.a {
        constructor(connection, {session, passport, passportSession}) {
            super(), this.use(session, passport, passportSession);
            for (let e of entities_entities) {
                const repository = connection.getRepository(e), access = check_entity_access(e), name = Object(external_param_case_.paramCase)(external_pluralize_default()(e.name)), singltonInstance = e.asyncProvider ? external_typescript_ioc_.Container.get(e) : null, saveModel = singltonInstance ? async (ctx, model) => {
                    const o = await data2Model(ctx, model, repository), {props} = e.rtti;
                    for (let p in props) singltonInstance[p] = o[p], Object(types.a)(props[p], 2) || delete o[p];
                    ctx.status = 201, msgpack(ctx, o);
                } : async (ctx, model) => {
                    const o = await data2Model(ctx, model, repository), {props} = e.rtti;
                    for (let p in props) Object(types.a)(props[p], 2) || delete o[p];
                    ctx.status = 201, msgpack(ctx, o);
                };
                this.get(name, "/" + name, access, async ctx => {
                    msgpack(ctx, await repository.find());
                }).get(`/${name}/:id`, access, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) throw 404;
                    msgpack(ctx, model);
                }).put("/" + name, access, body_parse_msgpack, ctx => saveModel(ctx, new e)).patch(`/${name}/:id`, access, body_parse_msgpack, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) throw 404;
                    await saveModel(ctx, model);
                }).delete("/" + name, access, async ctx => {
                    await repository.clear(), ctx.body = null;
                }).delete(`/${name}/:id`, access, async ctx => {
                    await repository.delete(ctx.params.id), ctx.body = null;
                });
            }
        }
    };
    ApiRouter_ApiRouter = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ DBConnection_DBConnection, AuthRegister_AuthRegister ]) ], ApiRouter_ApiRouter);
    var external_cheerio_ = __webpack_require__(38), external_cheerio_default = __webpack_require__.n(external_cheerio_), external_mailparser_ = __webpack_require__(109), external_stream_ = __webpack_require__(110);
    class AsyncQueue {
        constructor() {
            this.objects = [], this.resolvers = [];
        }
        [Symbol.asyncIterator]() {
            return this;
        }
        get size() {
            return this.objects.length;
        }
        add(value, priority = 0, nosort) {
            if (this.resolvers.length > 0) this.resolvers.shift()({
                done: !1,
                value
            }); else {
                let i = 0;
                nosort || (i = this.objects.findIndex(v => v.priority > priority)) < 0 ? this.objects.push({
                    value,
                    priority
                }) : this.objects.splice(i, 0, {
                    value,
                    priority
                });
            }
        }
        filter(f) {
            this.objects = this.objects.filter(v => f(v.value, v.priority));
        }
        delete(f) {
            for (let i = this.objects.length - 1; i >= 0; --i) {
                const {value, priority} = this.objects[i];
                f(value, priority) && this.objects.splice(i, 1);
            }
        }
        next() {
            return new Promise(resolve => {
                this.objects.length > 0 ? resolve({
                    done: !1,
                    value: this.objects.shift().value
                }) : this.resolvers.push(resolve);
            });
        }
    }
    var external_worker_threads_ = __webpack_require__(24);
    class PWorker_PWorker extends external_worker_threads_.Worker {
        kill(e) {
            this.dead || (this.dead = !0, this.removeAllListeners("error"), this.removeAllListeners("exit"), 
            this.removeAllListeners("message"), this.terminate().catch(console.error.bind(console, "(./src/PWorker.ts:18)")), 
            this.reject(e));
        }
        constructor(workerData) {
            super(__dirname + "/mail-worker.js", {
                workerData
            }), this.dead = !1, this.unref(), this.promise = new Promise((fullfil, reject) => {
                this.fulfill = fullfil, this.reject = reject;
            }), this.once("error", e => {
                console.error.bind(console, "(./src/PWorker.ts:35)")(e), this.kill(e);
            }), this.once("exit", code => {
                console.error.bind(console, "(./src/PWorker.ts:40)")(code), this.kill(code);
            }), this.on("message", value => {
                value.err ? (console.error.bind(console, "(./src/PWorker.ts:46)")(value.err), this.kill(value.err)) : value.folders ? (this.emit("folders", value.folders), 
                this.fulfill(this)) : value.source ? this.emit("mail", value) : value.info && console.log.bind(console, "(./src/PWorker.ts:54)")(value.info);
            });
        }
    }
    const TMP_IMAGE_DIR = external_make_dir_default.a.sync(Object(data_path.a)("mail"));
    function cleanHtml(html) {
        const doc = external_cheerio_default.a.load(`<!DOCTYPE html><html><head><meta charset="UTF-8"></head><body style="font-family:Ubuntu,Segoe UI,Optima,Trebuchet MS,-apple-system,BlinkMacSystemFont,sans-serif;">${html}</body></html>`);
        return doc("blockquote").prev().remove(), doc("blockquote").remove(), doc('[class*="quote"]').remove(), 
        doc.html();
    }
    const NO_RESTART = Symbol("no_restart");
    let MailClient_MailClient = class MailClient {
        constructor(tgclient, settings) {
            this.tgclient = tgclient, this.settings = settings, this.boxes = [], this.messages = new AsyncQueue, 
            this.workers = Object.create(null), this.processMessages();
        }
        searchChatTitles(q) {
            return this.boxes.filter(b => b.includes(q));
        }
        async processMessages() {
            for await (let m of this.messages) try {
                if (await this.tgclient.hasChannelsWithSubscription(m.folder)) {
                    let {textAsHtml, html, date} = await Object(external_mailparser_.simpleParser)(Buffer.from(m.source.buffer, m.source.byteOffset, m.source.byteLength), {
                        skipHtmlToText: !0,
                        skipTextLinks: !0
                    });
                    const tmpImage = await Object(external_tmp_promise_.tmpName)({
                        postfix: ".png",
                        dir: TMP_IMAGE_DIR
                    });
                    await new Promise((resolve, reject) => {
                        const cp = Object(external_child_process_.exec)(`wkhtmltoimage --width 430 --encoding utf-8 --quiet --disable-javascript --format png - - | pngquant --speed 11 100 - > "${tmpImage}"`, {
                            windowsHide: !0
                        }, err => {
                            err ? reject(err) : resolve();
                        });
                        external_stream_.Readable.from([ cleanHtml(html || textAsHtml) ]).pipe(cp.stdin);
                    });
                    const content = await this.tgclient.uploadAndUnlink(tmpImage, 1, this.tgclient.parseMD("`" + date.toTimeString().split(" (", 1)[0] + "`"));
                    delete content.caption;
                    const f = new Forecast.Forecast;
                    f.subscription = m.folder, f.added = date, f.info = content, this.tgclient.saveForecast(f);
                }
            } catch (err) {
                console.error.bind(console, "(./src/MailClient.ts:81)")(err), sendErrorEmail("Ошибка обработки email");
            }
        }
        connect(workerData, dropSame) {
            var _a;
            const hash = Object.entries(workerData).filter(([k]) => k.startsWith("email") || k.startsWith("imap")).flatMap(String).sort().join(), lastDead = null === (_a = this.workers[hash]) || void 0 === _a ? void 0 : _a.dead;
            return (dropSame || lastDead) && (lastDead && this.workers[hash].kill(NO_RESTART), 
            this.workers[hash] = null), this.workers[hash] || (this.workers[hash] = new PWorker_PWorker(workerData)).on("folders", folders => this.boxes.splice(0, this.boxes.length, ...folders)).on("mail", m => this.messages.add(m, 0, !0)).promise.then(worker => {
                for (let k in this.workers) {
                    const w = this.workers[k];
                    w !== worker && (delete this.workers[k], w.kill(NO_RESTART));
                }
            }, async e => {
                e !== NO_RESTART && (this.settings.imapReconnectPause > 0 && await Object(utils_delay.a)(1e3 * this.settings.imapReconnectPause), 
                this.connect(this.settings, !1));
            }), this.workers[hash].promise;
        }
    };
    MailClient_MailClient = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ TgClient_TgClient, ClientSettings.ClientSettings ]) ], MailClient_MailClient);
    var external_playwright_firefox_ = __webpack_require__(64), external_playwright_firefox_default = __webpack_require__.n(external_playwright_firefox_), external_vk_io_ = __webpack_require__(111), authorization_ = __webpack_require__(65);
    async function loadCookies(path) {
        return function(filename) {
            try {
                return Object(external_fs_.statSync)(filename).size > 0;
            } catch {}
            return !1;
        }(path) ? function fromNetscapeCookies(cookies) {
            return cookies.split("\n").filter(s => s && !s.startsWith("#")).map(s => {
                const [domain, , path, secure, expires, name, value] = s.split("\t");
                return {
                    name,
                    value,
                    domain,
                    path,
                    expires: 0 | +expires,
                    httpOnly: !1,
                    secure: "TRUE" === secure,
                    sameSite: "Lax"
                };
            });
        }(await external_fs_.promises.readFile(path, "utf-8")) : [];
    }
    function findTypeOf(a, ...props) {
        for (let p of props) {
            const v = a.find(e => e.type === p);
            if (v) return v;
        }
    }
    class VKClient_VKAuthError extends extendable_error.a {}
    const NAV_OPTIONS = {
        timeout: 1e4,
        waitUntil: "domcontentloaded",
        state: "visible"
    };
    async function processPage(page, phone, password, twoFaHandler) {
        for (let [selector, cb] of [ [ 'input[name="login"]', () => phone ], [ 'input[name="password"]', () => password ], [ 'input[name="otp"]', async () => twoFaHandler(await page.innerText("h3")) ] ]) {
            const $el = await page.$(selector);
            if ($el) for (let error = ""; ;) {
                await $el.fill(await (error ? twoFaHandler(BaseTelegramClient_PromptTelegramBot.plainText(error, await page.innerText("h3"))) : cb()));
                const [r] = await Promise.all([ Promise.race([ page.waitForNavigation(NAV_OPTIONS).then(_ => "nav"), page.waitForSelector(".vkc__TextField__errorMessage", NAV_OPTIONS) ]), page.click('button[type="submit"]') ]);
                if ("string" == typeof r) return;
                error = await r.innerText();
            }
        }
        throw new VKClient_VKAuthError(await page.innerText("body"));
    }
    const SUPPORTED_MESSAGES = new Set([ "audio_message", "doc", "sticker", "photo" ]);
    let VKClient_VKClient = class VKClient {
        constructor(tg, coreSettings, settings, coreSettingsRepository) {
            this.tg = tg, this.coreSettings = coreSettings, this.settings = settings, this.coreSettingsRepository = coreSettingsRepository, 
            this.processMessage = async (message, next) => {
                var _a;
                try {
                    let {text, id, isInbox} = message;
                    if (isInbox && filter_text_message(text, this.settings)) {
                        const hasAttaches = message.attachments.some(a => SUPPORTED_MESSAGES.has(a.type));
                        if (hasAttaches || text) {
                            const title = this.getTitles(await this.client.api.messages.getConversationsById({
                                peer_ids: message.peerId,
                                extended: !0
                            }))[0];
                            if (await this.tg.hasChannelsWithSubscription(title)) {
                                await message.loadMessagePayload();
                                const urls = message.attachments.map(a => {
                                    if ("photo" === a.type) return {
                                        type: 1,
                                        url: new URL(findTypeOf(a.sizes, "w", "z", "y", "x", "m", "s").url)
                                    };
                                    if ("doc" === a.type) {
                                        const url = new URL(a.url);
                                        return url.hash = Object(external_sanitize_filename_ts_.sanitize)(a.title), {
                                            type: 2,
                                            url
                                        };
                                    }
                                    return "audio_message" === a.type ? {
                                        type: 5,
                                        url: new URL(a.url)
                                    } : "sticker" === a.type ? {
                                        type: 1,
                                        url: new URL((images = a.payload.images, images.reduce((last, s) => !last || s.width * s.height > last.width * last.height ? s : last, null)).url)
                                    } : void 0;
                                    var images;
                                }).filter(Boolean), attachment = urls.shift();
                                if (!text && !attachment) return;
                                const tmessage = this.tg.createTextInput(text, !1, message.attachments.some(a => "link" === a.type)), f = await this.tg.createForecast(title, id, attachment ? await this.tg.uploadAndUnlink(attachment.url, attachment.type, tmessage.text) : tmessage, message.createdAt, null === (_a = message.replyMessage) || void 0 === _a ? void 0 : _a.id);
                                if (f && urls.length > 0) for await (let c of new PromisesIterator(urls.map(a => this.tg.uploadAndUnlink(a.url, a.type)))) f.info = c, 
                                this.tg.saveForecast(f);
                            }
                        }
                    }
                } catch (err) {
                    console.error.bind(console, "(./src/VKClient.ts:236)")(message, err);
                }
                return next();
            };
        }
        getTitles({items, profiles, groups}) {
            return items.map(i => {
                switch (i.peer.type) {
                  case "chat":
                    return i.chat_settings.title.trim();

                  case "user":
                    const {first_name, last_name} = profiles.find(p => p.id === i.peer.id);
                    return (first_name + " " + last_name).trim();

                  case "group":
                    const {name} = groups.find(p => p.id === -i.peer.id);
                    return name.trim();
                }
                return "";
            }).filter(Boolean);
        }
        async searchChatTitles(q, count) {
            if (q && this.client) {
                const conv = await this.client.api.messages.searchConversations({
                    q,
                    count,
                    extended: !0
                });
                return this.getTitles(conv);
            }
            return [];
        }
        dispose() {
            var _a;
            null === (_a = this.client) || void 0 === _a || _a.updates.stop(), this.client = null;
        }
        async getToken(phone, password, ctx) {
            if (this.coreSettings.vk.token && phone === this.coreSettings.vk.phone && password === this.coreSettings.vk.password) return this.coreSettings.vk.token;
            const twoFaHandler = ctx ? async payload => {
                const {value} = await fromCtx(ctx).remote("ElMessageBox").prompt(payload, {
                    showCancelButton: !1,
                    roundButton: !0
                }, "Vk Auth");
                return value;
            } : async payload => new BaseTelegramClient_PromptTelegramBot(phone, () => "*Vk Auth*\n" + payload).wait(), browser = await external_playwright_firefox_default.a.firefox.launch({
                headless: !0
            }), context = await browser.newContext({
                ...external_playwright_firefox_default.a.devices["Pixel 2"],
                isMobile: !1,
                javaScriptEnabled: !0,
                locale: "ru-RU",
                ignoreHTTPSErrors: !0,
                permissions: []
            }), cookiePath = Object(data_path.a)(Object(external_sanitize_filename_ts_.sanitize)(phone + "_cookies.txt"));
            try {
                await context.addCookies(await loadCookies(cookiePath));
                const allowed = new Set([ "document", "script", "xhr", "fetch" ]);
                await context.route("**", (route, request) => {
                    const t = request.resourceType();
                    allowed.has(t) ? route.continue() : route.fulfill({
                        status: 201
                    });
                });
                const page = await context.newPage();
                for (await page.goto("https://m.vk.com/docs", NAV_OPTIONS); page.url().startsWith("https://id.vk.com/auth"); ) await processPage(page, phone, password, twoFaHandler);
                const token = await async function getAccess(phone, password, page, twoFaHandler) {
                    for (const params = {
                        grant_type: "password",
                        client_id: authorization_.officialAppCredentials.windowsPhone.clientId,
                        client_secret: authorization_.officialAppCredentials.windowsPhone.clientSecret,
                        username: phone,
                        password,
                        scope: 462876..toString(),
                        v: "5.131",
                        "2fa_supported": "1"
                    }; ;) {
                        const resp = await page.goto("https://oauth.vk.com/token?" + new URLSearchParams(params).toString());
                        delete params.code;
                        let {access_token, error, redirect_uri, validation_type, phone_mask, error_description, error_type} = await resp.json();
                        if ("wrong_otp" === error_type || "otp_format_is_incorrect" === error_type || "need_validation" === error && validation_type) {
                            const code = await twoFaHandler(BaseTelegramClient_PromptTelegramBot.plainText(error_description) + "Введите код двуфакторной авторизации из " + ("2fa_sms" === validation_type ? `sms (отправлено на номер \`${phone_mask}\`) или воспользуйтесь резервными кодами.` : "сообщения от Администрации или из приложения для генерации кодов или воспользуйтесь резервными кодами.\nДля принудительной отправки кода по sms введите /sms (стало срабатывать не всегда)."));
                            "/sms" === code ? params.force_sms = "1" : params.code = code;
                        } else if ("need_validation" === error && redirect_uri) {
                            for (await page.goto(redirect_uri, NAV_OPTIONS); !page.url().includes("success=1"); ) if (await processPage(page, phone, password, twoFaHandler), 
                            page.url().includes("fail=1")) throw new VKClient_VKAuthError(await page.innerText("body"));
                            access_token = new URLSearchParams(new URL(page.url()).hash.slice(1)).get("access_token");
                        } else if (error) throw new VKClient_VKAuthError(`[${error}]: ` + error_description);
                        if (access_token) return access_token;
                    }
                }(phone, password, page, twoFaHandler);
                return this.coreSettings.vk = {
                    phone,
                    password,
                    token
                }, await this.coreSettingsRepository.save(this.coreSettings), token;
            } finally {
                await function saveCookies(cookies, path) {
                    return external_fs_.promises.writeFile(path, function toNetscapeCookies(cookies) {
                        return cookies.map(c => [ c.domain, c.domain.startsWith(".") ? "TRUE" : "FALSE", c.path, c.secure ? "TRUE" : "FALSE", c.expires, c.name, c.value ].join("\t")).join("\n");
                    }(cookies));
                }(await context.cookies(), cookiePath), browser.close().catch(console.error.bind(console, "(./src/VKClient.ts:300)"));
            }
        }
        async update(settings, ctx) {
            if (!this.client || !this.coreSettings.vk.token || settings.vkPhone !== this.coreSettings.vk.phone || settings.vkPassword !== this.coreSettings.vk.password) {
                const token = await this.getToken(settings.vkPhone, settings.vkPassword, ctx);
                this.dispose(), this.client = new external_vk_io_.VK({
                    token
                }), this.client.updates.on("message", this.processMessage), await this.client.updates.startPolling();
            }
        }
    };
    VKClient_VKClient = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ TgClient_TgClient, Settings.Settings, ClientSettings.ClientSettings, Settings.SettingsRepository ]) ], VKClient_VKClient);
    const SIMPLE_EMAIL_RE = /^.+@[a-z\-\d]+\.[a-z]+$/i;
    let App_App = class App {
        constructor(coreSettings, betonClient, mailClient, capchaHandlers, vkClient, tgClient, settings, betonAccountRepository, mainRouter, settingsRepository, apiRouter, adminRouter) {
            this.coreSettings = coreSettings, this.betonClient = betonClient, this.mailClient = mailClient, 
            this.capchaHandlers = capchaHandlers, this.vkClient = vkClient, this.tgClient = tgClient, 
            this.settings = settings, this.betonAccountRepository = betonAccountRepository, 
            applyRoutes(mainRouter, capchaHandlers), applyRoutes(adminRouter, this), RPC_RPC.register("TgClient", tgClient), 
            RPC_RPC.register("subscription", {
                async load(query) {
                    if (query = query.trim(), query && !SIMPLE_EMAIL_RE.test(query)) {
                        const titles = new Set((await Promise.all([ tgClient.searchChatTitles(query, 7), vkClient.searchChatTitles(query, 7), mailClient.searchChatTitles(query) ])).flat());
                        for (let a of await betonAccountRepository.find()) a.subscriptions.forEach(s => titles.add(s));
                        const lMap = Object.create(null), tArray = [];
                        for (let t of titles) tArray.push(t), lMap[t] = Object(external_fast_levenshtein_.get)(query, t, {
                            useCollator: !0
                        });
                        return tArray.sort((a, b) => lMap[a] - lMap[b]), tArray.length = Math.min(7, tArray.length), 
                        tArray;
                    }
                    return [];
                }
            }), this.tgClient.emmiter.on("reload", () => this.reload()), apiRouter.patch("tgclient_save", "/tgclient-save/:id", check_entity_access(ClientSettings.ClientSettings), body_parse_msgpack, smart_redirect, async ctx => {
                const body = ctx.request.body;
                ClientSettings.ClientSettings.insertValidate(body);
                const {vkPhone, vkPassword, ruCapchaKey} = body;
                if (ruCapchaKey != settings.ruCapchaKey) try {
                    await capchaHandlers.updateCallback(ruCapchaKey);
                } catch (err) {
                    console.error.bind(console, "(./src/App.ts:109)")(ruCapchaKey, err);
                    const message = Object(string_tools_.stringify)(err);
                    throw [ {
                        field: "ruCapchaKey",
                        message
                    } ];
                }
                try {
                    await this.vkClient.update(body, ctx);
                } catch (err) {
                    console.error.bind(console, "(./src/App.ts:120)")(vkPhone, vkPassword, err);
                    const message = Object(string_tools_.stringify)(err);
                    throw [ {
                        field: "vkPhone",
                        message
                    }, {
                        field: "vkPassword",
                        message
                    } ];
                }
                await this.tgClient.update({
                    type: "user",
                    ...body
                }, ctx);
                try {
                    await this.mailClient.connect(body, !1);
                } catch (err) {
                    console.error.bind(console, "(./src/App.ts:133)")(body, err);
                    const message = Object(string_tools_.stringify)(err);
                    throw [ {
                        field: "email",
                        message
                    }, {
                        field: "emailPassword",
                        message
                    }, {
                        field: "imapHost",
                        message
                    }, {
                        field: "imapPort",
                        message
                    } ];
                }
                const {props} = ClientSettings.ClientSettings.rtti;
                for (let k in body) {
                    const p = props[k];
                    p && !Object(types.a)(p, 98) && (settings[k] = body[k]);
                }
                await settingsRepository.save(settings), ctx.status = 201, msgpack(ctx, settings);
            });
        }
        async reload(ctx) {
            this.tgClient.rotate(), await Promise.all([ this.checkMail(), this.checkBeton() ]), 
            ctx && (ctx.status = 201);
        }
        async vkreload(ctx) {
            this.coreSettings.vk.token = null, await this.vkClient.update(this.settings, ctx), 
            ctx && (ctx.status = 201);
        }
        async checkMail() {
            const {settings} = this;
            if (settings.email && settings.emailPassword && settings.imapHost && settings.imapPort) try {
                await this.mailClient.connect(settings, !0), console.log.bind(console, "(./src/App.ts:194)")("Mail connected", settings.email);
            } catch (err) {
                console.error.bind(console, "(./src/App.ts:196)")(err), sendErrorEmail("Ошибка запуска email клиента.");
            }
        }
        async checkVk() {
            const {settings} = this;
            if (settings.vkPassword && settings.vkPhone) try {
                await this.vkClient.update(settings), console.log.bind(console, "(./src/App.ts:207)")("Vk connected", settings.vkPhone);
            } catch (err) {
                console.error.bind(console, "(./src/App.ts:209)")(err), sendErrorEmail("Ошибка запуска vk клиента. Проверьте телефон и пароль.");
            }
        }
        async checkBeton() {
            if (this.settings.deviceName && this.settings.deviceSerial) {
                const accaunts = await this.betonAccountRepository.find();
                if (accaunts.length) {
                    accaunts.sort((a, b) => a.subscriptions.length - b.subscriptions.length), this.betonClient.cancelPause();
                    for (let a of accaunts) {
                        try {
                            await a.actualizeSubscriptions(this.settings);
                        } catch (err) {
                            console.error.bind(console, "(./src/App.ts:225)")(a, err);
                            break;
                        }
                        await this.betonAccountRepository.save(a, {
                            reload: !1
                        }).catch(err => console.error.bind(console, "(./src/App.ts:228)")(a.login, err));
                    }
                }
            }
        }
        async init() {
            const {settings} = this;
            if (settings.ruCapchaKey) try {
                console.log.bind(console, "(./src/App.ts:240)")("Capcha hook install", await this.capchaHandlers.updateCallback(settings.ruCapchaKey));
            } catch (err) {
                console.error.bind(console, "(./src/App.ts:242)")(err), sendErrorEmail("Ошибка установки callback адреса для сервиса распознавания капч.");
            }
            if (settings.phone) try {
                await this.tgClient.update({
                    type: "user",
                    ...settings
                }), console.log.bind(console, "(./src/App.ts:250)")("Tg connected", settings.phone);
            } catch (err) {
                console.error.bind(console, "(./src/App.ts:252)")(err), this.tgClient.sendErrorEmail();
            }
            await Promise.allSettled([ this.checkMail(), this.checkVk(), this.checkBeton() ]);
        }
    };
    Object(tslib_es6.a)([ put({
        name: "reload"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], App_App.prototype, "reload", null), 
    Object(tslib_es6.a)([ put({
        name: "vkreload"
    }), Object(tslib_es6.b)("design:type", Function), Object(tslib_es6.b)("design:paramtypes", [ Object ]), Object(tslib_es6.b)("design:returntype", Promise) ], App_App.prototype, "vkreload", null), 
    App_App = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.c)(6, external_typescript_ioc_.Inject), Object(tslib_es6.c)(7, external_typescript_ioc_.Inject), Object(tslib_es6.c)(8, external_typescript_ioc_.Inject), Object(tslib_es6.c)(9, external_typescript_ioc_.Inject), Object(tslib_es6.c)(10, external_typescript_ioc_.Inject), Object(tslib_es6.c)(11, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ Settings.Settings, BetonClient_BetonClient, MailClient_MailClient, BetonCapchaHandlers_BetonCapchaHandlers, VKClient_VKClient, TgClient_TgClient, ClientSettings.ClientSettings, BetonAccount.BetonAccountRepository, MainRouter_MainRouter, ClientSettings.ClientSettingsRepository, ApiRouter_ApiRouter, AdminRouter_AdminRouter ]) ], App_App);
    let core_Main = class Main {
        constructor(baseApp, authRegister, app, router, apiRouter, adminRouter) {
            this.baseApp = baseApp, this.authRegister = authRegister, this.app = app, this.router = router, 
            this.apiRouter = apiRouter, this.adminRouter = adminRouter;
        }
        async main() {
            var _a;
            const {router, apiRouter, adminRouter, app, baseApp} = this;
            await baseApp.init(), router.use(config.b, apiRouter.routes(), apiRouter.allowedMethods()), 
            router.use(config.a, adminRouter.routes(), adminRouter.allowedMethods()), app.use(router.routes()).use(router.allowedMethods()).use(external_koa_mount_default()(config.h, ctx => external_koa_send_default()(ctx, ctx.path, {
                root: Object(external_path_.normalize)(__dirname + config.h),
                maxage: 31536e6,
                gzip: !0,
                brotli: !1,
                setHeaders(res) {
                    res.setHeader("Access-Control-Allow-Credentials", "true"), res.setHeader("Access-Control-Allow-Private-Network", "true"), 
                    res.setHeader("Access-Control-Allow-Origin", "http://" + config.d), res.setHeader("Cache-Control", "no-store, no-cache, max-age=0");
                }
            })));
            const server = Object(external_http_.createServer)(app.callback()), {passportComposed} = this.authRegister, wss = new external_ws_.Server({
                server,
                verifyClient: (info, callback) => {
                    try {
                        const ctx = app.createContext(info.req, null);
                        passportComposed(ctx, () => (callback(!!ctx.state.user), null)).catch(console.error);
                    } catch (err) {
                        console.error.bind(console, "(./src/core/index.ts:81)")(err);
                    }
                }
            });
            wss.on("connection", async (socket, request) => {
                try {
                    const ctx = app.createContext(request, null);
                    await passportComposed(ctx, () => {
                        let terminalSize = ctx.query.cols && ctx.query.rows ? {
                            cols: +ctx.query.cols || 100,
                            rows: +ctx.query.rows || 40
                        } : null;
                        return new ServerRPC_ServerRPC(socket, parseInt(ctx.query.tab), sessionSig(ctx), ctx.state.user, terminalSize), 
                        null;
                    });
                } catch (err) {
                    console.error.bind(console, "(./src/core/index.ts:104)")(err), socket.close();
                }
            }), Object(external_timers_.setInterval)(pingServers, 1e4, 8e3).unref();
            const [currentIp, hostIps] = await Promise.all([ await get_my_ip(), await Object(external_util_.promisify)(external_dns_.resolve4)(config.d).catch(console.error.bind(console, "(./src/core/index.ts:113)")), new Promise(resolve => server.listen(config.f, resolve)) ]);
            console.log(`Current ip: ${currentIp} \nHost: ${config.g} [${hostIps || ""}]\nAdmin url: http://${config.d}` + config.a), 
            null === (_a = process.send) || void 0 === _a || _a.call(process, "ready");
        }
    };
    core_Main = Object(tslib_es6.a)([ external_typescript_ioc_.Singleton, Object(tslib_es6.c)(0, external_typescript_ioc_.Inject), Object(tslib_es6.c)(1, external_typescript_ioc_.Inject), Object(tslib_es6.c)(2, external_typescript_ioc_.Inject), Object(tslib_es6.c)(3, external_typescript_ioc_.Inject), Object(tslib_es6.c)(4, external_typescript_ioc_.Inject), Object(tslib_es6.c)(5, external_typescript_ioc_.Inject), Object(tslib_es6.b)("design:paramtypes", [ App_App, AuthRegister_AuthRegister, Koa_Koa, MainRouter_MainRouter, ApiRouter_ApiRouter, AdminRouter_AdminRouter ]) ], core_Main), 
    dbInit().then(() => external_typescript_ioc_.Container.get(core_Main).main()).catch(err => {
        console.error.bind(console, "(./src/core/index.ts:128)")(err), process.exit(1);
    });
} ]);