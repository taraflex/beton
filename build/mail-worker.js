!function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: !1,
            exports: {}
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
        module.l = !0, module.exports;
    }
    __webpack_require__.m = modules, __webpack_require__.c = installedModules, __webpack_require__.d = function(exports, name, getter) {
        __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
            enumerable: !0,
            get: getter
        });
    }, __webpack_require__.r = function(exports) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(exports, "__esModule", {
            value: !0
        });
    }, __webpack_require__.t = function(value, mode) {
        if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
        if (4 & mode && "object" == typeof value && value && value.__esModule) return value;
        var ns = Object.create(null);
        if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
            enumerable: !0,
            value
        }), 2 & mode && "string" != typeof value) for (var key in value) __webpack_require__.d(ns, key, function(key) {
            return value[key];
        }.bind(null, key));
        return ns;
    }, __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module.default;
        } : function getModuleExports() {
            return module;
        };
        return __webpack_require__.d(getter, "a", getter), getter;
    }, __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 131);
}({
    112: function(module, exports) {
        module.exports = require("imapflow");
    },
    131: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.r(__webpack_exports__);
        var imapflow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(112), p_map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(34), p_map__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(p_map__WEBPACK_IMPORTED_MODULE_1__), worker_threads__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(24), _utils_AsyncUniqQueue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(35), _utils_delay__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(29), _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8);
        const {imapActionMaxTime, imapHost, imapPort, email, emailPassword, imapBeforePingPause, imapReconnectPause} = worker_threads__WEBPACK_IMPORTED_MODULE_2__.workerData;
        class MailTimeoutError extends _utils_extendable_error__WEBPACK_IMPORTED_MODULE_5__.a {}
        const race = imapActionMaxTime > 0 ? p => Promise.race([ p, new Promise((_, reject) => setTimeout(reject, 1e3 * imapActionMaxTime, new MailTimeoutError).unref()) ]) : p => p, queue = new _utils_AsyncUniqQueue__WEBPACK_IMPORTED_MODULE_3__.a, add = queue.add.bind(queue);
        class Imap extends imapflow__WEBPACK_IMPORTED_MODULE_0__.ImapFlow {
            constructor() {
                super({
                    host: imapHost,
                    port: imapPort,
                    secure: !0,
                    tls: {
                        rejectUnauthorized: !1
                    },
                    logger: {
                        debug: void 0,
                        info: void 0,
                        warn: console.warn.bind(console, "(./src/MailWorker.ts:74)"),
                        error: console.error.bind(console, "(./src/MailWorker.ts:75)")
                    },
                    auth: {
                        user: email,
                        pass: emailPassword
                    },
                    clientInfo: {
                        name: "beton"
                    }
                }), this.finalyze = async err => {
                    if (this.cancelReCheck(), queue.delete(this), this.removeAllListeners("error"), 
                    this.removeAllListeners("close"), this.removeAllListeners("exists"), await this.logout().catch(Boolean), 
                    this.folder) for (err && console.error.bind(console, "(./src/MailWorker.ts:49)")(err); ;) try {
                        await Object(_utils_delay__WEBPACK_IMPORTED_MODULE_4__.a)(1e3 * imapReconnectPause), 
                        await (new Imap).connectAndOpen(this.folder);
                        break;
                    } catch (err) {
                        console.error.bind(console, "(./src/MailWorker.ts:57)")(err);
                    } else worker_threads__WEBPACK_IMPORTED_MODULE_2__.parentPort.postMessage({
                        err
                    });
                };
            }
            async connectAndOpen(folder) {
                return this.usable || (await race(this.connect()), this.once("error", this.finalyze), 
                this.once("close", this.finalyze), this.on("exists", data => {
                    console.log.bind(console, "(./src/MailWorker.ts:93)")(data), add(this, !0);
                })), folder && (this.folder = folder, await race(this.mailboxOpen(folder))), add(this), 
                this;
            }
            planReCheck() {
                this.timeout = setTimeout(add, 1e3 * imapBeforePingPause, this).unref();
            }
            cancelReCheck() {
                clearTimeout(this.timeout);
            }
        }
        (async function main() {
            const client = await (new Imap).connectAndOpen(), folders = Array.from(function* findAllBets(folder) {
                if (folder.folders) for (let f of folder.folders) yield* findAllBets(f); else /^beton\b/i.test(folder.path) && (yield folder.path);
            }(await race(client.listTree())));
            await p_map__WEBPACK_IMPORTED_MODULE_1___default()(folders, (folder, i) => (0 === i ? client : new Imap).connectAndOpen(folder), {
                concurrency: 6
            }), worker_threads__WEBPACK_IMPORTED_MODULE_2__.parentPort.postMessage({
                folders
            });
            for await (let c of queue) {
                c.cancelReCheck();
                const uIds = await race(c.search({
                    seen: !1
                }, {
                    uid: !0
                }));
                if ((null == uIds ? void 0 : uIds.length) > 0) {
                    for await (let {source} of c.fetch(uIds.join(), {
                        source: !0
                    }, {
                        uid: !0
                    })) worker_threads__WEBPACK_IMPORTED_MODULE_2__.parentPort.postMessage({
                        source,
                        folder: c.folder
                    });
                    await c.messageFlagsAdd(uIds.join(), [ "\\Seen" ], {
                        uid: !0
                    });
                }
                c.planReCheck();
            }
        })().catch(err => worker_threads__WEBPACK_IMPORTED_MODULE_2__.parentPort.postMessage({
            err
        }));
    },
    24: function(module, exports) {
        module.exports = require("worker_threads");
    },
    29: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var _taraflex_cancelation_token__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
        function timeFn(connection, resolve) {
            connection.dispose(), resolve();
        }
        __webpack_exports__.a = function(time, cancelationToken) {
            return new Promise(cancelationToken ? (resolve, reject) => {
                const connection = cancelationToken.connect(() => {
                    clearTimeout(timeout), reject(new _taraflex_cancelation_token__WEBPACK_IMPORTED_MODULE_0__.CancelationTokenError);
                }), timeout = setTimeout(timeFn, time, connection, resolve);
            } : resolve => {
                setTimeout(resolve, time);
            });
        };
    },
    30: function(module, exports) {
        module.exports = require("@taraflex/cancelation-token");
    },
    34: function(module, exports) {
        module.exports = require("p-map");
    },
    35: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.d(__webpack_exports__, "a", (function() {
            return AsyncUniqQueue;
        }));
        class AsyncUniqQueue {
            constructor() {
                this.objects = [], this.resolvers = [];
            }
            [Symbol.asyncIterator]() {
                return this;
            }
            get size() {
                return this.objects.length;
            }
            add(value, first) {
                this.resolvers.length > 0 ? this.resolvers.shift()({
                    done: !1,
                    value
                }) : first ? (this.delete(value), this.objects.unshift(value)) : this.objects.includes(value) || this.objects.push(value);
            }
            delete(value) {
                const i = this.objects.indexOf(value);
                i > -1 && this.objects.splice(i, 1);
            }
            next() {
                return new Promise(resolve => {
                    this.objects.length > 0 ? resolve({
                        done: !1,
                        value: this.objects.shift()
                    }) : this.resolvers.push(resolve);
                });
            }
        }
    },
    8: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_require__.d(__webpack_exports__, "a", (function() {
            return ExtendableError;
        }));
        class ExtendableError extends Error {
            constructor(message) {
                super(message), this.name = this.constructor.name, Error.captureStackTrace(this, this.constructor);
            }
        }
    }
});