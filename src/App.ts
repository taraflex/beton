import { get as levenshtein } from 'fast-levenshtein';
import { Context } from 'koa';
import { Inject, Singleton } from 'typescript-ioc';

import { EntityClass, has, RTTIItemState } from '@crud/types';
import { BetonAccountRepository } from '@entities/BetonAccount';
import { ClientSettings, ClientSettingsRepository } from '@entities/ClientSettings';
import { Settings } from '@entities/Settings';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import smartRedirect from '@middlewares/smart-redirect';
import { RPC } from '@rpc/RPC';
import { stringify } from '@taraflex/string-tools';
import { applyRoutes, put } from '@utils/routes-helpers';
import { sendErrorEmail } from '@utils/send-email';

import { BetonCapchaHandlers } from './BetonCapchaHandlers';
import { BetonClient } from './BetonClient';
import { AdminRouter } from './core/AdminRouter';
import { ApiRouter, msgpack } from './core/ApiRouter';
import { MainRouter } from './core/MainRouter';
import { MailClient } from './MailClient';
import { TgClient } from './TgClient';
import { VKClient } from './VKClient';

const SIMPLE_EMAIL_RE = /^.+@[a-z\-\d]+\.[a-z]+$/i;

@Singleton
export class App {
    constructor(
        @Inject private readonly coreSettings: Settings,
        @Inject private readonly betonClient: BetonClient,
        @Inject private readonly mailClient: MailClient,
        @Inject private readonly capchaHandlers: BetonCapchaHandlers,
        @Inject private readonly vkClient: VKClient,
        @Inject private readonly tgClient: TgClient,
        @Inject private readonly settings: ClientSettings,
        @Inject private readonly betonAccountRepository: BetonAccountRepository,
        @Inject mainRouter: MainRouter,
        @Inject settingsRepository: ClientSettingsRepository,
        @Inject apiRouter: ApiRouter,
        @Inject adminRouter: AdminRouter
    ) {
        applyRoutes(mainRouter, capchaHandlers);
        applyRoutes(adminRouter, this);

        RPC.register('TgClient', tgClient);
        RPC.register('subscription', {
            async load(query: string) {
                query = query.trim();
                if (query && !SIMPLE_EMAIL_RE.test(query)) {

                    const titles: Set<string> = new Set((await Promise.all([
                        tgClient.searchChatTitles(query, 7),
                        vkClient.searchChatTitles(query, 7),
                        mailClient.searchChatTitles(query),
                    ])).flat());

                    for (let a of await betonAccountRepository.find()) {
                        a.subscriptions.forEach(s => titles.add(s));
                    }

                    const lMap = Object.create(null);
                    const tArray = [];
                    for (let t of titles) {
                        tArray.push(t);
                        lMap[t] = levenshtein(query, t, { useCollator: true });
                    }
                    tArray.sort((a, b) => lMap[a] - lMap[b]);
                    tArray.length = Math.min(7, tArray.length);
                    return tArray;
                } else {
                    return [];
                }
            }
        });

        this.tgClient.emmiter.on('reload', () => this.reload());

        // mainRouter.get('parse', '/parse/:secret', noStore, async (ctx: Context) => {
        //     if (ctx.params.secret === coreSettings.linkSecret) {
        //         try {
        //             await this.reload(ctx);
        //             ctx.status = 200;
        //             ctx.type = 'js';
        //             ctx.body = 'window.opener||(window.location.href="https://bet-hub.com/user/picks_active/")';
        //         } catch (err) {
        //             ctx.status = 200;
        //             ctx.type = 'js';
        //             ctx.body = 'renderError(`' + stringify(err) + '`)';
        //         }
        //     } else {
        //         ctx.status = 401;
        //         ctx.body = '';
        //     }
        // });

        apiRouter.patch('tgclient_save', '/tgclient-save/:id', checkEntityAccess(ClientSettings), bodyParseMsgpack, smartRedirect, async (ctx: Context) => {
            const body: ClientSettings = <any>ctx.request.body;

            (ClientSettings as EntityClass<ClientSettings>).insertValidate(body);

            const { vkPhone, vkPassword, ruCapchaKey } = body;

            if (ruCapchaKey != settings.ruCapchaKey) {
                try {
                    await capchaHandlers.updateCallback(ruCapchaKey);
                } catch (err) {
                    LOG_ERROR(ruCapchaKey, err);
                    const message = stringify(err);
                    throw [
                        { field: 'ruCapchaKey', message }
                    ];
                }
            }

            try {
                await this.vkClient.update(body, ctx);
            } catch (err) {
                LOG_ERROR(vkPhone, vkPassword, err);
                const message = stringify(err);
                throw [
                    { field: 'vkPhone', message },
                    { field: 'vkPassword', message }
                ];
            }

            await this.tgClient.update({ type: 'user', ...body }, ctx);

            try {
                await this.mailClient.connect(body, false);
            } catch (err) {
                LOG_ERROR(body, err);
                const message = stringify(err);
                throw [
                    { field: 'email', message },
                    { field: 'emailPassword', message },
                    { field: 'imapHost', message },
                    { field: 'imapPort', message }
                ];
            }

            const { props } = (ClientSettings as EntityClass).rtti;
            for (let k in body) {
                const p = props[k];
                if (p && !has(p, RTTIItemState.HIDDEN)) {
                    settings[k] = body[k];
                }
            }

            await settingsRepository.save(settings);

            ctx.status = 201;
            msgpack(ctx, settings);
        });
    }

    @put({ name: 'reload' })
    async reload(ctx?: Context) {
        this.tgClient.rotate();
        await Promise.all([this.checkMail(), this.checkBeton()]);
        if (ctx) {
            ctx.status = 201;
        }
    }

    @put({ name: 'vkreload' })
    async vkreload(ctx?: Context) {
        this.coreSettings.vk.token = null;
        await this.vkClient.update(this.settings, ctx);
        if (ctx) {
            ctx.status = 201;
        }
    }

    // @get({ name: 'get-reload-link' }, noStore)
    // getReloadLink(ctx: Context) {
    //     ctx.body = 'https://taraflex.gitlab.io/beton/?url=' + encodeURIComponent(resolveUrl(ctx, '/parse/' + this.coreSettings.linkSecret));
    // }

    // @patch({ name: 'update-reload-link' })
    // async updateReloadLink(ctx: Context) {
    //     await this.coreSettings.updateLinkSecret();
    //     await this.coreSettingsRepository.save(this.coreSettings);
    //     ctx.status = 201;
    //     this.getReloadLink(ctx);
    // }

    async checkMail() {
        const { settings } = this;
        if (settings.email && settings.emailPassword && settings.imapHost && settings.imapPort) {
            try {
                await this.mailClient.connect(settings, true);
                LOG_INFO('Mail connected', settings.email);
            } catch (err) {
                LOG_ERROR(err);
                sendErrorEmail('Ошибка запуска email клиента.');
            }
        }
    }

    async checkVk() {
        const { settings } = this;
        if (settings.vkPassword && settings.vkPhone) {
            try {
                await this.vkClient.update(settings);
                LOG_INFO('Vk connected', settings.vkPhone);
            } catch (err) {
                LOG_ERROR(err);
                sendErrorEmail('Ошибка запуска vk клиента. Проверьте телефон и пароль.');
            }
        }
    }

    async checkBeton() {
        if (this.settings.deviceName && this.settings.deviceSerial) {
            const accaunts = await this.betonAccountRepository.find();
            if (accaunts.length) {
                accaunts.sort((a, b) => a.subscriptions.length - b.subscriptions.length);
                this.betonClient.cancelPause();
                for (let a of accaunts) {
                    try {
                        await a.actualizeSubscriptions(this.settings);
                    } catch (err) {
                        LOG_ERROR(a, err);
                        break;
                    }
                    await this.betonAccountRepository.save(a, { reload: false }).catch(err => LOG_ERROR(a.login, err));
                }
            }
        }
    }

    async init() {

        const { settings } = this;

        if (settings.ruCapchaKey) {
            try {
                LOG_INFO('Capcha hook install', await this.capchaHandlers.updateCallback(settings.ruCapchaKey));
            } catch (err) {
                LOG_ERROR(err);
                sendErrorEmail('Ошибка установки callback адреса для сервиса распознавания капч.');
            }
        }

        if (settings.phone) {
            try {
                await this.tgClient.update({ type: 'user', ...settings });
                LOG_INFO('Tg connected', settings.phone);
            } catch (err) {
                LOG_ERROR(err);
                this.tgClient.sendErrorEmail();
            }
        }

        await Promise.allSettled([
            this.checkMail(),
            this.checkVk(),
            this.checkBeton()
        ]);
    }
}