import { Worker } from 'worker_threads';

import { ClientSettings } from '@entities/ClientSettings';

export class PWorker extends Worker {

    protected fulfill: (value: PWorker | PromiseLike<PWorker>) => void;
    protected reject: (reason?: any) => void;

    dead = false;

    kill(e?: any) {
        if (!this.dead) {
            this.dead = true;
            this.removeAllListeners('error');
            this.removeAllListeners('exit');
            this.removeAllListeners('message');
            this.terminate().catch(LOG_ERROR);
            this.reject(e);
        }
    }

    readonly promise: Promise<PWorker>;

    constructor(workerData: ClientSettings) {
        super(__dirname + '/mail-worker.js', { workerData });
        this.unref();

        this.promise = new Promise<PWorker>((fullfil, reject) => {
            this.fulfill = fullfil;
            this.reject = reject;
        });

        this.once('error', (e) => {
            LOG_ERROR(e);
            this.kill(e);
        });

        this.once('exit', code => {
            LOG_ERROR(code);
            this.kill(code);
        })

        this.on('message', (value) => {
            if (value.err) {
                LOG_ERROR(value.err);
                this.kill(value.err);
            } else if (value.folders) {
                this.emit('folders', value.folders);
                this.fulfill(this);
            } else if (value.source) {
                this.emit('mail', value);
            } else if (value.info) {
                LOG_INFO(value.info);
            }
        });
    }
}