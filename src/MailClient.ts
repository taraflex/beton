import cheerio from 'cheerio';
import { exec } from 'child_process';
import { simpleParser } from 'mailparser';
import makeDir from 'make-dir';
import { Readable } from 'stream';
import { tmpName } from 'tmp-promise';
import { Inject, Singleton } from 'typescript-ioc';

import { ClientSettings } from '@entities/ClientSettings';
import { Forecast } from '@entities/Forecast';
import { AsyncQueue } from '@utils/AsyncQueue';
import dataPath from '@utils/data-path';
import delay from '@utils/delay';
import { sendErrorEmail } from '@utils/send-email';

import { AttachmentType } from './core/BaseTelegramClient';
import { PWorker } from './PWorker';
import { TgClient } from './TgClient';
import { inputMessagePhoto } from 'tdlib-types';

const TMP_IMAGE_DIR = makeDir.sync(dataPath('mail'));

function cleanHtml(html: string) {
    const doc = cheerio.load(`<!DOCTYPE html><html><head><meta charset="UTF-8"></head><body style="font-family:Ubuntu,Segoe UI,Optima,Trebuchet MS,-apple-system,BlinkMacSystemFont,sans-serif;">${html}</body></html>`);

    doc('blockquote').prev().remove();
    doc('blockquote').remove();
    doc('[class*="quote"]').remove();

    return doc.html();
}

const NO_RESTART = Symbol('no_restart');

@Singleton
export class MailClient {
    protected readonly boxes: string[] = [];
    protected readonly messages = new AsyncQueue<{ folder: string, source: Uint8Array }>();

    constructor(
        @Inject protected readonly tgclient: TgClient,
        @Inject protected readonly settings: ClientSettings
    ) {
        this.processMessages();
    }

    searchChatTitles(q: string): string[] {
        return this.boxes.filter(b => b.includes(q));
    }

    protected async processMessages() {
        for await (let m of this.messages) {
            try {
                if (await this.tgclient.hasChannelsWithSubscription(m.folder)) {
                    //@ts-ignore
                    let { textAsHtml, html, date } = await simpleParser(Buffer.from(m.source.buffer, m.source.byteOffset, m.source.byteLength), { skipHtmlToText: true, skipTextLinks: true });
                    const tmpImage = await tmpName({ postfix: '.png', dir: TMP_IMAGE_DIR });

                    await new Promise<void>((resolve, reject) => {
                        const cp = exec(`wkhtmltoimage --width 430 --encoding utf-8 --quiet --disable-javascript --format png - - | pngquant --speed 11 100 - > "${tmpImage}"`, { windowsHide: true }, (err) => {
                            if (err) {
                                reject(err)
                            } else {
                                resolve();
                            }
                        });
                        Readable.from([cleanHtml(html || textAsHtml)]).pipe(cp.stdin);
                    });

                    const content = (await this.tgclient.uploadAndUnlink(tmpImage, AttachmentType.PIC, this.tgclient.parseMD("`" + date.toTimeString().split(' (', 1)[0] + "`"))) as inputMessagePhoto;
                    delete content.caption;

                    const f = new Forecast();
                    f.subscription = m.folder;
                    f.added = date;
                    f.info = content;
                    this.tgclient.saveForecast(f);
                }
            }
            catch (err) {
                LOG_ERROR(err);
                sendErrorEmail('Ошибка обработки email');
            }
        }
    }

    protected readonly workers: Record<string, PWorker> = Object.create(null);

    connect(workerData: ClientSettings, dropSame: boolean): Promise<PWorker> {
        const hash = Object.entries(workerData).filter(([k]) => k.startsWith('email') || k.startsWith('imap')).flatMap(String).sort().join();
        const lastDead = this.workers[hash]?.dead;
        if (dropSame || lastDead) {
            if (lastDead) this.workers[hash].kill(NO_RESTART);
            this.workers[hash] = null;
        }
        if (!this.workers[hash]) {
            (this.workers[hash] = new PWorker(workerData))
                .on('folders', folders => this.boxes.splice(0, this.boxes.length, ...folders))
                .on('mail', m => this.messages.add(m, 0, true))
                .promise.then(worker => {
                    for (let k in this.workers) {
                        const w = this.workers[k];
                        if (w !== worker) {
                            delete this.workers[k];
                            w.kill(NO_RESTART);
                        }
                    }
                }, async e => {
                    if (e !== NO_RESTART) {
                        if (this.settings.imapReconnectPause > 0) {
                            await delay(this.settings.imapReconnectPause * 1000);
                        }
                        this.connect(this.settings, false);
                    }
                })
        }
        return this.workers[hash].promise;
    }
}