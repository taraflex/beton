import { BetonClient } from 'src/BetonClient';
import { EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { Channel } from '@entities/Channel';

@EventSubscriber()
export class ChannelSubscriber implements EntitySubscriberInterface<Channel> {

    listenTo() {
        return Channel;
    }

    beforeInsert({ entity }: InsertEvent<Channel>) {
        if (entity.subscriptions) {
            entity.subscriptions = entity.subscriptions.map(v => v && v.trim()).filter(Boolean);
        }
    }

    beforeUpdate({ entity }: UpdateEvent<Channel>) {
        if (entity.subscriptions) {
            entity.subscriptions = entity.subscriptions.map(v => v && v.trim()).filter(Boolean);
        }
    }

    afterUpdate({ entity }: UpdateEvent<Channel>) {
        Container.get(BetonClient).processSubs(entity.subscriptions);
    }
}