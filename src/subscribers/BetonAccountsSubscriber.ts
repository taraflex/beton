import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { BetonAccount } from '@entities/BetonAccount';

import { BetonClient } from '../BetonClient';

@EventSubscriber()
export class BetonAccountsSubscriber implements EntitySubscriberInterface<BetonAccount> {

    listenTo() {
        return BetonAccount;
    }

    afterUpdate({ entity }: UpdateEvent<BetonAccount>) {
        if (entity.subscriptions && entity.subscriptions.length > 0) {
            Container.get(BetonClient).processAccount(entity.id);
        }
    }

    afterInsert({ entity }: UpdateEvent<BetonAccount>) {
        if (entity.subscriptions && entity.subscriptions.length > 0) {
            Container.get(BetonClient).processAccount(entity.id);
        }
    }
}
