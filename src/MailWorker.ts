import { ImapFlow, ListTreeResponse } from 'imapflow';
import pMap from 'p-map';
import { parentPort, workerData } from 'worker_threads';

import { AsyncUniqQueue } from '@utils/AsyncUniqQueue';
import delay from '@utils/delay';
import { ExtendableError } from '@utils/extendable-error';

import type { ClientSettings } from '@entities/ClientSettings';

function* findAllBets(folder: ListTreeResponse): Generator<string, void> {
    if (folder.folders) {
        for (let f of folder.folders) {
            yield* findAllBets(f);
        }
    } else if (/^beton\b/i.test(folder.path)) {
        yield folder.path;
    }
}

const { imapActionMaxTime, imapHost, imapPort, email, emailPassword, imapBeforePingPause, imapReconnectPause } = workerData as ClientSettings;

class MailTimeoutError extends ExtendableError { }

const race = imapActionMaxTime > 0 ?
    <T>(p: PromiseLike<T>) => Promise.race([
        p,
        new Promise<never>((_, reject) => setTimeout(reject, imapActionMaxTime * 1000, new MailTimeoutError()).unref())
    ]) :
    <T>(p: PromiseLike<T>) => p as Promise<T>;

const queue = new AsyncUniqQueue<Imap>();
const add = queue.add.bind(queue);

class Imap extends ImapFlow {

    folder?: string;
    protected timeout?: ReturnType<typeof setTimeout>;

    readonly finalyze = async (err: any) => {
        this.cancelReCheck();
        queue.delete(this);
        this.removeAllListeners('error');
        this.removeAllListeners('close');
        this.removeAllListeners('exists');
        await this.logout().catch(Boolean);
        if (this.folder) {
            if (err) {
                LOG_ERROR(err);
            }
            for (; ;) {
                try {
                    await delay(imapReconnectPause * 1000);
                    await new Imap().connectAndOpen(this.folder);
                    break;
                } catch (err) {
                    LOG_ERROR(err);
                }
            }
        } else {
            parentPort.postMessage({ err });
        }
    }

    constructor() {
        super({
            host: imapHost,
            port: imapPort,
            secure: true,
            tls: { rejectUnauthorized: false },
            logger: {
                debug: 0 && DEBUG ? LOG_INFO : undefined,
                info: DEBUG ? LOG_INFO : undefined,
                warn: LOG_WARN,
                error: LOG_ERROR
            },
            auth: {
                user: email,
                pass: emailPassword
            },
            clientInfo: { name: APP_NAME }
        });
    }

    async connectAndOpen(folder?: string) {

        if (!this.usable) {
            await race(this.connect());

            this.once('error', this.finalyze);
            this.once('close', this.finalyze);
            this.on('exists', data => {
                LOG_INFO(data);
                add(this, true);
            });
        }

        if (folder) {
            this.folder = folder;
            await race(this.mailboxOpen(folder));
        }

        add(this);

        return this;
    }

    planReCheck() {
        this.timeout = setTimeout(add, imapBeforePingPause * 1000, this).unref();
    }

    cancelReCheck() {
        clearTimeout(this.timeout);
    }
}

async function main() {

    const client = await new Imap().connectAndOpen();
    const folders = Array.from(findAllBets(await race(client.listTree())));

    await pMap(folders, (folder, i) => (i === 0 ? client : new Imap()).connectAndOpen(folder), { concurrency: 6 });

    parentPort.postMessage({ folders });

    for await (let c of queue) {
        c.cancelReCheck();
        const uIds = await race(c.search({ seen: false }, { uid: true }));
        if (uIds?.length > 0) {
            for await (let { source } of c.fetch(
                uIds.join(),
                { source: true },
                //@ts-ignore
                { uid: true }
            )) {
                parentPort.postMessage({ source, folder: c.folder });
            }
            await c.messageFlagsAdd(uIds.join(), ['\\Seen'], { uid: true });
        }
        c.planReCheck();
    }
}

main().catch(err => parentPort.postMessage({ err }));