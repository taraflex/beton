import { ClientSettings } from '@entities/ClientSettings';

export default function (text: string, settings: ClientSettings): boolean {
    if (text) {
        if (!new RegExp(settings.whiteFilter, 'gi').test(text)) {
            return false
        }
        if (new RegExp(settings.blackFilter, 'gi').test(text)) {
            return false;
        }
    }
    return true;
}