import cheerio from 'cheerio';
import { js as beautify } from 'js-beautify';
import { AttachmentType } from 'src/core/BaseTelegramClient';
import { TgClient } from 'src/TgClient';
import { URL } from 'url';
import { runInNewContext } from 'vm';

import { Forecast } from '@entities/Forecast';
import { ExtendableError } from '@utils/extendable-error';
import { escMdForce as esc } from '@utils/tg-utils';

export function unPack(code: string): string {

    runInNewContext(code, {
        eval(c: string) {
            code = c;
        },
        window: {},
        document: {}
    });

    return beautify(code).replace(/(JsHttpRequest\.query|grecaptcha\.render|grecaptcha\.ready|grecaptcha\.execute|\$\.ajax|\$\("#\w+"\)\.click\()/g, 'return $1').replace(/div\.innerHTML\s*=\s*/g, 'return ');
}

export function last<T>(a: T[]) {
    return a[a.length - 1];
}

const months = {
    'января': 'jan',
    'февраля': 'feb',
    'марта': 'mar',
    'апреля': 'apr',
    'мая': 'may',
    'июня': 'jun',
    'июля': 'jul',
    'августа': 'aug',
    'сентября': 'sep',
    'октября': 'oct',
    'ноября': 'nov',
    'декабря': 'dec'
}

function parseDate(s: string) {
    return new Date(s.replace(/января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря/i, m => months[m]).replace(/[^\w:\+\s]+/g, '').replace(/\s+/g, ' '));
}

function sportIcon(s: string): string {
    switch (s) {
        case 'Теннис':
            return '🎾 ';
        case 'Футбол':
            return '⚽️ ';
        case 'Волейбол':
            return '🏐 ';
        case 'Бейсбол':
            return '⚾️ ';
        case 'Баскетбол':
            return '🏀 ';
        case 'Хоккей':
            return '🏒 ';
        case 'Киберспорт':
            return '🎮 ';
        case 'Боевые искусства':
            return '🤼‍♂️ ';
        case 'Регби':
            return '🏈 ';
        case 'Бокс':
            return '🥊 ';
        case 'Гандбол':
            return '🤾‍♂️ ';
    }
    return '';
}

export function toHtmlArray($: cheerio.Root, c: cheerio.Cheerio): string[] {
    return c.map((_, e) => $(e).html().trim()).toArray() as any;
}

export function toTextArray($: cheerio.Root, c: cheerio.Cheerio): string[] {
    return c.map((_, e) => $(e).text().trim()).toArray() as any;
}

function TEXT(c: cheerio.Cheerio) {
    c.children().after('<span> </span>');
    return esc(c.text().trim());
}

export class BannedError extends ExtendableError { }
export class AutoBidError extends ExtendableError { }
export class EmptyLinkError extends ExtendableError { }
export class InvalidSubscription extends ExtendableError { }
export class ExpiredError extends ExtendableError { }
export class CapchaError extends ExtendableError { }
export class Ban10MinutesError extends CapchaError {
    public readonly expire = Date.now() + 12 * 60 * 1000;
}

export async function tableToForecast(tg: TgClient, html: string): Promise<Forecast> {
    if (html.includes('Вы не смогли пройти капчу три раза в течение 10 минут')) {
        throw new Ban10MinutesError();
    }
    if (html == 'RCV2' || html.includes('Recaptcha') || html.includes(' капч') || html.includes('токен безопасности')) {
        throw new CapchaError(html);
    }
    if (html.includes('Показ прогноза запрещён')) {
        throw new BannedError(html);
    }
    if (html.includes('Данный прогноз доступен только для авто-ставок')) {
        throw new AutoBidError(html);
    }
    if (html.includes('Использование прогноза невозможно: данное событие уже')) {
        throw new ExpiredError(html);
    }
    const tables = cheerio.load(html, { decodeEntities: false })('table');

    //sub
    const cs = tables.find('.comment_sub_name');
    cs.find('div,span').remove();
    const subscription = cs.text().trimLeft().split(/\s/, 1)[0];
    if (!/\.(L|UT|U)$/.test(subscription)) {
        throw new InvalidSubscription(html);
    }

    //location
    const loc = TEXT(tables.find('.location'));

    //sport
    const sport = new Function('function alert(v){ return v.split("|",2)[1] }; return ' + tables.find('.sport').attr('onclick'))();

    //comment
    const comment = (esc(tables.find('.center').text().trim()) + '\n' + TEXT(tables.find('.comment').children().first())).trim();

    //dates
    const dates = (new Function('function alert(v){ return v }; return ' + tables.find('.header_td').children().first().attr('onclick'))() as string).split(/\n+/);
    const enterTime = dates.find(s => s.startsWith('Введено: '));
    const actionTime = dates.find(s => s.startsWith('Событие: '));

    //protection
    const img = tables.find('img[src^="/cache/sales_protection/"]').attr('src');

    const f = new Forecast();

    if (img) {
        f.info = await tg.uploadAndUnlink(new URL('https://bet-hub.com' + img), AttachmentType.PIC, tg.parseMD(`${sportIcon(sport)}${esc(sport)}
*${loc}* 
${actionTime ? esc(actionTime.replace('Событие: ', '')) : ''}
\`Подписка\` ${esc(subscription)}
${comment}`.trim()));
    } else {
        //event
        const eventLink = tables.find('.event > a');
        const eventUrl = (eventLink.attr('href') || '').replace('/_seo/subs_mobile.php?q=', 'https://bet-hub.com/sub/');
        if (!eventUrl) {
            throw new EmptyLinkError(html);
        }
        const event = TEXT(eventLink);

        //book
        const book = tables.find('.book > a');
        const infoRow = book.parent().parent();
        let { bookLink, bookName } = new Function(`function go_book_mobile(bookLink, bookName) { return { bookLink, bookName } }; return {${book.attr('href') || 'javascript:go_book_mobile("https://bet-hub.com/","")'}}.javascript`)();
        bookLink = bookLink || 'https://bet-hub.com/';
        bookName = bookName || new URL(bookLink).hostname;

        f.info = tg.createTextInput(`${sportIcon(sport)}${esc(sport)}
*${loc}* 
${actionTime ? esc(actionTime.replace('Событие: ', '')) : ''}
\`Событие    \` [${event}](${eventUrl})
\`Прогноз    \` ${TEXT(infoRow.find('.outcome'))}
\`Ставка     \` ${TEXT(infoRow.find('.stake'))}
\`Коэффициент\` ${TEXT(infoRow.find('.odds'))}
\`Контора    \` [${esc(bookName)}](${bookLink})
\`Подписка   \` [${esc(subscription)}](${eventUrl.replace(/\/picks\/.+/, '')})
${comment}`.trim(), true);
    }
    f.added = enterTime ? parseDate(enterTime) : new Date();
    f.subscription = subscription;
    return f;
}