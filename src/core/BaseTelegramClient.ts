import { Counter, CTR } from 'aes-es';
import { Mutex } from 'async-mutex';
import { decode as base32768 } from 'base32768';
import child_process from 'child_process';
import { existsSync, promises as fs } from 'fs';
import { Context } from 'koa';
import makeDir from 'make-dir';
import { getTdjson } from 'prebuilt-tdlib';
import { sanitize } from 'sanitize-filename-ts';
import { Client } from 'tdl';
import { TDLib } from 'tdl-tdlib-ffi';
import { promisify } from 'util';

import { decode as msgpack } from '@msgpack/msgpack';
import { fromCtx } from '@rpc/ServerRPC';
import { stringify } from '@taraflex/string-tools';
import { PORT } from '@utils/config';
import dataPath from '@utils/data-path';
import { ExtendableError } from '@utils/extendable-error';
import http from '@utils/http';
import rndId from '@utils/rnd-id';
import { sendErrorEmail } from '@utils/send-email';
import { BOT_TOKEN_RE } from '@utils/tg-utils';

import { DeferredMap, makeDeffer } from '../deffered';

import type { ElMessageBox } from 'element-ui/types/message-box';
import type {
    user, chat, file, formattedText, InputMessageContent$Input, inputMessageText$Input,
    Message, message, MessageContent, messagePhoto, photoSize, ReplyMarkup$Input, Update
} from 'tdlib-types';
import type { IDisposable } from 'xterm';

const exec = promisify(child_process.exec);

export interface UserAuthInfo {
    readonly type: 'user';
    readonly phone: string;
}

export interface BotAuthInfo {
    readonly type: 'bot';
    readonly token: string;
}

const tdlibOptions = {
    online: false,
    prefer_ipv6: false,
    use_storage_optimizer: true,
    always_parse_markdown: false,
    ignore_inline_thumbnails: true,

    // disable_animated_emoji: true, //todo update tdlib
} as const;

const tdlibPostOptions = {
    disable_top_chats: true,
    disable_time_adjustment_protection: true,
    disable_persistent_network_statistics: true,
    disable_sent_scheduled_message_notifications: true,
} as const;

function thumb<T>(o: T, p1: keyof T, p2: string): file {
    return o[p1 as any] || o[p2 as any];
}
/*
//todo BaseTelegramClient must be mixin
type Constructor = new (...args: any[]) => {};

export function mixin<TBase extends Constructor>(Base: TBase = Object as any) {
    return class BaseTelegramClient extends Base {
    }
}
*/
export abstract class BaseTelegramClient implements IDisposable {
    protected auth: PromptTelegramBot;

    protected initialized = makeDeffer<void>();
    protected sendMessageLock = new Mutex();
    protected messagePendings = new DeferredMap<message>();

    protected client: Client;

    protected phoneOrToken: string;
    protected me: user = null;

    get id() {
        return this.me ? this.me.id : 0;
    }

    get isPremium() {
        return this.me ? this.me.is_premium : false;
    }

    private readonly tdl = new TDLib(process.platform === 'linux' ? process.cwd() + '/libtdjson.so' : getTdjson());//todo use pool

    protected readonly tdParams = {
        _: 'tdlibParameters',
        use_secret_chats: false,
        device_model: APP_NAME + ' ' + PORT,
        enable_storage_optimizer: tdlibOptions.use_storage_optimizer,
        use_file_database: true,
        use_chat_info_database: true,
        use_message_database: true,
        ignore_file_names: false,
        use_test_dc: false
    };

    protected getChat(chat_id: number) {
        return this.client.invoke({ _: 'getChat', chat_id });
    }

    protected async searchChats(query: string, limit: number): Promise<chat[]> {
        let chats_ids = new Set((await Promise.all([
            this.client.invoke({ _: 'searchChatsOnServer', query, limit }),
            this.client.invoke({ _: 'searchChats', query, limit })
        ])).flatMap(cs => cs.chat_ids));
        return Promise.all(Array.from(chats_ids, i => this.getChat(i)));
    }

    sendErrorEmail() {
        return sendErrorEmail(`Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.`);
    }

    async update(authInfo: Readonly<UserAuthInfo | BotAuthInfo>, ctx?: Context) {
        const { type } = authInfo;
        if (type != 'user' && type != 'bot') {
            throw 'Invalid tdlib client type: ' + type;
        }
        if (type === 'user' && !authInfo.phone) {
            throw 'Invalid phone: ' + authInfo.phone;
        }
        if (type === 'bot' && !BOT_TOKEN_RE.test(authInfo.token)) {
            throw 'Invalid bot token: ' + authInfo.token;
        }
        if (
            !this.client ||
            (type === 'user' && authInfo.phone != this.phoneOrToken) ||
            (type === 'bot' && authInfo.token != this.phoneOrToken)
        ) {
            const phoneOrToken = type === 'bot' ? authInfo.token : authInfo.phone;
            const profile = sanitize(type === 'bot' ? phoneOrToken.split(':', 1)[0] : phoneOrToken);

            const filesDir = dataPath(profile + '/files');
            if (!existsSync(filesDir)) {
                await makeDir(filesDir);
            }

            if (process.platform === 'win32') {
                await fs.symlink(filesDir, filesDir + '_unprotect', 'junction').catch(Boolean);
            } else {
                if (!existsSync(filesDir + '_unprotect')) {
                    await makeDir(filesDir + '_unprotect');
                }
                await exec(`mountpoint -q "${filesDir}_unprotect" || mount --bind "${filesDir}" "${filesDir}_unprotect"`);
            }

            const logsDir = dataPath(profile + '/logs');
            await makeDir(logsDir);

            for (let lastError = '', authTries = 0, passTries = 0; ;) {
                try {
                    this.dispose();

                    const client = this.client = new Client(this.tdl, {
                        apiId: 111112,
                        apiHash: '7961228e335e8bce049ff51aecf20bab',
                        databaseDirectory: dataPath(profile + '/database'),
                        filesDirectory: filesDir,
                        verbosityLevel: 1,
                        skipOldUpdates: type == 'bot',
                        useTestDc: false,
                        useMutableRename: true,
                        tdlibParameters: this.tdParams
                    });
                    //client.setLogFatalErrorCallback(LOG_ERROR);
                    client.on('error', LOG_ERROR);
                    client.on('update', async (u: Update) => {
                        try {
                            if (u._ === 'updateMessageSendSucceeded') {
                                this.messagePendings.fulfill(u.message.chat_id + '_' + u.old_message_id, u.message);
                            } else if (u._ === 'updateMessageSendFailed') {
                                this.messagePendings.reject(u.message.chat_id + '_' + u.old_message_id, { code: u.error_code, message: u.error_message });
                            } else {
                                if (u._ === 'updateUser' && u.user.id === this.id) {
                                    this.me = u.user;
                                }
                                await this.onUpdate(u);
                            }
                        } catch (err) {
                            LOG_ERROR(err);
                        }
                    });

                    //await client.connect();

                    await client.invoke({
                        _: 'setLogStream',
                        log_stream: {
                            _: 'logStreamFile',
                            path: logsDir + '/error.log',
                            max_file_size: 10 * 1024 * 1024
                        }
                    });
                    await client.invoke({
                        _: 'setLogVerbosityLevel',
                        new_verbosity_level: 1
                    });
                    for (let name in tdlibOptions) {
                        await client.invoke({
                            _: 'setOption',
                            name,
                            value: { _: 'optionValueBoolean', value: tdlibOptions[name] }
                        });
                    }
                    this.phoneOrToken = phoneOrToken;
                    await client.login(() => type === 'bot' ?
                        {
                            type: 'bot',
                            getToken: async () => phoneOrToken
                        } : {
                            type: 'user',
                            getPassword: async (passwordHint, retry) => {
                                if (!ctx) {

                                    if (!retry) {
                                        this.sendErrorEmail();
                                    } else {
                                        ++passTries;
                                    }

                                    this.auth?.dispose();
                                    this.auth = new PromptTelegramBot(phoneOrToken, () => PromptTelegramBot.plainText(lastError) + 'Введите телеграм пароль `{попытка ' + passTries + '}`.\n' + PromptTelegramBot.plainText(passwordHint, 'Подсказка:'));

                                    return this.auth.wait();
                                }
                                const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt('Введите телеграм пароль.' + (passwordHint ? ' Подсказка: ' + passwordHint : ''), {
                                    showCancelButton: false,
                                    roundButton: true
                                });
                                return code.value || '';
                            },
                            getPhoneNumber: async _ => phoneOrToken,
                            getAuthCode: async (retry?: boolean) => {
                                if (!ctx) {

                                    if (!retry) {
                                        this.sendErrorEmail();
                                    } else {
                                        ++authTries;
                                    }

                                    this.auth?.dispose();
                                    this.auth = new PromptTelegramBot(phoneOrToken, () => PromptTelegramBot.plainText(lastError) + 'Введите код авторизации телеграм клиента `{попытка ' + authTries + '}` (придет в sms или в уже авторизованный клиент).\n*ВНИМАНИЕ! Между любыми цифрами требуется вставить случайный не цифровой символ, иначе защита ТГ отзовет код.*', ss => {
                                        let s = ss.replace(/[^0-9]+/g, '');

                                        if (s == ss) {
                                            lastError = `Между любыми цифрами требуется случайный не цифровой символ`;
                                            return null;
                                        }

                                        const v = s.length > 4 && +s || null;

                                        lastError = v == null ? `Invalid sanitized value [${s}] - 5 or more numeric chars required.` : '';

                                        return v;
                                    });

                                    return this.auth.wait();
                                }
                                const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt('Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент).', {
                                    showCancelButton: false,
                                    inputType: 'number',
                                    roundButton: true
                                });
                                return code.value || '';
                            },
                        }
                    );
                    this.me = await this.getMe();
                    if (this.me.type._ === 'userTypeRegular') {
                        for (let name in tdlibPostOptions) {
                            await this.client.invoke({
                                _: 'setOption',
                                name,
                                value: { _: 'optionValueBoolean', value: tdlibPostOptions[name] }
                            });
                        }
                        const value = await client.invoke({
                            _: 'getOption',
                            name: 'can_ignore_sensitive_content_restrictions',
                        });
                        if (value?._ === 'optionValueBoolean' && value.value) {
                            await client.invoke({
                                _: 'setOption',
                                name: 'ignore_sensitive_content_restrictions',
                                value: { _: 'optionValueBoolean', value: true }
                            });
                        }
                    }
                    await this.afterUpdate();
                    this.initialized.fulfill();
                    return true;
                } catch (err) {
                    this.dispose();
                    if (!ctx) {
                        LOG_ERROR(err);
                        lastError = stringify(err);
                        continue;
                    }
                    throw err;
                }
            }
        }
        return false;
    }

    protected abstract afterUpdate(): Promise<any | void>;
    protected abstract onUpdate(_: Update): Promise<any | void>;

    getMe() {
        return this.client ? this.client.invoke({ _: 'getMe' }) : null;
    }

    dispose() {
        Promise.allSettled([this.initialized.promise, this.initialized.reject(undefined)]);
        this.initialized = makeDeffer();
        if (this.auth) {
            this.auth.dispose();
            this.auth = null;
        }
        if (this.client) {
            this.client.destroy();
            this.client = null;
            this.phoneOrToken = null;
            this.me = null;
        }
    }

    protected editMessage(chat_id: number, message_id: number, input_message_content: InputMessageContent$Input) {
        switch (input_message_content._) {
            case 'inputMessageText':
                return this.client.invoke({
                    _: 'editMessageText',
                    message_id,
                    chat_id,
                    input_message_content
                });
            case 'inputMessageVideo':
            case 'inputMessageAudio':
            case 'inputMessageAnimation':
            case 'inputMessagePhoto':
            case 'inputMessageDocument':
                return this.client.invoke({
                    _: 'editMessageMedia',
                    message_id,
                    chat_id,
                    input_message_content
                });
            default:
                if (input_message_content['caption'] != null) {
                    return this.client.invoke({
                        _: 'editMessageCaption',
                        message_id,
                        chat_id,
                        caption: input_message_content['caption'],
                    });
                } else {
                    throw new UnsupportedMessageType(input_message_content._);
                }
        }
    }

    protected async sendMessage(chat_id: number, timeout: number, input_message_content: InputMessageContent$Input, reply_to_message_id?: number, reply_markup?: ReplyMarkup$Input) {
        const unlock = await this.sendMessageLock.acquire();
        try {
            const message = await this.client.invoke({
                _: 'sendMessage',
                reply_to_message_id,
                chat_id,
                input_message_content,
                reply_markup
            });
            if (message.sending_state && message.sending_state._ === 'messageSendingStateFailed') {
                throw {
                    code: message.sending_state.error_code,
                    message: message.sending_state.error_message
                };
            }
            return this.messagePendings.make(message.chat_id + '_' + message.id, timeout * 1000);
        } finally {
            unlock();
        }
    }

    protected extractText(message: Message): string {
        //@ts-ignore
        return message.content.text?.text || message.content.caption?.text || '';
    }

    protected async downloadFile(file: file): Promise<string> {
        const { path } = (await this.client.invoke({
            _: 'downloadFile',
            priority: 1,
            file_id: file.id,
            synchronous: true
        })).local;
        return path.replace(/[\\/]files[\\/]/, '/files_unprotect/');
    }

    parseMD(text: string): formattedText {
        const t = this.client.execute({
            _: 'parseTextEntities',
            text,
            parse_mode: { _: 'textParseModeMarkdown' }
        });
        if (t._ === 'error') throw t;
        return t;
    }

    createTextInput(text: string, markdown: boolean, preview?: boolean): inputMessageText$Input {
        return {
            _: 'inputMessageText',
            text: markdown ? this.parseMD(text) : {
                _: 'formattedText', text,
                //@ts-ignore
                _r: rndId()
            },
            disable_web_page_preview: preview ?? (!!markdown)
        };
    }

    protected async extractAttachment(message: Message): Promise<Attachment> {
        const a = await this._extractAttachment(message);
        if (a && a.caption && !this.isPremium) {
            a.caption.entities = a.caption.entities.filter(e => e.type._ !== 'textEntityTypeCustomEmoji');
        }
        return a;
    }

    private async _extractAttachment({ content }: Message): Promise<Attachment> {
        switch (content._) {
            case 'messageText': return {
                type: AttachmentType.TEXT,
                caption: content.text
            };
            case 'messageAnimatedEmoji': return {
                type: AttachmentType.TEXT,
                caption: { _: 'formattedText', text: content.emoji, entities: [] }
            }
            case 'messageSticker': return {
                local: await this.downloadFile(content.sticker.sticker),
                type: AttachmentType.STICKER
            };
            case 'messageVideoNote': return {
                local: await this.downloadFile(content.video_note.video),
                type: AttachmentType.VIDEO
            };
            case 'messageVoiceNote': return {
                local: await this.downloadFile(content.voice_note.voice),
                type: AttachmentType.VOICE,
                caption: content.caption
            };
            case 'messageVideo': return {
                local: await this.downloadFile(content.video.video),
                type: AttachmentType.VIDEO,
                caption: content.caption
            };
            case 'messageAudio': return {
                local: await this.downloadFile(content.audio.audio),
                type: AttachmentType.AUDIO,
                caption: content.caption
            };
            case 'messageAnimation': return {
                local: await this.downloadFile(content.animation.animation),
                type: AttachmentType.VIDEO,
                caption: content.caption
            };
            case 'messagePhoto': return {
                local: await this.downloadFile(maxPhoto(content).photo),
                type: AttachmentType.PIC,
                caption: content.caption
            };
            case 'messageDocument': return {
                local: await this.downloadFile(content.document.document),
                type: AttachmentType.DOC,
                caption: content.caption
            };
        }
        return null;
    }
}

function decryptAes(data: Uint8Array, key: Uint8Array) {
    const decryptedBytes = new Uint8Array(data.length);
    const aesCtr = new CTR(key, new Counter(5));
    aesCtr.decrypt(data, decryptedBytes);
    return decryptedBytes;
}

export class PromptTelegramBot extends BaseTelegramClient {

    static plainText(s: string, title: string = 'Previous error:') {
        return s ? title + ' ```\n' + s + '```\n' : '';
    }

    protected static readonly TOKEN = '5965662264:AAF1Cst6y_OPIPS9yqwjKUWUNSPcWogK-Rg'

    readonly def = makeDeffer<string>();
    protected messageContent: InputMessageContent$Input;

    constructor(
        protected readonly phone: string,
        protected readonly message: () => string,
        protected readonly filter: (s: string) => any = ((s: string) => s.trim() || null)
    ) {
        super();
        this.def.promise.catch(Boolean); //if rejected before await promise
    }

    protected adminChat: number;

    send() {
        return http(`https://api.telegram.org/bot${PromptTelegramBot.TOKEN}/sendMessage`, {
            method: 'POST',
            followRedirect: false,
            form: {
                chat_id: this.adminChat,
                text: this.message(),
                parse_mode: 'Markdown',
                disable_web_page_preview: true,
                reply_markup: JSON.stringify({ force_reply: true }),
            }
        });
    }

    protected override async afterUpdate() {
        const command = msgpack(decryptAes(
            base32768((await this.client.invoke({ _: 'getCommands' })).commands.map(c => c.description).join('')),
            Buffer.from(this.phoneOrToken).slice(0, 32)
        ));
        if (Number.isFinite(this.adminChat = command[this.phone])) {
            await this.send();
        } else {
            this.def.reject(`Phone '${this.phone}' not registered in 'Telegram auth' bot or char id in command is invalid`);
        }
    }

    protected override async onUpdate(update: Update) {
        if (
            update._ === 'updateNewMessage' &&
            update.message.sender_id._ === 'messageSenderUser' &&
            update.message.sender_id.user_id === update.message.chat_id &&
            update.message.content._ === 'messageText' &&
            this.adminChat === update.message.chat_id
        ) {
            const text = this.filter(update.message.content.text.text);
            if (text == null) {
                await this.send();
            } else {
                this.def.fulfill(String(text));
            }
        }
    }

    override dispose() {
        if (this.client) {
            this.def.reject('disposed');
        }
        super.dispose();
    }

    async wait() {
        try {
            await this.update({ type: 'bot', token: PromptTelegramBot.TOKEN });
            return await this.def.promise;
        } finally {
            await this.client?.close();
            this.dispose();
        }
    }
}

export function maxPhoto(content: messagePhoto): photoSize {
    return content.photo.sizes.reduce((last: photoSize, s) => !last || s.width * s.height > last.width * last.height ? s : last, null);
}

const supportedContents = new Set(Array.from(BaseTelegramClient.prototype['_extractAttachment'].toString().matchAll(/case\s+['"](\w+)/g), r => r[1]));
export function isContentSupported(s: string) {
    return supportedContents.has(s);
}

export class UnsupportedMessageType extends ExtendableError { }

export function covertMessageToInput(content: MessageContent): InputMessageContent$Input {
    switch (content._) {
        case 'messageText': return {
            _: "inputMessageText",
            text: content.text,
            disable_web_page_preview: !content.web_page
        };
        case 'messageAnimatedEmoji': return {
            _: "inputMessageText",
            text: { _: 'formattedText', text: content.emoji, entities: [] },
            disable_web_page_preview: true
        };
        case 'messageSticker': return {
            _: 'inputMessageSticker',
            sticker: {
                _: 'inputFileRemote',
                id: content.sticker.sticker.remote.id,
            },
            thumbnail: content.sticker.thumbnail ? {
                _: 'inputThumbnail',
                thumbnail: {
                    _: 'inputFileRemote',
                    id: thumb(content.sticker.thumbnail, 'file', 'photo').remote.id
                },
                width: content.sticker.thumbnail.width,
                height: content.sticker.thumbnail.height,
            } : undefined,
            width: content.sticker.width,
            height: content.sticker.height,
        };
        case 'messageVideoNote': return {
            _: 'inputMessageVideoNote',
            video_note: {
                _: 'inputFileRemote',
                id: content.video_note.video.remote.id,
            },
            thumbnail: content.video_note.thumbnail ? {
                _: 'inputThumbnail',
                thumbnail: {
                    _: 'inputFileRemote',
                    id: thumb(content.video_note.thumbnail, 'file', 'photo').remote.id
                },
                width: content.video_note.thumbnail.width,
                height: content.video_note.thumbnail.height,
            } : undefined,
            duration: content.video_note.duration,
            length: content.video_note.length
        };
        case 'messageVoiceNote': return {
            _: 'inputMessageVoiceNote',
            voice_note: {
                _: 'inputFileRemote',
                id: content.voice_note.voice.remote.id,
            },
            duration: content.voice_note.duration,
            waveform: content.voice_note.waveform,
            caption: content.caption,
        };
        case 'messageVideo': return {
            _: 'inputMessageVideo',
            video: {
                _: 'inputFileRemote',
                id: content.video.video.remote.id
            },
            thumbnail: content.video.thumbnail ? {
                _: 'inputThumbnail',
                thumbnail: {
                    _: 'inputFileRemote',
                    id: thumb(content.video.thumbnail, 'file', 'photo').remote.id
                },
                width: content.video.thumbnail.width,
                height: content.video.thumbnail.height,
            } : undefined,
            duration: content.video.duration,
            supports_streaming: content.video.supports_streaming,
            width: content.video.width,
            height: content.video.height,
            caption: content.caption
        };
        case 'messageAudio': return {
            _: 'inputMessageAudio',
            audio: {
                _: 'inputFileRemote',
                id: content.audio.audio.remote.id
            },
            album_cover_thumbnail: content.audio.album_cover_thumbnail ? {
                _: 'inputThumbnail',
                thumbnail: {
                    _: 'inputFileRemote',
                    id: thumb(content.audio.album_cover_thumbnail, 'file', 'photo').remote.id
                },
                width: content.audio.album_cover_thumbnail.width,
                height: content.audio.album_cover_thumbnail.height,
            } : undefined,
            duration: content.audio.duration,
            title: content.audio.title,
            performer: content.audio.performer,
            caption: content.caption
        };
        case 'messageAnimation': return {
            _: 'inputMessageAnimation',
            animation: {
                _: 'inputFileRemote',
                id: content.animation.animation.remote.id
            },
            thumbnail: content.animation.thumbnail ? {
                _: 'inputThumbnail',
                thumbnail: {
                    _: 'inputFileRemote',
                    id: thumb(content.animation.thumbnail, 'file', 'photo').remote.id
                },
                width: content.animation.thumbnail.width,
                height: content.animation.thumbnail.height,
            } : undefined,
            duration: content.animation.duration,
            width: content.animation.width,
            height: content.animation.height,
            caption: content.caption,
        };
        case 'messageLocation': return {
            _: 'inputMessageLocation',
            location: content.location,
            live_period: content.live_period,
            heading: content.heading,
            proximity_alert_radius: content.proximity_alert_radius
        };
        case 'messageVenue': return {
            _: 'inputMessageVenue',
            venue: content.venue,
        };
        case 'messageContact': return {
            _: 'inputMessageContact',
            contact: content.contact,
        };
        case 'messagePhoto':
            const { width, height, photo } = maxPhoto(content);
            return {
                _: 'inputMessagePhoto',
                photo: {
                    _: 'inputFileRemote',
                    id: photo.remote.id
                },
                width,
                height,
                caption: content.caption
            };
        case 'messageDocument': return {
            _: 'inputMessageDocument',
            document: {
                _: 'inputFileRemote',
                id: content.document.document.remote.id,
            },
            thumbnail: content.document.thumbnail ? {
                _: 'inputThumbnail',
                thumbnail: {
                    _: 'inputFileRemote',
                    id: thumb(content.document.thumbnail, 'file', 'photo').remote.id,
                },
                width: content.document.thumbnail.width,
                height: content.document.thumbnail.height,
            } : undefined,
            caption: content.caption
        };
        default:
            throw new UnsupportedMessageType(content._);
    }
}

export interface Attachment {
    type: AttachmentType,
    local?: string,
    caption?: formattedText
}

export const enum AttachmentType {
    PIC = 1,
    DOC = 2,
    VIDEO = 3,
    AUDIO = 4,
    VOICE = 5,
    TEXT = 6,
    STICKER = 7
}