import { AsyncProvider, ParametricProvider } from '@crud/types';

export function createAsync<T, Args extends any[]>(initCb: (...args: Args) => Promise<T>): AsyncProvider<T, Args> {
    let instance: T = null;
    return {
        async init(...args: Args) {
            return instance || (instance = await initCb(...args));
        },
        get() {
            return instance;
        }
    }
}

export function create<T, Args extends any[]>(initCb: (...args: Args) => T): ParametricProvider<T, Args> {
    let instance: T;
    return {
        init(...args: Args) {
            return instance || (instance = initCb(...args));
        },
        get() {
            return instance;
        }
    }
}