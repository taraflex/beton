'use strict';

const { IgnorePlugin } = require('webpack');
const { basename, resolve } = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { DefinePlugin, ContextReplacementPlugin } = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const { VueLoaderPlugin } = require('vue-loader');
const { sentenceCase } = require('sentence-case');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');
const VirtualModulePlugin = require('virtual-module-webpack-plugin');
const { paramCase } = require('param-case');
const { argv } = require('yargs');
const hogan = require('hogan.js');
const fs = require('fs');
const { pascalCase } = require('pascal-case');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const { DuplicatesPlugin } = require('inspectpack/plugin');

const DEV_SERVER = require.main && basename(require.main.filename) === 'webpack-dev-server.js';
const DEV = !!(DEV_SERVER || argv['watch'] || process.env.NODE_ENV?.startsWith('dev') || argv['mode']?.startsWith('dev'));

//@ts-ignore
const packageJson = require('../../package.json');
const APP_TITLE = sentenceCase(packageJson.name);
const devDependencies = Object.keys(packageJson.devDependencies);

const cssLoader = {
    loader: 'css-loader',
    options: {
        sourceMap: DEV
    }
}
const sassLoader = {
    loader: 'sass-loader',
    options: {
        sourceMap: DEV
    }
}
const postcssLoader = DEV ? undefined : {
    loader: 'postcss-loader',
    options: {
        plugins: [
            require('autoprefixer')
        ],
        sourceMap: DEV
    }
}

const extensions = ['.js', '.ts', '.jsx', '.tsx', '.vue', '.pug'];

const u = (...a) => a.filter(Boolean);

function readDir(...parts) {
    try {
        return fs.readdirSync(resolve(...parts));
    } catch (_) { }
    return [];
}

function getEntitiesFilenames(dir, noScanCore, ext = '.ts') {
    const files = (noScanCore ? [] : readDir(__dirname, dir))
        .concat(readDir(__dirname, '../' + dir))
        .filter(n => n.endsWith(ext))
        .map(n => basename(n, ext))
        .filter(n => pascalCase(n) === n);
    return Array.from(new Set(files));
}

const nameTemplate = (ext = 'js') => `[name].${ext}?h=[contenthash:7]`;

module.exports = function config(BROWSER) {
    const BROWSER_PROD = !DEV && BROWSER;
    const styleLoader = DEV ? 'style-loader' : MiniCssExtractPlugin.loader;

    const vueComponents = BROWSER ? getEntitiesFilenames('frontend', true, '.vue') : [];

    return {
        target: BROWSER ? 'web' : 'node',
        devtool: DEV ? (BROWSER ? 'eval' : 'cheap-module-eval-source-map') : undefined,
        mode: DEV ? 'development' : 'production',
        externals: BROWSER ? [] : [nodeExternals({ modulesFromFile: true, allowlist: devDependencies })],
        output: {
            globalObject: BROWSER ? 'self' : undefined,
            devtoolModuleFilenameTemplate: '[absolute-resource-path]',
            path: resolve(__dirname, '../../', BROWSER ? 'build/static' : 'build'),
            publicPath: BROWSER ? '/static/' : undefined,
            filename: nameTemplate(),
            chunkFilename: nameTemplate(),
        },
        resolve: {
            extensions,
            alias: {
                'vue$': 'vue/dist/vue.runtime.esm.js',
                'typescript-ioc$': 'typescript-ioc/es6'
            },
            plugins: [new TsconfigPathsPlugin({ extensions })],
        },
        node: BROWSER ? {
            process: DEV,
            setImmediate: false,
        } : {
            __dirname: false
        },
        performance: { hints: false },
        optimization: {
            minimize: !DEV,
            usedExports: !DEV,
            minimizer: [
                new TerserPlugin({
                    parallel: 4,
                    //cache: true,
                    extractComments: false,
                    terserOptions: {
                        ecma: 2017,
                        compress: {
                            keep_classnames: true,
                            keep_fnames: true, //чтобы не сломать Function.name полифил
                            collapse_vars: false
                        },
                        output: {
                            beautify: !BROWSER,
                            comments: false
                        },
                        mangle: BROWSER_PROD ? {
                            reserved: [
                                ...vueComponents,
                                'x' //чтобы не сломать Function.name полифил
                            ]
                        } : false
                    }
                }),
                new OptimizeCssAssetsPlugin({
                    assetNameRegExp: /\.css(\?h=\w+)?$/,
                    cssProcessorPluginOptions: {
                        preset: ['default', { discardComments: { removeAll: true }, zindex: false }],
                    }
                })
            ],
            splitChunks: {
                chunks: 'async',
                minChunks: 2,
                maxInitialRequests: 1000,
                maxAsyncRequests: 4
            }
        },
        plugins: u(
            DEV ? new DuplicatesPlugin() : 0,
            DEV ? new CircularDependencyPlugin({
                exclude: /node_modules/,
                allowAsyncCycles: true
            }) : 0,
            BROWSER || fs.existsSync(resolve(__dirname, '../App.ts')) ? 0 : new VirtualModulePlugin({
                moduleName: 'src/App.ts',
                contents: `
import { Singleton } from 'typescript-ioc';
@Singleton
export class App { 
    constructor() {} 
    async init() {}
}`}),
            BROWSER ? new VirtualModulePlugin({
                moduleName: 'src/frontend/generated.ts',
                contents: hogan.compile(`
{{#files}}
//@ts-ignore
import {{{.}}} from '@frontend/{{{.}}}.vue';
{{/files}}
export default [{{#files}}{{{.}}},{{/files}}];
`).render({ files: vueComponents }),
            }) : 0,
            BROWSER ? 0 : new VirtualModulePlugin({
                moduleName: 'src/entities/generated.ts',
                contents: hogan.compile(`
import { getMetadataArgsStorage } from 'typeorm';
import { SingletonRepository } from '@ioc';
export const entities = []; 
export const repositories = [];
const storage = getMetadataArgsStorage();
{{#files}}
const { {{{.}}}, {{{.}}}Repository } = require('@entities/{{{.}}}.ts');
if({{{.}}} && storage.filterTables({{{.}}}).length > 0) {
    entities.push({{{.}}});
    if({{{.}}}Repository) {
        repositories.push(SingletonRepository({{{.}}})({{{.}}}Repository)); 
    }
}
{{/files}}
`).render({ files: getEntitiesFilenames('entities') }),
            }),
            BROWSER ? 0 : new VirtualModulePlugin({
                moduleName: 'src/subscribers/generated.ts',
                contents: hogan.compile(`
{{#files}}
import { {{{.}}} } from '@subscribers/{{{.}}}';
{{/files}}
export default [{{#files}}{{{.}}},{{/files}}];
`).render({ files: getEntitiesFilenames('subscribers') }),
            }),
            new ContextReplacementPlugin(
                /moment[/\\]locale$/,
                /ru|en-gb/
            ),
            new DefinePlugin({
                BROWSER: JSON.stringify(BROWSER),
                DEBUG: JSON.stringify(DEV),
                APP_NAME: "'" + paramCase(APP_TITLE) + "'",
                APP_TITLE: "'" + APP_TITLE + "'",
                PROJECT_SETTINGS_EXIST: fs.existsSync(resolve(__dirname, '../entities/Settings.ts')),
                'process.env.NODE_ENV': JSON.stringify(DEV ? 'development' : 'production'),
                ...(BROWSER ? { 'process.env.DEBUG': JSON.stringify(DEV) } : {})
            }),
            BROWSER ? new IgnorePlugin({
                contextRegExp: /monaco-editor[\/\\]esm[\/\\]vs[\/\\]basic-languages[\/\\]/,
                resourceRegExp: /^\.\/(?!javascript|typescript)[\w-]+\.js$/
            }) : 0,
            BROWSER ? new IgnorePlugin({
                contextRegExp: /monaco-editor[\/\\]esm[\/\\]vs[\/\\]language[\/\\]/,
                resourceRegExp: /^\.\/(cssMode|htmlMode|jsonMode)\.js$/,
            }) : 0,
            BROWSER ? new VueLoaderPlugin() : 0,
            BROWSER_PROD ? new MiniCssExtractPlugin({ filename: nameTemplate('css') }) : 0,
            BROWSER && fs.existsSync(resolve(__dirname, '../../favicon.ico')) ? new CopyWebpackPlugin([
                {
                    from: './favicon.ico',
                    to: './'
                }
            ]) : 0,
            BROWSER_PROD ? new CompressionPlugin({
                filename: '[path].gz[query]',
                test: /\.(eot|svg|ttf|woff|js|css)(\?h=\w+)?$/,
                exclude: /common\.js$/,
                minRatio: 0.9
            }) : 0
        ),
        module: {
            exprContextCritical: false,
            rules: [
                BROWSER && {
                    test: /\.scss$/,
                    use: u(styleLoader, cssLoader, postcssLoader, sassLoader)
                },
                BROWSER && {
                    test: /\.css$/,
                    use: u(styleLoader, cssLoader, postcssLoader)
                }, {
                    test: /\.tsx?$/,
                    use: [{
                        loader: 'ts-loader',
                        options: {
                            compilerOptions: {
                                sourceMap: DEV,
                                target: BROWSER_PROD ? 'ES6' : 'ES2019',
                                noUnusedLocals: !DEV,
                                noUnusedParameters: !DEV
                            },
                            appendTsSuffixTo: [/\.vue$/],
                        }
                    }, {
                        loader: 'preprocessor-loader',
                        options: {
                            macros: {
                                LOG_ERROR: "(console.error.bind(console, '(__FILE__:__LINE__)'))",
                                LOG_WARN: "(console.warn.bind(console, '(__FILE__:__LINE__)'))",
                                LOG_INFO: "(console.log.bind(console, '(__FILE__:__LINE__)'))"
                            }
                        }
                    }],
                    exclude: BROWSER ? [/node_modules/] : [/frontend|node_modules/]
                },
                BROWSER && {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            ts: 'ts-loader',
                            js: 'ts-loader'
                        },
                        esModule: true
                    }
                },
                BROWSER && {
                    test: /\.(png|jpg|gif|webp)$/,
                    loader: 'url-loader',
                    options: {
                        limit: 4096,
                        name: '[name].[md5:hash:base58:7].[ext]',
                        outputPath: 'assets/'
                    }
                },
                BROWSER && {
                    test: /\.(eot|svg|ttf|woff|woff2)$/,
                    loader: 'url-loader',
                    options: {
                        limit: 8192,
                        name: '[md5:hash:base58:7].[ext]',
                        outputPath: 'assets/'
                    }
                }, {
                    test: /\.pug$/,
                    loader: 'pug-loader',
                    options: {
                        pretty: DEV
                    }
                }, {
                    test: /\.mustache(\.conf)?$/,
                    loader: 'mustache-loader',
                    options: {
                        tiny: !DEV
                    }
                }].filter(Boolean),
        },
        resolveLoader: {
            modules: ['node_modules', resolve(__dirname, '../loaders'), resolve(__dirname, 'loaders')]
        },
        stats: {
            hash: false,
            modules: false,
            version: false,
            children: false,
            assets: false,
            colors: true,
            depth: false,
            performance: false
        }
    }
}
module.exports.u = u;
module.exports.DEV = DEV;
module.exports.DEV_SERVER = DEV_SERVER; 