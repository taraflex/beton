export class AsyncQueue<T = any> implements AsyncIterator<T, void> {
    [Symbol.asyncIterator]() {
        return this;
    }
    protected objects: { priority: number, value: T }[] = [];
    protected readonly resolvers: ((o: { value: T, done: false }) => void)[] = [];
    get size() {
        return this.objects.length;
    }
    add(value: T, priority: number = 0, nosort?: boolean) {
        if (this.resolvers.length > 0) {
            this.resolvers.shift()({ done: false, value });
        } else {
            let i = 0;
            if (nosort || (i = this.objects.findIndex(v => v.priority > priority)) < 0) {
                this.objects.push({ value, priority });
            } else {
                this.objects.splice(i, 0, { value, priority });
            }
        }
    }
    filter(f: (value: T, priority: number) => boolean) {
        this.objects = this.objects.filter(v => f(v.value, v.priority));
    }
    delete(f: (value: T, priority: number) => boolean) {
        for (let i = this.objects.length - 1; i >= 0; --i) {
            const { value, priority } = this.objects[i];
            if (f(value, priority)) {
                this.objects.splice(i, 1);
            }
        }
    }
    next(): Promise<{ value: T, done: false }> {
        return new Promise(resolve => {
            if (this.objects.length > 0) {
                resolve({ done: false, value: this.objects.shift().value });
            } else {
                this.resolvers.push(resolve);
            }
        });
    }
}