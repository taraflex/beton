//based on https://github.com/marcuswestin/store.js

interface IStorage {
    read(key: string): string;
    write(key: string, data: any): void;
    has(key: string): boolean;
    clear(): void;
    each(): IterableIterator<{
        value: string;
        key: string;
    }>;
}

class CookieStorage implements IStorage {

    read(key: string) {
        if (!key || !this.has(key)) { return null; }
        const regexpStr = "(?:^|.*;\\s*)" +
            escape(key).replace(/[\-\.\+\*]/g, "\\$&") +
            "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"
        return unescape(document.cookie.replace(new RegExp(regexpStr), "$1"));
    }

    *each() {
        const cookies = document.cookie.split(/; ?/g)
        for (let i = cookies.length - 1; i >= 0; i--) {
            if (!cookies[i] || !cookies[i].trim()) {
                continue;
            }
            const kvp = cookies[i].split('=');
            const key = unescape(kvp[0]);
            const value = unescape(kvp[1]);
            yield { value, key };
        }
    }

    write(key: string, data) {
        if (!key) { return; }
        document.cookie = escape(key) + "=" + escape(data) + `; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/`;
    }

    remove(key: string) {
        if (!key || !this.has(key)) {
            return;
        }
        document.cookie = escape(key) + `=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/`;
    }

    clear() {
        for (let { key } of this.each()) {
            this.remove(key);
        }
    }

    has(key: string) {
        return (new RegExp("(?:^|;\\s*)" + escape(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
    }
}

class LocalStorage implements IStorage {

    static support() {
        try {
            localStorage['__storage_test__'] = '1';
            const r = '1' === localStorage['__storage_test__'];
            localStorage.removeItem('__storage_test__');
            return r;
        } catch{ }
        return false;
    }

    read(key: string) {
        return localStorage.getItem(key);
    }

    write(key: string, data) {
        return localStorage.setItem(key, data);
    }

    *each() {
        for (let i = localStorage.length - 1; i >= 0; i--) {
            const key = localStorage.key(i);
            yield { value: this.read(key), key };
        }
    }

    remove(key: string) {
        return localStorage.removeItem(key);
    }

    clear() {
        return localStorage.clear();
    }

    has(key: string) {
        return localStorage.getItem(key) !== undefined;
    }
}

export const storage: IStorage = LocalStorage.support() ? new LocalStorage() : new CookieStorage();