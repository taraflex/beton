import { SetOption } from 'cookies';
import { Context } from 'koa';

export const DEFAULT_COOKIE_OPTIONS = Object.freeze({
    maxAge: 60 * 3 * 1000,
    secure: false,
    httpOnly: true,
    sameSite: 'lax',
    signed: false,
    overwrite: true
}) as SetOption;

const DELETE_COOKIE_OPTIONS: SetOption = Object.freeze({
    ...DEFAULT_COOKIE_OPTIONS,
    maxAge: undefined,
    expires: new Date(-1),
    signed: false
});

export function setCookie(name: string, ctx: Context, data: any) {
    ctx.cookies.set(name, encodeURIComponent(JSON.stringify(data)), DEFAULT_COOKIE_OPTIONS);
}

export function deleteCookie(name: string, ctx: Context, signed?: boolean) {
    ctx.cookies.set(name, '', DELETE_COOKIE_OPTIONS);
    if (signed) {
        ctx.cookies.set(name + '.sig', '', DELETE_COOKIE_OPTIONS);//удалим также подпись
    }
}

export function getCookie(name: string, ctx: Context) {
    try {
        const v = ctx.cookies.get(name);
        return v ? JSON.parse(decodeURIComponent(v)) : null;
    } catch (err) {
        LOG_ERROR(name, err);
        deleteCookie(name, ctx);
    }
    return null;
}