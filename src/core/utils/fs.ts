import { createWriteStream, promises as fs } from 'fs';
import { pipeline } from 'stream/promises';

import type { Readable } from 'stream';

export async function cp(stream: Readable, target: string) {
    try {
        await pipeline(stream, createWriteStream(target, { autoClose: true }));
    } catch (e) {
        await fs.unlink(target).catch(Boolean);
        throw e;
    }
    return target;
}