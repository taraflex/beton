export class Chain<T = any> {
    protected readResolve: (value?: T | PromiseLike<T>) => void;
    write(o: T) {
        if (this.readResolve) {
            this.readResolve(o);
            this.readResolve = null;
        }
    }
    read() {
        return new Promise<T>(resolve => this.readResolve = resolve);
    }
}
