import { existsSync } from 'fs';
import { resolve } from 'path';
import { isIP } from 'net';

const regulus = __non_webpack_require__(__dirname + '/../package.json').regulus || {};

export const REAL_HOSTNAME: string = regulus.hostname || 'localhost';
export const HOSTNAME: string = process.argv.includes('--local') && !isIP(REAL_HOSTNAME) && REAL_HOSTNAME != 'localhost' ? 'dev.' + REAL_HOSTNAME : REAL_HOSTNAME;
export const PEM = REAL_HOSTNAME !== HOSTNAME && existsSync(__dirname + '/../REGULUS.pem') && resolve(__dirname + '/../REGULUS.pem') || '';
export const PORT: number = REAL_HOSTNAME !== HOSTNAME ? (regulus.devport || 7813) : (regulus.port || 7812);

const routes = Object.assign({
    admin: '/adm',
    api: '/api',
    health: '/health'
}, regulus.routes);

export const ADMIN: string = routes.admin;
export const API: string = routes.api;
export const HEALTH: string = routes.health;
export const STATIC: string = '/static';