import { Context } from 'koa';

export function setNoStoreHeader(ctx: Context) {
    ctx.set('Cache-Control', 'no-store, no-cache, max-age=0');
}

export default function (ctx: Context, next: () => Promise<any>) {
    setNoStoreHeader(ctx);
    return next();
}