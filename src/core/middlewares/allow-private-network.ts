import { Context } from 'koa';

export default function (ctx: Context, next: () => Promise<any>) {
    if (ctx.headers.origin) {
        ctx.set('Access-Control-Allow-Credentials', 'true');
        ctx.set('Access-Control-Allow-Private-Network', 'true');
        ctx.set('Access-Control-Allow-Origin', ctx.headers.origin);
    }
    return next();
}