import { Mutex } from 'async-mutex';
import Emittery from 'emittery';
import { promises as fs } from 'fs';
import makeDir from 'make-dir';
import pMap from 'p-map';
import {
    chatTypeSupergroup, ConnectionState, formattedText$Input, inlineKeyboardButtonTypeCallback,
    inputMessageAudio$Input, InputMessageContent$Input, inputMessageDocument$Input,
    inputMessagePhoto$Input, inputMessageSticker$Input, inputMessageText$Input,
    inputMessageVideo$Input, inputMessageVoiceNote$Input, Message, messageText, proxy,
    replyMarkupInlineKeyboard, Update, updateDeleteMessages
} from 'tdlib-types';
import { tmpName } from 'tmp-promise';
import { Inject, Singleton } from 'typescript-ioc';

import { BetonAccountRepository } from '@entities/BetonAccount';
import { Channel, ChannelRepository } from '@entities/Channel';
import { ClientSettings } from '@entities/ClientSettings';
import { Forecast, ForecastRepository } from '@entities/Forecast';
import { stringify, trim } from '@taraflex/string-tools';
import { AsyncUniqQueue } from '@utils/AsyncUniqQueue';
import dataPath from '@utils/data-path';
import isGoodMessage from '@utils/filter-text-message';
import { cp } from '@utils/fs';
import http from '@utils/http';
import rndId from '@utils/rnd-id';
import { sendErrorEmail } from '@utils/send-email';
import sqlEscape from '@utils/sql-escape';
import { toChatId } from '@utils/tg-utils';

import {
    AttachmentType, BaseTelegramClient, covertMessageToInput, isContentSupported,
    UnsupportedMessageType
} from './core/BaseTelegramClient';

const TMP_DIR = makeDir.sync(dataPath('uploads'));

const VALID_SUB_RE = /^[\w-\.]+\.[a-zA-Z]+\.[UTL]+$/;

function isRegisterMessage(s: string) {
    return s && /^register[\s_]*channel$/i.test(s);
}

function makeSub(subscription: string, id: number) {
    return id ? subscription + '│' + id : subscription;
}

type PProxy = (proxy & { seconds?: number, err?: boolean }) | { id?: number, seconds?: number, err?: boolean }

@Singleton
export class TgClient extends BaseTelegramClient {

    protected readonly fqueue = new AsyncUniqQueue<true>();
    public readonly rotate = this.fqueue.add.bind(this.fqueue, true);

    protected readonly forecastLock = new Mutex();
    protected readonly requestedChats = new Set<number>();

    readonly emmiter = new Emittery<{ sub: string; reload: undefined }>();
    public state: ConnectionState;

    constructor(
        @Inject protected readonly channelRepository: ChannelRepository,
        @Inject protected readonly forecastRepository: ForecastRepository,
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly betonAccountsRepository: BetonAccountRepository
    ) {
        super();
        this.dispose();
        this.thread();
    }

    async getState(): Promise<{ state: ConnectionState, proxies: PProxy[] }> {
        const { proxies } = await this.client.invoke({ _: 'getProxies' });
        proxies.unshift({ id: 0 } as any);
        return {
            proxies: await Promise.all(proxies.map(this.pingProxy)),
            state: this.state
        };
    }

    async enableProxy(newProxyId: number) {
        try {
            if (newProxyId) {
                await this.client.invoke({ _: 'enableProxy', proxy_id: newProxyId });
            } else if (newProxyId === 0) {
                await this.client.invoke({ _: 'disableProxy' });
            }
            return await this.getState();
        } finally {
            this.rotate();
        }
    }

    async reloadProxies() {
        const pp: { host: string, port: string, secret: string }[] = await http('https://raw.githubusercontent.com/hookzof/socks5_list/master/tg/mtproto.json', { responseType: 'json', resolveBodyOnly: true });

        const { proxies } = await this.client.invoke({ _: 'getProxies' });
        await Promise.allSettled(proxies.map(current => {
            if (
                !pp.some(p => p.host == current.server && +p.port == current.port)
            ) {
                return this.client.invoke({ _: 'removeProxy', proxy_id: current.id })
            }
        }));

        await Promise.allSettled(pp.map(p => {
            if (
                !proxies.some(current => p.host == current.server && +p.port == current.port)
            ) {
                return this.client.invoke({
                    _: 'addProxy',
                    server: p.host,
                    port: +p.port,
                    type: {
                        _: 'proxyTypeMtproto',
                        secret: p.secret,
                    }
                })
            }
        }));

        return this.getState();
    }

    private readonly pingProxy = async (p: Partial<proxy>): Promise<PProxy> => {
        try {
            const ping = await Promise.race([
                new Promise<never>((_, reject) => setTimeout(reject, 4000, 1).unref()),
                this.client.invoke({ _: 'pingProxy', proxy_id: p.id })
            ]);
            return Object.assign(ping, p);
        } catch {
            return Object.assign({ err: true }, p);
        }
    }

    override dispose() {
        this.state = { _: 'connectionStateWaitingForNetwork' };
        super.dispose();
        this.requestedChats.clear();


    }

    protected async syncChats() {
        for (const c of this.requestedChats) {
            if (await this.getChat(c).catch(_ => false)) {
                this.requestedChats.delete(c);
            }
        }
        while (this.requestedChats.size) {
            try {
                await this.client.invoke({ _: 'loadChats', limit: 2147483647 });
            } catch (e) {
                if (e?.code === 404) break;
                LOG_ERROR(e);
            }
        }
    }

    protected override async afterUpdate() {
        await this.client.invoke({ _: 'createPrivateChat', user_id: this.id });
        for (let { supergroupId } of await this.channelRepository.find({ select: ['supergroupId'] })) {
            this.requestedChats.add(toChatId(supergroupId));
        }
        await this.syncChats();
        this.rotate();
    }

    async searchChatTitles(query: string, limit: number): Promise<string[]> {
        await this.initialized.promise;
        const chats = await this.searchChats(query, limit);
        return chats.map(c => c.title);
    }

    protected async getChannelComment(supergroup_id: number, chat_id: number) {
        try {
            const { invite_link } = await this.client.invoke({ _: 'getSupergroupFullInfo', supergroup_id });
            let link = invite_link ? 'tg://join?invite=' + invite_link.invite_link.split('/', 4)[3] + '|' : '';

            let { members } = await this.client.invoke({
                _: 'searchChatMembers',
                chat_id,
                query: '',
                limit: 1,
                filter: { _: 'chatMembersFilterMembers' }
            });
            if (members?.[0].member_id._ === 'messageSenderUser') {
                const info = await this.client.invoke({ _: 'getUser', user_id: members[0].member_id.user_id });
                link += [(info.last_name || ''), (info.first_name || ''), (info.username || '')].join(' ').replace(/\|/g, '').trim();
            }
            return link;
        } catch (err) {
            LOG_ERROR(supergroup_id, chat_id, err);
        }
        return '';
    }

    protected createContent(path: string, type: AttachmentType, caption: formattedText$Input): inputMessageText$Input | inputMessageSticker$Input | inputMessagePhoto$Input | inputMessageVideo$Input | inputMessageAudio$Input | inputMessageDocument$Input | inputMessageVoiceNote$Input {
        switch (type) {
            case AttachmentType.TEXT: return {
                _: 'inputMessageText',
                disable_web_page_preview: false,
                text: caption
            };
            case AttachmentType.STICKER: return {
                _: 'inputMessageSticker',
                sticker: { _: 'inputFileLocal', path }
            };
            case AttachmentType.PIC: return {
                _: 'inputMessagePhoto',
                photo: { _: 'inputFileLocal', path },
                caption
            };
            case AttachmentType.VIDEO: return {
                _: 'inputMessageVideo',
                video: { _: 'inputFileLocal', path },
                caption
            };
            case AttachmentType.AUDIO: return {
                _: 'inputMessageAudio',
                audio: { _: 'inputFileLocal', path },
                caption
            };
            case AttachmentType.DOC: return {
                _: 'inputMessageDocument',
                document: { _: 'inputFileLocal', path },
                caption
            };
            case AttachmentType.VOICE: return {
                _: 'inputMessageVoiceNote',
                voice_note: { _: 'inputFileLocal', path },
                caption
            }
        }
    };

    async uploadAndUnlink(path: string | URL, type: AttachmentType, caption?: formattedText$Input): Promise<InputMessageContent$Input> {
        try {
            if (path instanceof URL) {
                const f = decodeURIComponent(path.hash.slice(1));
                path = await cp(http.stream(path), f ? TMP_DIR + '/' + f : await tmpName({ dir: TMP_DIR }));
            }
            await this.initialized.promise;
            const { content } = await this.sendMessage(this.id, this.settings.tgMaxImageUploadTime, this.createContent(path, type, caption));
            return covertMessageToInput(content);
        } finally {
            if (!(path instanceof URL)) {
                await fs.unlink(path).catch(LOG_ERROR);
            }
        }
    }

    protected override async onUpdate(update: Update) {
        if (update._ === 'updateNewChat') {
            this.requestedChats.delete(update.chat.id);
        } else if (update._ === 'updateNewMessage') {
            await this.processMessage(update.message);
        } else if (update._ === 'updateMessageEdited' /*|| update._ === 'updateMessageContent'*/) {
            const { message_id, chat_id } = update;
            const { messages } = await this.client.invoke({ _: 'getMessages', chat_id, message_ids: [message_id] });
            if (messages[0]) {
                await this.processMessage(messages[0]);
            }
        } else if (update._ === 'updateConnectionState') {
            this.state = update.state;
        } else if (update._ === 'updateDeleteMessages' && (update.is_permanent || !update.from_cache)) {
            await this.deleteMessages(update);
        }
    }

    protected readonly forRemove = new Set<string>();

    protected async deleteMessages({ chat_id, message_ids }: updateDeleteMessages) {
        const { title } = await this.getChat(chat_id);
        if (await this.hasChannelsWithSubscription(title)) {
            for (const i of message_ids) {
                this.forRemove.add(makeSub(title, i));
            }
            this.rotate();
        }
    }

    protected async processMessage(message: Message) {
        if (message.is_outgoing) {
            if (
                message.can_be_forwarded &&
                message.is_channel_post &&
                isRegisterMessage(this.extractText(message))
            ) {
                try {
                    const { type, title } = await this.getChat(message.chat_id);
                    const { supergroup_id } = type as chatTypeSupergroup;
                    if (supergroup_id) {
                        const c = await this.channelRepository.findOne({ supergroupId: supergroup_id }) || new Channel();
                        c.title = title;
                        c.supergroupId = supergroup_id;
                        c.subscriptions = c.subscriptions || [];
                        c.comment = await this.getChannelComment(c.supergroupId, message.chat_id) || c.comment;
                        await this.channelRepository.save(c, { reload: false, listeners: false });
                        await this.client.invoke({
                            _: 'deleteMessages',
                            chat_id: message.chat_id,
                            message_ids: [message.id],
                            revoke: true
                        });
                    }
                } catch (err) {
                    LOG_ERROR(message, err);
                }
            } else if (message.chat_id === this.id && message.content._ === 'messageDice') {
                await this.emmiter.emit('reload');
                await this.client.invoke({
                    _: 'deleteMessages',
                    chat_id: message.chat_id,
                    message_ids: [message.id]
                });
            }
            return;
        }

        if (message.chat_id == 382431027 /*@bet-hub bot*/) {
            const s = trim(this.extractText(message).split('\n', 1)[0]);
            if (VALID_SUB_RE.test(s)) {
                this.emmiter.emit('sub', s);
            }
            return;
        }

        //расшифровка текста 
        if (
            message.via_bot_user_id === 211684723 &&
            (message.reply_markup as replyMarkupInlineKeyboard)?.rows[0]?.[0]?.text === 'Read'
        ) {
            const { data } = (message.reply_markup as replyMarkupInlineKeyboard).rows[0][0].type as inlineKeyboardButtonTypeCallback;
            const res = await this.client.invoke({
                _: 'getCallbackQueryAnswer',
                chat_id: message.chat_id,
                message_id: message.id,
                payload: { _: 'callbackQueryPayloadData', data }
            });
            (message.content as messageText).text = {
                _: 'formattedText',
                text: res.text,
                entities: []
            }
        }

        if (
            isContentSupported(message.content._) &&
            isGoodMessage(this.extractText(message), this.settings)
        ) {
            const { title } = await this.getChat(message.chat_id);
            if (await this.hasChannelsWithSubscription(title)) {
                const a = await this.extractAttachment(message);
                if (a && (a.local || a.type === AttachmentType.TEXT)) {
                    await this.createForecast(
                        title,
                        message.id,
                        a.type === AttachmentType.TEXT ?
                            {
                                _: "inputMessageText",
                                text: {
                                    ...a.caption,
                                    //@ts-ignore
                                    _r: rndId()
                                },
                                disable_web_page_preview: !(message.content as messageText).web_page
                            } :
                            await this.uploadAndUnlink(a.local, a.type, a.caption),
                        message.date,
                        message.reply_to_message_id
                    );
                }
            }
        }
    }

    protected findForecast(subscription: string) {
        return this.forecastRepository.findOne({ subscription }, { select: ['publishedTo', 'id'] });
    }

    protected async deleteForecast(subscription: string) {
        const f = await this.findForecast(subscription);
        if (f) {
            for (let c in f.publishedTo) {
                await this.initialized.promise;
                await this.client.invoke({
                    _: 'deleteMessages',
                    chat_id: toChatId(c),
                    message_ids: [Math.abs(f.publishedTo[c])],
                    revoke: true
                }).catch(LOG_ERROR);
            }
            await this.forecastRepository.delete(f);
        }
        this.forRemove.delete(subscription);
    }

    async createForecast(title: string, id: number, content: InputMessageContent$Input, date: number, replyTo: number) {
        const unlock = await this.forecastLock.acquire();
        try {
            const s = makeSub(title, id);
            if (this.forRemove.has(s)) {
                return this.deleteForecast(s);
            }

            const f = (await this.findForecast(s)) || new Forecast();
            for (let c in f.publishedTo) {
                f.publishedTo[c] = -Math.abs(f.publishedTo[c]);
            }
            f.published = false;
            f.added = new Date(date * 1000);
            f.subscription = s;
            f.info = content;
            if (replyTo) {
                f.replyTo = makeSub(title, replyTo);
            }
            await this.saveForecast(f);
            return f;
        } finally {
            unlock();
        }
    }

    saveForecast(forecast: Forecast) {
        return this.forecastRepository.save(forecast, { reload: false }).catch(Boolean).finally(this.rotate);//игнорируем когда вставляем дубликат
    }

    protected async projectReplyTo(cache: Record<string, Forecast>, { replyTo }: Forecast, supergroupId: number) {
        return (cache[replyTo] ||= await this.forecastRepository.findOne({ subscription: replyTo }, { select: ['publishedTo'] }).catch(LOG_ERROR) as Forecast)?.publishedTo[supergroupId] || 0;
    }

    protected async thread() {
        const { fqueue, forecastRepository, channelRepository, settings, forRemove, rotate } = this;
        let ftm: NodeJS.Timer = null;

        for await (let _ of fqueue) {

            clearTimeout(ftm);

            const unlock = await this.forecastLock.acquire();
            try {
                await this.initialized.promise;

                while (forRemove.size) {
                    for (const sub of forRemove) {
                        await this.deleteForecast(sub);
                    }
                }

                let minPublishAt = Number.POSITIVE_INFINITY;
                let somePublished = false;

                const forecasts = await forecastRepository.find({ where: { published: false }, order: { added: 'ASC' } });
                const cache = Object.fromEntries(forecasts.map(f => ([f.subscription, f])));

                for (const f of forecasts) {

                    let modified = false;

                    const { publishedChannels } = f;
                    const channels = await channelRepository
                        .createQueryBuilder()
                        .select()
                        .where(`instr(subscriptions, '${sqlEscape(f.subscriptionTitle)}')${publishedChannels.length ? ` and supergroupId not in(${publishedChannels})` : ''}`)
                        .getMany();

                    const publishAt =
                        channels.length &&
                        Math.min.apply(null, (await pMap(channels, async channel => {
                            const { supergroupId } = channel;
                            const pubAt = f.added.getTime() + channel.timeout;
                            try {
                                if (Date.now() >= pubAt) {
                                    const chatId = toChatId(supergroupId);

                                    const message = f.publishedTo[supergroupId] < 0 ?
                                        await this.editMessage(chatId, -f.publishedTo[supergroupId], f.info) :
                                        await this.sendMessage(
                                            chatId,
                                            this.settings.tgMaxImageUploadTime,
                                            f.info,
                                            f.replyTo ? await this.projectReplyTo(cache, f, supergroupId) : 0
                                        );
                                    f.publishedTo[supergroupId] = message.id;
                                    modified = true;
                                } else {
                                    return pubAt;
                                }
                            } catch (err) {
                                LOG_ERROR(f, err, channel.title);
                                if (err instanceof UnsupportedMessageType) {
                                    f.published = modified = true;
                                } else if (err) {
                                    if (err._ == 'error' && (err.message === 'Message not found')) {
                                        f.publishedTo[supergroupId] = undefined;
                                        modified = true;
                                        return pubAt;
                                    } else if (err._ == 'error' && (
                                        err.code == 400 ||
                                        err.message === 'Chat not found' ||
                                        err.message === 'Chat info not found' ||
                                        err.message === "Can't access the chat"
                                    )) {
                                        sendErrorEmail(`К каналу ${channel.title} ${channel.comment.split('|', 1)[0]} нет доступа.\nОшибка: ${stringify(err)}`);
                                    }
                                }
                            }
                            return Number.POSITIVE_INFINITY;
                        }, { concurrency: 6 })));//tdlib не умеет отправлять несколько сообщений параллельно

                    if (Number.isFinite(publishAt)) {
                        minPublishAt = Math.min(minPublishAt, publishAt);
                    } else {
                        f.published = modified = true;
                    }
                    if (modified) {
                        somePublished = true;
                        await forecastRepository.save(f, { reload: false });
                    }
                }
                if (Number.isFinite(minPublishAt)) {
                    const now = Date.now();
                    if (now >= minPublishAt) {
                        rotate();
                    } else {
                        ftm = setTimeout(rotate, minPublishAt - now).unref();
                    }
                } else if (somePublished && settings.removeAfter > 0 && !fqueue.size) {
                    await forecastRepository
                        .createQueryBuilder()
                        .delete()
                        .where(`added < date('now','-${settings.removeAfter} day')`)
                        .execute();
                }
            } catch (err) {
                LOG_ERROR(err);
                rotate();
            } finally {
                unlock();
            }
        }
    }

    async hasChannelsWithSubscription(s: string): Promise<boolean> {
        return s ? !!(
            await this.channelRepository
                .createQueryBuilder()
                .select('supergroupId')
                .where(`instr(subscriptions, '${sqlEscape(s)}')`)
                .getRawOne()
        ) : false;
    }
}