import playwright, { Page } from 'playwright-firefox';
import { sanitize } from 'sanitize-filename-ts';
import { Inject, Singleton } from 'typescript-ioc';
import { MessageContext, VK } from 'vk-io';

import { ClientSettings } from '@entities/ClientSettings';
import { Settings, SettingsRepository } from '@entities/Settings';
import { fromCtx } from '@rpc/ServerRPC';
import dataPath from '@utils/data-path';
import { ExtendableError } from '@utils/extendable-error';
import isGoodMessage from '@utils/filter-text-message';
import { PromisesIterator } from '@utils/PromisesIterator';
import { officialAppCredentials } from '@vk-io/authorization';
import { loadCookies, saveCookies } from './cookie-parser';
import { AttachmentType, PromptTelegramBot } from './core/BaseTelegramClient';
import { TgClient } from './TgClient';

import type { ElMessageBox } from 'element-ui/types/message-box';
import type { Context } from 'koa';
import type { IDisposable } from 'xterm';
import type { MessagesSearchConversationsResponse, MessagesGetConversationsByIdResponse } from 'vk-io/lib/api/schemas/responses';

function findTypeOf(a: any[], ...props: string[]) {
    for (let p of props) {
        const v = a.find(e => e.type === p);
        if (v) {
            return v;
        }
    }
}

export function maxPhoto(images: readonly { width: number, height: number }[]) {
    return images.reduce((last: { width: number, height: number }, s) => !last || s.width * s.height > last.width * last.height ? s : last, null);
}

const enum Scopes {
    notify = 1,
    friends = 2,
    photos = 4,
    audio = 8,
    video = 16,
    pages = 128,
    link = 256,
    status = 1024,
    notes = 2048,
    messages = 4096,
    wall = 8192,
    ads = 32768,
    offline = 65536,
    docs = 131072,
    groups = 262144,
    notifications = 524288,
    stats = 1048576,
    email = 4194304,
    market = 134217728
}

class VKAuthError extends ExtendableError { }

const NAV_OPTIONS = { timeout: DEBUG ? 0 : 10000, waitUntil: 'domcontentloaded', state: 'visible' } as const;

async function processPage(page: Page, phone: string, password: string, twoFaHandler: (payload: string) => Promise<string>) {

    for (let [selector, cb] of [
        ['input[name="login"]', () => phone],
        ['input[name="password"]', () => password],
        ['input[name="otp"]', async () => twoFaHandler(await page.innerText('h3'))],
    ] as const) {
        const $el = await page.$(selector);
        if ($el) {
            for (let error = ''; ;) {
                await $el.fill(await (error ? twoFaHandler(PromptTelegramBot.plainText(error, await page.innerText('h3'))) : cb()));
                const [r] = await Promise.all([
                    Promise.race([
                        page.waitForNavigation(NAV_OPTIONS).then(_ => 'nav'),
                        page.waitForSelector('.vkc__TextField__errorMessage', NAV_OPTIONS)
                    ]),
                    page.click('button[type="submit"]')
                ]);
                if (typeof r === 'string') {
                    return;
                } else {
                    error = await r.innerText();
                }
            }
        }
    }
    throw new VKAuthError(await page.innerText('body'));
}

async function getAccess(phone: string, password: string, page: playwright.Page, twoFaHandler: (payload: string) => Promise<string>): Promise<string> {
    for (const params: Record<string, string> = {
        grant_type: 'password',
        client_id: officialAppCredentials.windowsPhone.clientId,
        client_secret: officialAppCredentials.windowsPhone.clientSecret,
        username: phone,
        password,
        scope: (Scopes.offline | Scopes.messages | Scopes.groups | Scopes.photos | Scopes.audio | Scopes.video | Scopes.docs).toString(),
        //test_redirect_uri: DEBUG ? '1' : '0',
        v: '5.131',
        '2fa_supported': '1'
    }; ;) {
        const resp = await page.goto('https://oauth.vk.com/token?' + new URLSearchParams(params).toString());
        delete params.code;
        let { access_token, error, redirect_uri, validation_type, phone_mask, error_description, error_type } = (await resp.json()) as Record<string, string>;

        if (error_type === 'wrong_otp' || error_type === 'otp_format_is_incorrect' || (error === 'need_validation' && validation_type)) {
            const code = await twoFaHandler(PromptTelegramBot.plainText(error_description) + 'Введите код двуфакторной авторизации из ' + (
                validation_type === '2fa_sms' ?
                    `sms (отправлено на номер \`${phone_mask}\`) или воспользуйтесь резервными кодами.` :
                    'сообщения от Администрации или из приложения для генерации кодов или воспользуйтесь резервными кодами.\nДля принудительной отправки кода по sms введите /sms (стало срабатывать не всегда).'

            ));
            if (code === '/sms') {
                params.force_sms = '1';
            } else {
                params.code = code;
            }
        } else if (error === 'need_validation' && redirect_uri) {
            await page.goto(redirect_uri, NAV_OPTIONS);
            while (!page.url().includes('success=1')) {
                await processPage(page, phone, password, twoFaHandler);
                if (page.url().includes('fail=1')) {
                    throw new VKAuthError(await page.innerText('body'));
                }
            }
            access_token = new URLSearchParams(new URL(page.url()).hash.slice(1)).get('access_token');
        } else if (error) {
            throw new VKAuthError(`[${error}]: ` + error_description);
            //todo twoFaHandler with capcha https://dev.vk.com/api/direct-auth
        }
        if (access_token) {
            return access_token;
        }
    }
}

const SUPPORTED_MESSAGES = new Set(['audio_message', 'doc', 'sticker', 'photo']);

@Singleton
export class VKClient implements IDisposable {
    protected client: VK;

    constructor(
        @Inject protected readonly tg: TgClient,
        @Inject protected readonly coreSettings: Settings,
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly coreSettingsRepository: SettingsRepository,
    ) { }

    getTitles({ items, profiles, groups }: MessagesSearchConversationsResponse | MessagesGetConversationsByIdResponse): string[] {
        return items.map(i => {
            switch (i.peer.type) {
                case 'chat':
                    return i.chat_settings.title.trim();
                case 'user':
                    const { first_name, last_name } = profiles.find(p => p.id === i.peer.id);
                    return (first_name + ' ' + last_name).trim();
                case 'group':
                    const { name } = groups.find(p => p.id === -i.peer.id);
                    return name.trim();
            }
            return '';
        }).filter(Boolean);
    }

    async searchChatTitles(q: string, count: number): Promise<string[]> {
        if (q && this.client) {
            const conv = await this.client.api.messages.searchConversations({ q, count, extended: true });
            return this.getTitles(conv);
        }
        return [];
    }

    protected processMessage = async (message: MessageContext, next: Function) => {
        try {
            let { text, id, isInbox } = message;
            if (isInbox && isGoodMessage(text, this.settings)) {

                const hasAttaches = message.attachments.some(a => SUPPORTED_MESSAGES.has(a.type));

                if (hasAttaches || text) {
                    const title = this.getTitles(await this.client.api.messages.getConversationsById({
                        peer_ids: message.peerId,
                        extended: true
                    }))[0];
                    if (await this.tg.hasChannelsWithSubscription(title)) {

                        await message.loadMessagePayload();

                        const urls: { type: AttachmentType, url: URL }[] = message.attachments.map(a => {
                            if (a.type === 'photo') return {
                                type: AttachmentType.PIC,
                                url: new URL(findTypeOf(a['sizes'], 'w', 'z', 'y', 'x', 'm', 's').url)
                            };
                            if (a.type === 'doc') {
                                const url = new URL(a['url']);
                                url.hash = sanitize(a['title']);
                                return { type: AttachmentType.DOC, url };
                            }
                            if (a.type === 'audio_message') return {
                                type: AttachmentType.VOICE,
                                url: new URL(a['url'])
                            };
                            if (a.type === 'sticker') return {
                                type: AttachmentType.PIC,
                                url: new URL(maxPhoto(a['payload'].images)['url'])
                            };
                        }).filter(Boolean);

                        const attachment = urls.shift();
                        if (!text && !attachment) {
                            return;
                        }

                        const tmessage = this.tg.createTextInput(text, false, message.attachments.some(a => a.type === 'link'));

                        const f = await this.tg.createForecast(
                            title,
                            id,
                            attachment ? await this.tg.uploadAndUnlink(attachment.url, attachment.type, tmessage.text) : tmessage,
                            message.createdAt,
                            message.replyMessage?.id
                        );

                        if (f && urls.length > 0) {
                            for await (let c of new PromisesIterator(urls.map(a => this.tg.uploadAndUnlink(a.url, a.type)))) {
                                f.info = c;
                                this.tg.saveForecast(f);
                            }
                        }
                    }
                }
            }
        } catch (err) {
            LOG_ERROR(message, err);
        }
        return next();
    }

    dispose() {
        this.client?.updates.stop();
        this.client = null;
    }

    async getToken(phone: string, password: string, ctx?: Context) {

        if (this.coreSettings.vk.token && phone === this.coreSettings.vk.phone && password === this.coreSettings.vk.password) {
            return this.coreSettings.vk.token;
        }

        const twoFaHandler = ctx ? async (payload: string): Promise<string> => {
            const { value } = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt(payload, {
                showCancelButton: false,
                roundButton: true
            }, 'Vk Auth');
            return value;
        } : async (payload: string) => new PromptTelegramBot(phone, () => '*Vk Auth*\n' + payload).wait();

        const browser = await playwright.firefox.launch({ headless: !DEBUG });
        const context = await browser.newContext({
            ...playwright.devices['Pixel 2'],
            isMobile: false,
            javaScriptEnabled: true,
            locale: 'ru-RU',
            ignoreHTTPSErrors: true,
            permissions: [],
        });

        const cookiePath = dataPath(sanitize(phone + '_cookies.txt'));

        try {
            await context.addCookies(await loadCookies(cookiePath));

            const allowed = new Set(['document', 'script', 'xhr', 'fetch']);

            await context.route('**', (route, request) => {
                const t = request.resourceType();
                if (allowed.has(t)) {
                    route.continue();
                } else {
                    route.fulfill({ status: 201 });
                }
            });

            const page = await context.newPage();

            await page.goto('https://m.vk.com/docs', NAV_OPTIONS);
            while (page.url().startsWith('https://id.vk.com/auth')) {
                await processPage(page, phone, password, twoFaHandler);
            }

            const token = await getAccess(phone, password, page, twoFaHandler);

            this.coreSettings.vk = { phone, password, token };
            await this.coreSettingsRepository.save(this.coreSettings);
            return token;
        } finally {
            await saveCookies(await context.cookies(), cookiePath);
            browser.close().catch(LOG_ERROR);
        }
    }

    async update(settings: ClientSettings, ctx?: Context) {
        if (
            !this.client ||
            !this.coreSettings.vk.token ||
            settings.vkPhone !== this.coreSettings.vk.phone ||
            settings.vkPassword !== this.coreSettings.vk.password
        ) {
            const token = await this.getToken(settings.vkPhone, settings.vkPassword, ctx);

            this.dispose();
            this.client = new VK({ token });

            this.client.updates.on('message', this.processMessage);
            await this.client.updates.startPolling();
        }
    }
}
