import { Column, Entity, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

@Access({ GET: 0, DELETE: 0, PATCH: 0 }, { display: 'table', icon: 'bullhorn', order: 10 })
@Entity()
export class Channel {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ state: RTTIItemState.READONLY })
    @Column({ default: '' })
    title: string;

    @v({ type: 'link' })
    @Column({ default: '' })
    comment: string;

    @v({ state: RTTIItemState.HIDDEN })
    @Column({ type: 'bigint', unique: true })
    supergroupId: number;

    @Column({ type: 'int', unsigned: true, default: 0 })
    timeout: number;

    @v({ remote: 'subscription.load', items: { type: 'string', empty: false }, state: RTTIItemState.ALLOW_CREATE })
    @Column({ default: '[]', type: 'simple-json' })
    subscriptions: string[];
}

export abstract class ChannelRepository extends Repository<Channel>{ }