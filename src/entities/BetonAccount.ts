import cheerio from 'cheerio';
import { createHash } from 'crypto';
import kfs from 'key-file-storage';
import { CurlHttpVersion, curly } from 'node-libcurl';
import { Column, Entity, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';
import decode from '@taraflex/decode-html';
import { stringify } from '@taraflex/string-tools';
import { toTextArray } from '@utils/beton-utils';
import dataPath from '@utils/data-path';
import { ExtendableError } from '@utils/extendable-error';
import http, { CookieJar } from '@utils/http';

import { ClientSettings } from './ClientSettings';

const tokens = kfs(dataPath('tokens'));

class AuthRequiredError extends ExtendableError { }
class LoginRequiredError extends ExtendableError { }
class PasswordRequiredError extends ExtendableError { }
class WarnPageError extends ExtendableError { }

async function isValidToken(token: string) {
    try {
        if (token) {
            return (await http('https://bet-hub.com/mobile_check_token/' + token).json())['status'] == 'valid';
        }
    } catch { }
    return false;
}

@Access({ _: 0 }, { icon: 'bold', order: 5 })
@Entity()
export class BetonAccount {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ empty: false })
    @Column({ default: '' })
    login: string;

    @v({ empty: false })
    @Column({ default: '' })
    password: string;

    @v({ state: RTTIItemState.READONLY })
    @Column({ default: '[]', type: 'simple-json' })
    subscriptions: string[];

    protected get hash() {
        if (!this.login)
            throw new LoginRequiredError();
        if (!this.password)
            throw new PasswordRequiredError();
        return this.login + '_' + createHash('md5').update(this.password).digest('hex');
    }

    async dropInvalidToken() {
        if (!await isValidToken(tokens[this.hash])) {
            delete tokens[this.hash];
        }
    }

    protected async getTokenPayload(): Promise<string> {
        const { hash } = this;
        let token = tokens[hash];
        if (token) {
            return token;
        }
        /*
                const response = await http('https://bet-hub.com/mobile_get_token/', {
                    method: 'PUT',
                     headers: {
                        'Accept': 'application/json',
                        'Authorization': 'Basic ' + Buffer.from(this.login + ':' + this.password).toString('base64'),
                        'Content-Type': 'application/json',
                        'User-Agent': 'Java/15',
                        'Host': 'bet-hub.com',
                        'Connection': 'keep-alive',
                        'Content-Length': '43'
                    },
                    body: '{"sKey":"nS96#F@o4y|Pxa7LOgr5eg2JP|#IH8%x"}'
                })*/
        const response = await curly('https://bet-hub.com/mobile_get_token/', {
            //proxy: 'http://' + PROXY_INFO,
            httpProxyTunnel: 1,
            httpVersion: CurlHttpVersion.V1_1,
            customRequest: 'PUT',
            timeout: 60,
            sslVerifyPeer: 0,
            httpHeader: [
                'Accept: application/json',
                'Authorization: Basic ' + Buffer.from(this.login + ':' + this.password).toString('base64'),
                'Content-Type: application/json',
                'User-Agent: Java/15',
                'Host: bet-hub.com',
                'Connection: keep-alive',
                'Content-Length: 43'
            ],
            postFields: '{"sKey":"nS96#F@o4y|Pxa7LOgr5eg2JP|#IH8%x"}'
        });
        try {
            token = JSON.parse(response.data)['token'];
        } catch { }
        if (!token) {
            throw new AuthRequiredError(stringify(response));
        }
        tokens[hash] = token;
        return token;
    }

    async loadDom(url: string, settings?: ClientSettings, cookieJar?: CookieJar) {
        url += (url.includes('?') ? '&' : '?') + 'token=' + await this.getTokenPayload() + '&width=1080';
        if (settings) {
            url += '&sr=' + encodeURIComponent((settings.deviceName + settings.deviceSerial).replace(/\s+/g, ''));
        }
        const { body, headers } = await http({ url, cookieJar, responseType: 'buffer' });
        const $ = cheerio.load(decode(body, headers['content-type']), { decodeEntities: false });
        const warn = $('.picks_warning').text().trim();
        if (warn && warn != 'Нажимая кнопку "Показать" Вы соглашаетесь с фактом использования прогноза. Если кнопка "Показать" неактивна, обновите страницу.') {
            throw new WarnPageError(warn);
        }
        return $;
    }

    async loadFavoritesLinks(settings?: ClientSettings, cookieJar?: CookieJar): Promise<{ url: string, title: string }[]> {
        const favs = await this.loadDom('https://bet-hub.com/user/subs_favorites/mobile.php', settings, cookieJar);
        return favs('.capper_L,.capper_UT,.capper_U').map(function (_, e) {
            return { url: `https://bet-hub.com/_seo/subs_mobile.php?q=${(e.parent as cheerio.TagElement).attribs.href.replace(/beton:\/\/betonapp_(statsall|pickfree)\//, '')}/picks/`, title: favs(e).text() };
        }).toArray() as any[];
    }

    async actualizeSubscriptions(settings?: ClientSettings) {
        await this.getTokenPayload();
        const newSubs = new Set((
            await Promise.all([
                'https://bet-hub.com/user/orders_active/mobile.php',
                'https://bet-hub.com/user/subs_favorites/mobile.php'
            ].map(url => this.loadDom(url, settings)))
        ).flatMap(doc => toTextArray(doc, doc('.capper_L,.capper_UT,.capper_U'))));
        if (
            newSubs.size > 0 &&
            this.subscriptions &&
            this.subscriptions.length === newSubs.size &&
            this.subscriptions.every(s => newSubs.has(s))
        ) {
            return false;
        }
        this.subscriptions = Array.from(newSubs);
        return true;
    }

    async beforeFilter() {
        try {
            await this.actualizeSubscriptions();
        } catch (err) {
            await this.dropInvalidToken();
            LOG_ERROR(err);
            throw [
                {
                    field: 'login',
                    message: 'Invalid bet-hub Login or Password'
                }, {
                    field: 'password',
                    message: 'Invalid bet-hub Login or Password'
                }
            ];
        }
    }
}

export abstract class BetonAccountRepository extends Repository<BetonAccount>{ }