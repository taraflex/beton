import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

const $1DAY = 60 * 60 * 24;

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'cog', order: 4 }, { PATCH: 'tgclient_save' })
@SingletonEntity()
export class ClientSettings {
    @PrimaryGeneratedColumn()
    id: number;

    // @v({ notEqual: 0, description: 'Telegram Api id взять [тут](https://my.telegram.org/apps)' })
    // @Column({ type: 'int', default: 0, unsigned: true })
    // apiId: number;

    // @v({ pattern: API_HASH_RE, description: 'Telegram Api hash взять [там же](https://my.telegram.org/apps)' })
    // @Column({ default: '', length: 32 })
    // apiHash: string;

    @v({ min: 5, description: 'Telegram телефон' })
    @Column({ default: '' })
    phone: string;

    @v({ min: 5, description: 'VK телефон' })
    @Column({ default: '' })
    vkPhone: string;

    @v({ min: 1, description: 'VK пароль' })
    @Column({ default: '' })
    vkPassword: string;

    @v({ pattern: /^.+@.+$/, description: 'Email (вида vasya@mail.ru)' })
    @Column({ default: '' })
    email: string;

    @v({ min: 1, description: 'Email пароль' })
    @Column({ default: '' })
    emailPassword: string;

    @v({ min: 1, description: 'IMAP хост (гуглить imap настройки почтового провайдера)' })
    @Column({ default: 'imap.yandex.ru' })
    imapHost: string;

    @v({ max: 65535, description: 'IMAP порт (гуглить imap настройки почтового провайдера)' })
    @Column({ type: 'int', default: 993, unsigned: true })
    imapPort: number;

    @v({ max: $1DAY, description: 'Пауза перед повторым подключением в секундах после ошибки IMAP подключения' })
    @Column({ type: 'int', default: 5, unsigned: true })
    imapReconnectPause: number;

    @v({ max: $1DAY, description: 'Максимальное время выполнения imap запроса в секундах (если не уложились пересоздание соединения)' })
    @Column({ type: 'int', default: 10, unsigned: true })
    imapActionMaxTime: number;

    @v({ min: 1, max: $1DAY, description: 'Пауза перед принудительным опросом IMAP в секундах после последнего удачного обновления (даже если не было уведомлений о новых сообщениях)' })
    @Column({ type: 'int', default: 20, unsigned: true })
    imapBeforePingPause: number;

    @v({ min: 1, description: `Имя android устройста (регистр имеет значение), привязанного к аккаунтам на bet-hub [колонка "Устройство"](https://bet-hub.com/user/mobile_app_devices/)` })
    @Column({ default: '' })
    deviceName: string;

    @v({ min: 1, description: `Id android устройста (регистр имеет значение), привязанного к аккаунтам на bet-hub [поле "Hardware serial"](https://play.google.com/store/apps/details?id=onemlab.deviceid)` })
    @Column({ default: '' })
    deviceSerial: string;

    @v({ description: `Регулярное выражение для фильтрации (белый список) входящих сообщений.` })
    @Column({ default: '.*' })
    whiteFilter: string;

    @v({ description: `Регулярное выражение для фильтрации (черный список) входящих сообщений.` })
    @Column({ default: 't\\.me\\/' })
    blackFilter: string;

    @v({ pattern: /^[0-9abcdef]{32}$/, description: '[captcha KEY](https://rucaptcha.com/setting) - [Не забудьте также включить уведомления о низком балансе](https://rucaptcha.com/profile/notif)' })
    @Column({ default: '' })
    ruCapchaKey: string;

    @v({ min: 30, max: $1DAY / 24, description: `Максимальное время на разгадывание капчи в секундах.` })
    @Column({ type: 'int', default: 3 * 60, unsigned: true })
    ruCapchaTimeout: number;

    @v({ max: $1DAY / 24, description: `Максимальное время на отправку вложения в телеграм сообщение в секундах (без учета времени генерации или скачивания со стороннего ресурса). ***0*** - не ограничено` })
    @Column({ type: 'int', default: 60, unsigned: true })
    tgMaxImageUploadTime: number;

    @v({ min: 1, max: $1DAY, description: `Минимальная пауза между запросами к bet-hub.com в секундах.` })
    @Column({ type: 'int', default: 60, unsigned: true })
    minimumUpdateInterval: number;

    @v({ max: $1DAY, description: `Максимальная пауза между запросами к bet-hub.com в секундах, после превышения которой на почту будет отправлено уведомление. Если ⩽ "минимальная пауза"  - не уведомлять.` })
    @Column({ type: 'int', default: $1DAY / 24, unsigned: true })
    alertInterval: number;

    @v({ min: 1, max: $1DAY, description: `Максимальная пауза между запросами к bet-hub.com в секундах.` })
    @Column({ type: 'int', default: $1DAY / 8, unsigned: true })
    maximumUpdateInterval: number;

    @v({ description: 'Удалить события из базы старше ***х*** дней (но не из телеграм канала). ***0*** - не удалять.' })
    @Column({ type: 'int', default: 5, unsigned: true })
    removeAfter: number;

    @v({ min: 1, max: 9, description: 'min_score google recapcha v3' })
    @Column({ type: 'int', default: 7, unsigned: true })
    capchaMinScore: number;
}

export abstract class ClientSettingsRepository extends Repository<ClientSettings>{ }