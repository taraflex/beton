import { InputMessageContent$Input } from 'tdlib-types';
import { Column, Entity, Index, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

@Access({ GET: 0, DELETE: 0 }, { icon: 'bell', order: 15 })
@Entity()
export class Forecast {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    added: Date;

    @Index()
    @Column()
    subscription: string;

    get subscriptionTitle() {
        return this.subscription.split('│', 1)[0];
    }

    @v({ state: RTTIItemState.HIDDEN, type: 'object' })
    @Column({ unique: true, default: '{}', type: 'text', transformer: { to: JSON.stringify, from: JSON.parse } })
    info: InputMessageContent$Input;

    @Index()
    @Column({ default: false })
    published: boolean;

    @v({ state: RTTIItemState.HIDDEN, type: 'object' })
    @Column({ default: '{}', type: 'text', transformer: { to: JSON.stringify, from: JSON.parse } })
    readonly publishedTo: Record<string, number>;

    get publishedChannels() {
        return Object.entries(this.publishedTo).filter(p => p[1] > 0).map(p => p[0]);
    }

    @v({ state: RTTIItemState.HIDDEN })
    @Column({ default: '' })
    replyTo: string;
}

export abstract class ForecastRepository extends Repository<Forecast>{ }