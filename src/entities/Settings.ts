import { Column, Repository } from 'typeorm';

import { SingletonEntity } from '@ioc';

import { Settings as CoreSettings } from '../core/entities/Settings';

@SingletonEntity()
export class Settings extends CoreSettings {
    @Column({ default: '{}', type: 'simple-json' })
    vk: { phone?: string, password?: string, token?: string };
}

export abstract class SettingsRepository extends Repository<Settings>{ }