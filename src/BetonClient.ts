import Backoff from 'backo2';
import lastMatch from 'last-match';
import { current } from 'node-zone';
import pFilter from 'p-filter';
import pMap from 'p-map';
import { Inject, Singleton } from 'typescript-ioc';
import { runInNewContext } from 'vm';

import { BetonAccount, BetonAccountRepository } from '@entities/BetonAccount';
import { ClientSettings } from '@entities/ClientSettings';
import { CancelationToken } from '@taraflex/cancelation-token';
import decodeHtml from '@taraflex/decode-html';
import { AsyncUniqQueue } from '@utils/AsyncUniqQueue';
import {
    AutoBidError, Ban10MinutesError, BannedError, CapchaError, ExpiredError, tableToForecast,
    toHtmlArray, unPack
} from '@utils/beton-utils';
import http, { CookieJar, USERAGENT } from '@utils/http';
import hex_md5 from '@utils/md5';
import { PromisesIterator } from '@utils/PromisesIterator';
import { sendErrorEmail } from '@utils/send-email';
import sqlEscape from '@utils/sql-escape';

import { BetonCapchaHandlers, RuCapchaError } from './BetonCapchaHandlers';
import { TgClient } from './TgClient';

const ReplayBack = Symbol('replay');

class ZoneString extends String {
    constructor(data: string, public readonly zone: string) {
        super(data);
    }
}

function merge(src: Record<string, any>, o: string) {
    delete src.is_reload;
    delete src.error;
    for (let k in src) {
        src[k] = new ZoneString(src[k], o);
    }
    return src;
}

async function loadRequest(cookieJar: CookieJar, path: string, form: Record<string, string>): Promise<string | ZoneString | Record<string, string | ZoneString>> {
    const { body, headers } = await http.post({
        url: `https://bet-hub.com${path}?JsHttpRequest=${Date.now()}-xml`,
        cookieJar,
        form,
        responseType: 'buffer'
    });
    const zone = current.name;
    const text = decodeHtml(body, headers["content-type"]);
    try {
        const { js } = JSON.parse(text);
        return zone && zone != '<root>' ? merge(js, zone) : js;
    } catch {
        return zone && zone != '<root>' ? new ZoneString(text, zone) : text;
    }
}

function wrapRegister(s: string) {
    return `register_promise(()=>{return ${s};})`;
}

@Singleton
export class BetonClient {
    private readonly chain = new AsyncUniqQueue<number>();
    private readonly backoff = new Backoff({ factor: 3 });

    constructor(
        @Inject private readonly capchaHandlers: BetonCapchaHandlers,
        @Inject private readonly accountsRepository: BetonAccountRepository,
        @Inject private readonly settings: ClientSettings,
        @Inject private readonly tgClient: TgClient
    ) {
        this.thread();
        tgClient.emmiter.on('sub', s => { this.processSub(s); });
    }

    protected ctoken = new CancelationToken();

    protected async pause(t?: number) {
        if (this.ctoken.canceled) {
            return false;
        }
        this.backoff.setMin(this.settings.minimumUpdateInterval);
        this.backoff.setMax(this.settings.maximumUpdateInterval);
        const d = t ?? this.backoff.duration();
        if (d > this.settings.alertInterval && this.settings.alertInterval > this.settings.minimumUpdateInterval) {
            sendErrorEmail(`Превышено время ожидания парсера ${d} сек.`);
        }
        return d > 0 ? await new Promise(resolve => {
            if (this.ctoken.canceled) {
                return resolve(false);
            }
            const connection = this.ctoken.connect(() => {
                clearTimeout(timeout);
                resolve(false);
            });
            const timeout = setTimeout(() => {
                connection.dispose();
                resolve(true);
            }, d * 1000).unref();
        }) : true;
    }

    cancelPause() {
        this.ctoken.cancel();
        this.capchaHandlers.rejectAll();
    }

    protected pauseReset() {
        this.backoff.setMin(this.settings.minimumUpdateInterval);
        this.backoff.setMax(this.settings.maximumUpdateInterval);
        this.backoff.reset();
    }

    private async unpack(account: BetonAccount, jar: CookieJar, pageurl: string, fromFavs: boolean): Promise<PromisesIterator<string | ZoneString>> {
        const { capchaHandlers, settings } = this;

        const $ = await account.loadDom(pageurl, settings, jar);

        const scripts = toHtmlArray($, $('script'));
        const codes = scripts.filter(s => s.startsWith('eval(function(p,a,c,k')).map(s => (s = unPack(s), s.startsWith('function ') ? s : wrapRegister(s)));

        if (codes.length < 1) {
            LOG_WARN(`Can't find packed script on page`, account.login, pageurl);
            return new PromisesIterator([]);
        }

        codes.push(...(fromFavs ?
            $('.header_active + .section .buy_button').toArray().map(e => $(e).attr('onclick')) :
            scripts.filter(s => s && (s.startsWith('spp(') || s.startsWith('spp2(')))
        ).filter(Boolean).map(wrapRegister));

        const requests: PromiseLike<string | ZoneString>[] = [];

        function register_promise(cb: () => PromiseLike<string>) {
            const p = cb();
            if (p) {
                requests.push(p);
            }
        }

        function query(path: string, form: Record<string, string>, success: any) {
            return loadRequest(jar, path, form).then(success);
        }

        Object.assign($.prototype, {
            ready(cb: Function) {
                return cb.call(this, $);
            },
            click(cb: Function) {
                return cb.call(this);
            }
        });

        $['ajax'] = ({ url, data, success }) => query(url, data, success);

        const code = codes.join(';\n');

        let breakNext = false;

        runInNewContext(code, {
            register_promise,
            window: Object.assign('html', { navigator: { userAgent: USERAGENT } }),
            hex_md5,
            pick_id_rcv3: {},
            document: {
                getElementById() {
                    return {}
                }
            },
            confirm() { return true },
            $,
            grecaptcha: {
                ready(cb: Function) {
                    return cb();
                },
                async render(_: string, options: { sitekey: string, callback: Function }) {
                    if (breakNext) throw ReplayBack;
                    breakNext = true;
                    const capchaId = await capchaHandlers.request(pageurl, options.sitekey);
                    return await current.fork(capchaId).run(options.callback, options, [await capchaHandlers.waitResult(capchaId)]);
                },
                async execute(sitekey: string, options: { action: string }) {
                    try {
                        DEBUG && LOG_INFO('before v3', account.login, options.action);
                        const capchaId = await capchaHandlers.request(pageurl, sitekey, options.action);
                        return await capchaHandlers.waitResult(capchaId);
                    } finally {
                        DEBUG && LOG_INFO('after v3', account.login);
                    }
                }
            },
            JsHttpRequest: { query }
        });
        return new PromisesIterator(requests);
    }

    private async thread() {
        this.pauseReset();
        const concurrency = 3;

        for await (let accountId of this.chain) {
            let hasForecast = false;
            let account: BetonAccount;
            let error: any = null;
            try {
                this.ctoken = new CancelationToken();

                account = await this.accountsRepository.findOne(accountId);
                if (!account || this.ctoken.canceled) {
                    continue;
                }

                const cookieJar = new CookieJar();

                //найдем избранные подписки, на которые хоть кто-нибудь подписан в тг
                const favs = await pFilter(
                    await account.loadFavoritesLinks(this.settings, cookieJar),
                    f => this.tgClient.hasChannelsWithSubscription(f.title),
                    { concurrency }
                );

                if (this.ctoken.canceled) {
                    continue;
                }

                //купим все прогнозы в нужном избранном
                await pMap(favs, async fav => {
                    try {
                        for await (let _ of await this.unpack(account, cookieJar, fav.url, true)) { }
                    } catch (e) {
                        LOG_ERROR(account, fav, e);
                    }
                }, { concurrency });

                if (this.ctoken.canceled) {
                    continue;
                }

                //купим все активные прогнозы + загрузим все открытые ранее
                for await (let content of await this.unpack(account, cookieJar, 'https://bet-hub.com/user/picks_active/mobile.php', false)) {
                    hasForecast = true;
                    const { zone } = content as ZoneString;
                    content = content.toString();

                    try {
                        this.tgClient.saveForecast(await tableToForecast(this.tgClient, content));
                        if (zone) {
                            this.capchaHandlers.reportGood(zone).catch(LOG_ERROR);
                        }
                    } catch (err) {
                        if (err instanceof CapchaError) {
                            LOG_ERROR(account.login, content, err);
                            if (zone) {
                                this.capchaHandlers.reportBad(zone).catch(LOG_ERROR);
                            }
                            if (!(error instanceof Ban10MinutesError)) {
                                error = err;
                            }
                            continue;
                        } else if (err instanceof BannedError) {
                            error = null;
                            LOG_ERROR(account.login, content, err);
                            sendErrorEmail(`Вероятно аккаунт ${account.login} забанен.`);
                            break;
                        } else if (
                            !(err instanceof AutoBidError) &&
                            !(err instanceof ExpiredError)
                        ) {
                            LOG_ERROR(account.login, content, err);
                            sendErrorEmail(`Ошибка парсинга прогноза в ${account.login}. Проверьте логи на сервере.`);
                            continue;
                        }
                    }

                    const fid = lastMatch(content, /javascript:up_archive_pick\('(\d+)'/);
                    if (fid) {
                        this.pauseReset();
                        try {
                            await http({
                                cookieJar,
                                url: 'https://bet-hub.com/user/picks_active/do_archive/?' + fid,
                                followRedirect: false
                            });
                        } catch (err) {
                            LOG_ERROR(account.login, content, err);
                        }
                    }
                }
            } catch (err) {
                if (err !== null) {
                    if (!(error instanceof Ban10MinutesError)) {
                        error = err;
                    }
                    if (err === ReplayBack) {
                        this.pauseReset();
                    } else {
                        LOG_ERROR(account.login, err);
                        if (err instanceof RuCapchaError) {
                            this.pauseReset();
                            if (err.capchaId) {
                                this.capchaHandlers.reportBad(err.capchaId).catch(LOG_ERROR);
                            }
                        } else {
                            await account.dropInvalidToken();
                        }
                    }
                }
            } finally {
                if (!hasForecast) {
                    this.pauseReset();
                }
                this.capchaHandlers.rejectAll();
                if (error instanceof Ban10MinutesError) {
                    sendErrorEmail(`Бан аккаунта ${account.login} на 10 минут. Бот приостанавливает парсинг бетона на 12 минут. Рекомендуется открытие вручную после разбана.`);
                }
                if (
                    error &&
                    await this.pause(error instanceof Ban10MinutesError ? (error.expire - Date.now()) / 1000 : null)
                ) {
                    this.processAccount(account.id);
                }
            }
        }
    }

    async processSubs(subs: string[]) {
        try {
            if (subs && subs.length > 0) {
                const builder = this.accountsRepository
                    .createQueryBuilder()
                    .select()
                    .where(`instr(subscriptions, '${sqlEscape(subs.shift())}')`);

                for (let s of subs) {
                    builder.orWhere(`instr(subscriptions, '${sqlEscape(s)}')`);
                }

                for (let a of await builder.getMany()) {
                    this.processAccount(a.id);
                }
            }
        } catch (err) {
            LOG_ERROR(subs, err);
        }
    }

    async processSub(s: string) {
        try {
            if (s) {
                const a = await this.accountsRepository
                    .createQueryBuilder()
                    .select()
                    .where(`instr(subscriptions, '${sqlEscape(s)}')`).getOne();
                if (a) {
                    this.processAccount(a.id);
                }
            }
        } catch (err) {
            LOG_ERROR(s, err);
        }
    }

    processAccount(id: number) {
        if (id) {
            this.chain.add(id);
        }
    }
}