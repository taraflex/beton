import { notify } from '@frontend/error-handler-mixin';
import { ClientRPC } from '@rpc/ClientRPC';
import { http } from '@utils/axios-smart';

export default {
    name: 'Force check',
    icon: 'sync',
    data() {
        return {
            proxy_loading: true,
            proxies: [],
            tg_loading: true,
            tg_color: 'info',
            tg_status: '...',
            loading: false,
            copyLoading: false,
            updateLoading: false
        };
    },
    computed: {
        active_proxy: {
            get() {
                return this.proxies.find(p => p.is_enabled)?.id | 0
            },
            set(v: number) {
                this.remote('enableProxy', v);
            }
        }
    },
    mounted() {
        this.remote('getState');
    },
    methods: {
        formatDate(p) {
            return p.last_used_date > 0 && !p.is_enabled ? 'last active: ' + new Date(p.last_used_date * 1000).toLocaleString() : '';
        },
        async remote(method: string, ...args: any[]) {
            try {
                this.proxy_loading = true;
                const { state, proxies } = await (await (this.$rpc as ClientRPC).waitConnect()).remote('TgClient')[method](...args);
                this.proxies = proxies;
                switch (state._) {
                    case 'connectionStateWaitingForNetwork':
                        this.tg_loading = true;
                        this.tg_color = 'danger';
                        this.tg_status = 'waiting for the network to become available'
                        break;

                    case 'connectionStateConnectingToProxy':
                        this.tg_loading = true;
                        this.tg_color = 'warning';
                        this.tg_status = 'establishing a connection with a proxy server'
                        break;

                    case 'connectionStateConnecting':
                        this.tg_loading = true;
                        this.tg_color = 'warning';
                        this.tg_status = 'establishing a connection to the Telegram servers'
                        break;

                    case 'connectionStateUpdating':
                        this.tg_loading = true;
                        this.tg_color = 'success';
                        this.tg_status = 'downloading data received while the client was offline'
                        break;

                    case 'connectionStateReady':
                        this.tg_loading = false;
                        this.tg_color = 'success';
                        this.tg_status = 'ready'
                        break;
                }
            } catch (err) {
                notify(err);
            } finally {
                this.proxy_loading = false;
            }
        },
        async vkcheck() {
            try {
                this.loading = true;
                await http.put(Paths['vkreload'] as string);
            } catch (err) {
                notify(err);
            } finally {
                this.loading = false;
            }
        },
        async check() {
            try {
                this.loading = true;
                await http.put(Paths['reload'] as string);
            } catch (err) {
                notify(err);
            } finally {
                this.loading = false;
            }
        },
        // async copyLink() {
        //     try {
        //         this.copyLoading = true;
        //         const { data } = await http.get(Paths['get-reload-link'] as string);
        //         window.open(data, '_blank');
        //     } catch (err) {
        //         notify(err);
        //     } finally {
        //         this.copyLoading = false;
        //     }
        // },
        // async updateLink() {
        //     try {
        //         this.updateLoading = true;
        //         const { data } = await http.patch(Paths['update-reload-link'] as string);
        //         window.open(data, '_blank');
        //     } catch (err) {
        //         notify(err);
        //     } finally {
        //         this.updateLoading = false;
        //     }
        // }
    }
}