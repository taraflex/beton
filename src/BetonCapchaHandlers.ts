import encode32 from 'base32-encode';
import { randomBytes } from 'crypto';
import { Context } from 'koa';
import bodyParser from 'koa-body';
import tunnel from 'tunnel';
import { Inject, Singleton } from 'typescript-ioc';

import { ClientSettings } from '@entities/ClientSettings';
import { stringify } from '@taraflex/string-tools';
import { HOSTNAME } from '@utils/config';
import delay from '@utils/delay';
import { ExtendableError } from '@utils/extendable-error';
import http from '@utils/http';
import { PROXY_AUTH, PROXY_HOST, PROXY_PORT, PROXY_URL } from '@utils/proxy-info';
import { post } from '@utils/routes-helpers';

import { DeferredMap } from './deffered';

export class RuCapchaError extends ExtendableError {
    constructor(public capchaId: string, message: string) {
        super(message);
    }
}

async function make(url: string, agent?: any) {
    const { body, statusCode, statusMessage } = await http<{ status: number, request: string }>({ url, throwHttpErrors: false, responseType: 'json', agent });
    DEBUG && LOG_INFO(statusCode, body);
    if (statusCode !== 200 || !body) {
        throw new RuCapchaError(null, stringify({ statusCode, statusMessage, body }));
    }
    if (body.status !== 1 && body.request !== 'CAPCHA_NOT_READY') {
        throw new RuCapchaError(null, stringify(body));
    }
    return body;
}

@Singleton
export class BetonCapchaHandlers {
    private readonly secret = encode32(randomBytes(20), 'Crockford').toLowerCase();
    private readonly pingback = `http://${HOSTNAME}/hooks/capcha/` + this.secret;
    private readonly store = new DeferredMap<string>();

    constructor(@Inject private readonly settings: ClientSettings) {
        this.processCapchas();
    }

    async processCapchas() {
        let { settings, store } = this;

        for (; ;) {
            let needPause = true;
            for (let id of store.keys()) {
                if (store.has(id)) {
                    if (Date.now() - store.createdAt(id) > settings.ruCapchaTimeout * 1000) {
                        this.inform(id, 'ERROR_BETON_TIMEOUT');
                    } else {
                        try {
                            const res = await make(`http://rucaptcha.com/res.php?key=${this.settings.ruCapchaKey}&action=get&json=1&id=` + id);

                            if (res.request !== 'CAPCHA_NOT_READY') {
                                this.inform(id, res.request);
                            }
                        } catch (err) {
                            LOG_ERROR(err);
                            if (err instanceof RuCapchaError) {
                                this.inform(id, err);
                            }
                        } finally {
                            await delay(10 * 1000);
                            needPause = false;
                        }
                    }
                }
            }
            if (needPause) {
                await delay(10 * 1000);
            }
        }
    }

    rejectAll() {
        this.store.rejectAll(null);
    }

    async request(pageurl: string, sitekey: string, action?: string): Promise<string> {
        const u = new URL('https://rucaptcha.com/in.php?method=userrecaptcha&json=1')
        const us = u.searchParams;

        us.set('key', this.settings.ruCapchaKey);
        us.set('pingback', this.pingback);
        us.set('googlekey', sitekey);
        us.set('pageurl', pageurl);

        if (action) {
            us.set('action', action);
            us.set('version', 'v3');
            us.set('min_score', '0.' + this.settings.capchaMinScore);
        }

        if (!DEBUG) {
            us.set('proxytype', 'HTTP');
            us.set('proxy', PROXY_URL);
        }

        const { request } = await make(u.href);

        return request;
    }

    waitResult(capchaId: string) {
        return this.store.make(capchaId);
    }

    reportBad(capchaId: string) {
        return make(`http://rucaptcha.com/res.php?key=${this.settings.ruCapchaKey}&action=reportbad&json=1&id=` + capchaId);
    }

    reportGood(capchaId: string) {
        return make(`http://rucaptcha.com/res.php?key=${this.settings.ruCapchaKey}&action=reportgood&json=1&id=` + capchaId);
    }

    @post({
        name: 'capcha',
        path: '/hooks/capcha/:secret'
    }, bodyParser({
        text: false,
        json: false
    }))
    capcha(ctx: Context) {
        if (ctx.params.secret === this.secret) {
            DEBUG && LOG_INFO('capcha callback', ctx.request.body);
            this.inform(ctx.request.body.id, ctx.request.body.code);
        }
        ctx.status = 200;
        ctx.body = '';
    }

    inform(id: string, code: string | RuCapchaError) {
        if (code instanceof RuCapchaError) {
            code.capchaId = id;
            this.store.reject(id, code);
        } else if (typeof code !== 'string') {
            this.store.reject(id, new RuCapchaError(id, stringify(code)));
        } else if (code.startsWith('ERROR')) {
            this.store.reject(id, new RuCapchaError(id, code));
        } else {
            this.store.fulfill(id, code);
        }
    }

    updateCallback(key: string) {
        return make(`https://rucaptcha.com/res.php?key=${key}&json=1&action=add_pingback&addr=` + encodeURIComponent(this.pingback), {
            https: tunnel.httpsOverHttp({
                proxy: {
                    host: PROXY_HOST,
                    port: PROXY_PORT,
                    // localAddress: localAddress, // Local interface if necessary
                    proxyAuth: PROXY_AUTH
                }
            })
        });
    }
}